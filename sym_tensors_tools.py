#!/usr/bin/env python3

###############################################################################
#
# Define useful functions to construct SU(N) and C_4v symmetric tensors.
#
###############################################################################

import itertools

import numpy as np
import scipy.sparse as sp

from PEPS_global import sparse_reshape


def rep_C4v(D):
    """
    Construct representation of C_4v in Hilbert space of 4 sites with local
    variable D.
    D : integer, dimension of on-site local variable
    return : (rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd3) matrix rep of C4v
    """
    idD = sp.eye(D**4).tocoo(copy=False)
    sh = (D**4, D, D, D, D)
    rot1 = sparse_reshape(idD, sh, (0, 4, 1, 2, 3))
    rot2 = sparse_reshape(idD, sh, (0, 3, 4, 1, 2))
    rot3 = sparse_reshape(idD, sh, (0, 2, 3, 4, 1))
    sigv1 = sparse_reshape(idD, sh, (0, 3, 2, 1, 4))
    sigv2 = sparse_reshape(idD, sh, (0, 1, 4, 3, 2))
    sigd1 = sparse_reshape(idD, sh, (0, 2, 1, 4, 3))
    sigd2 = sparse_reshape(idD, sh, (0, 4, 3, 2, 1))
    return rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd2


def sum_4sites(m0):
    """
    Take an operator m0 acting on a Hilbert space of 1 site
    return m1 + m2 + m3 + m4, where m_i acts on site i in Hilbert space of 4 sites
    """
    D = m0.shape[0]
    m1 = sp.kron(m0, sp.eye(D**3, dtype=m0.dtype)).tocoo(copy=False)
    sh1 = (D,) * 8
    m1234 = m1.tocsr()
    m1234 += sparse_reshape(m1, sh1, (3, 0, 1, 2, 7, 4, 5, 6))  # add m2
    m1234 += sparse_reshape(m1, sh1, (2, 3, 0, 1, 6, 7, 4, 5))  # add m3
    m1234 += sparse_reshape(m1, sh1, (1, 2, 3, 0, 5, 6, 7, 4))  # add m4
    return m1234


def c2_4sites(gen):
    """
    construct SU(N) Casimir operator ||(sum S)^2|| of 4 sites
    gen: generators of local representation on one site
    """
    D = gen.shape[2]
    SdS_2s = np.tensordot(gen, gen, (0, 0)).real.swapaxes(1, 2).reshape(D**2, D**2)
    SdS_4s = sp.kron(SdS_2s, sp.eye(D**2)).tocoo(copy=False)

    sh4 = (D,) * 8
    S1dS2 = SdS_4s.tocsr(copy=False)
    S1dS3 = sparse_reshape(SdS_4s, sh4, (0, 2, 1, 3, 4, 6, 5, 7))
    S1dS4 = sparse_reshape(SdS_4s, sh4, (0, 3, 2, 1, 4, 7, 6, 5))
    S2dS3 = sparse_reshape(SdS_4s, sh4, (2, 1, 0, 3, 6, 5, 4, 7))
    S2dS4 = sparse_reshape(SdS_4s, sh4, (3, 1, 2, 0, 7, 5, 6, 4))
    S3dS4 = sparse_reshape(SdS_4s, sh4, (2, 3, 0, 1, 6, 7, 4, 5))

    cas0 = np.tensordot(
        gen, gen, ((0, 2), (0, 1))
    ).real  # Casimir in Hilbert space of one site
    cas1234 = sum_4sites(cas0) + 2 * (
        S1dS2 + S1dS3 + S1dS4 + S2dS3 + S2dS4 + S3dS4
    )  # ||sum S_i||^2
    return cas1234


def casimir2(*generators):
    """
    construct SU(N) Casimir operator ||(sum S)^2|| from given generators. No spatial
    symmetry is assumed.
    """
    n_sites = len(generators)
    n_gen = generators[0].shape[0]
    local_dim = tuple(g.shape[2] for g in generators)
    h_dim = np.prod(local_dim)
    H = np.zeros((h_dim, h_dim))
    for i, j in itertools.combinations(range(n_sites), 2):
        d0_i = np.prod(local_dim[:i]).astype(int)  # np.prod([]) = 1
        dj_n = np.prod(local_dim[j + 1 :]).astype(int)
        di_j = np.prod(local_dim[i + 1 : j]).astype(int)
        h_ij = np.zeros((local_dim[i] * di_j * local_dim[j],) * 2)
        id_ij = np.eye(di_j)
        for k in range(n_gen):
            h_ij += np.kron(2 * generators[i][k], np.kron(id_ij, generators[j][k])).real
        H += np.kron(np.eye(d0_i), np.kron(h_ij, np.eye(dj_n)))
    for i, g in enumerate(generators):
        d0_i = np.prod(local_dim[:i]).astype(int)
        di_n = np.prod(local_dim[i + 1 :]).astype(int)
        h_ii = np.tensordot(g, g, ((0, 2), (0, 1))).real
        H += np.kron(np.eye(d0_i), np.kron(h_ii, np.eye(di_n)))
    return H


def rotate2exact(T1, T2, tol=1e-11):
    # take 2 corresponding to 2 tensors with same symetries and an arbitrary
    # non-analytic expression.
    # we want to find a rotation angle phi such that
    # T1' = cos(phi)*T1 + sin(phi)*T2
    # takes an exact form.
    # work around: find an index i such that T1[i]**2 + T2[i]**2 < 1
    # fix phi such that T1'[i] = 0
    # then T1' and T2' take an exact form. DIY but works.

    t = T1**2 + T2**2
    t /= np.max(t)
    i = ((t > tol) & (t < 1 - tol)).nonzero()[0][0]
    phi = -np.arctan(T1[i] / T2[i])
    T1p = np.cos(phi) * T1 + np.sin(phi) * T2
    T2p = -np.sin(phi) * T1 + np.cos(phi) * T2
    return np.array([T1p, T2p])


def write_tensor_latex(fname, Tname, T, norm_str):
    with open(fname, "w") as f:
        f.write("\\begin{longtable}{MMM}\n\\hline \\hline\n")
        i = 0
        it = np.nditer(T, flags=["multi_index"])
        while not it.finished:
            if it[0]:
                i += 1
                end = "  &  " if i % 3 else "  \\\\\n"
                f.write(
                    Tname
                    + "["
                    + ",".join(map(str, it.multi_index))
                    + f"] = {it[0]: }"
                    + end
                )
            it.iternext()
        f.write(
            "\n\\hline \\hline\n\\caption{Tensor $"
            + Tname
            + "$ (multiplied by $"
            + norm_str
            + "$).}\n"
        )
        f.write("\\label{Table:tensorT}\n\\end{longtable}")
