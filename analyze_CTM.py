#!/usr/bin/env python3

###############################################################################
#
# Analyze data from CTMRG
# input: 1 parameter file following input_sample/input_sample_analysis_CTM.txt
#        1 file containing list of file names with CTM data
#
###############################################################################


import numpy as np
import scipy.linalg as lg
from sys import argv
from time import time
from CTMRG import CTMRG, CTM_Z2, sparseCTM
from PEPS_global import mvirt


if len(argv) < 3:
    print("not enough input files given, abort")
    print("requires 1 parameter file and 1 list of file names file")
    exit()

print("#" * 79, "Analyze data obtain from CTMRG", sep="\n")
print("Take input parameters from file", argv[1])
print("Analyze data from data files listed in file", argv[2])

# assume A is always the same in all data files and take it from parameter files
# allow to deal with different CTM data file formats and avoid re-computing E
with open(argv[1], "r") as fin:
    A_i_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    a_i0 = np.fromstring(fin.readline().split("=")[1], sep=",")
    cplxL = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    peps_sqrt_s = fin.readline().split("=")[1].strip()  # peps_sqrt name or '0'
    pcolDs = fin.readline().split("=")[1].strip()  # pseudo-colors
    force_real = bool(int(fin.readline().split("=")[1]))
    alternate = bool(int(fin.readline().split("=")[1]))
    sparse = bool(int(fin.readline().split("=")[1]))
    tol = float(fin.readline().split("=")[1])  # CTMRG tol
    obs2_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    obs3_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    compute_tsp = bool(int(fin.readline().split("=")[1]))
    compute_xi = bool(int(fin.readline().split("=")[1]))
    compute_s_ent = bool(int(fin.readline().split("=")[1]))
    Sz_s = fin.readline().split("=")[1].strip()
    rmax_Sz = int(fin.readline().split("=")[1])  # <Sz(0)Sz(r)>, 0 = not computed
    S_op_s = fin.readline().split("=")[1].strip()
    rmax_dim = int(fin.readline().split("=")[1])  # <(S(0).S(1))(S(r).S(r+1)>
    saveObs = fin.readline().split("=")[1].strip()

if pcolDs == "0":
    if sparse:
        print("Error: need Z2 symmetry to use sparse CTM")
        exit()
    if compute_tsp:
        print("Error: need Z2 symmetry to compute transfer spectrum")
        exit()
    print("no pcol, use class CTMRG")
    Z2 = False
else:
    Z2 = True

compute_xi |= compute_tsp  # get it for free from tsp, simpler code.

print(f"A_i = {A_i_s}")
print(f"a_i = {a_i0}")
print(f"cplxL = {cplxL}")
print(f"peps_sqrt = {peps_sqrt_s}, pcolD = {pcolDs}")
print(f"force_real = {force_real}")
print(f"alternate = {alternate}")
print(f"sparse = {sparse}")
print(f"tolerance = {tol}")
print(f"obs2L = {obs2_s}")  # 2-site observables
print(f"obs3L = {obs3_s}")  # 3-site observables
print(f"compute_xi = {compute_xi}")
print(f"compute_s_ent = {compute_s_ent}")
print(f"compute_tsp = {compute_tsp}")
print(f"rmax_Sz = {rmax_Sz}")
print(f"rmax_dim = {rmax_dim}")
print(f"save observables in file {saveObs}")


dctm = {
    "a_i": a_i0,
    "A_i_s": A_i_s,
    "cplxL": cplxL,
    "peps_sqrt_s": peps_sqrt_s,
    "pcolDs": pcolDs,
    "force_real": force_real,
    "alternate": alternate,
    "sparse": sparse,
    "tol": tol,
}
dobs = {}  # set observables in dictionary to save them
dtsp = {}
if compute_xi:
    dobs["xiL"] = []
if compute_s_ent:
    dobs["s_entL"] = []

# load tensors
try:
    with np.load("tensors.npz") as fin:
        A_i = fin["tRVB2"]
except FileNotFoundError:
    print("", "#" * 79, "file tensors.npz not found, recompute it", sep="\n")
    import save_tensors

    print("#" * 79, "\n")

compute_rho2, compute_rho3, compute_SzSz, compute_dim = [False] * 4
with np.load("tensors.npz") as fin:
    A_i = [fin[s] for s in A_i_s]
    if Z2:
        pcolD = fin[pcolDs]
        print("pcolD =", pcolD, sep="\n")
    if peps_sqrt_s != "0":
        peps_sqrt = fin[peps_sqrt_s]
        print("peps_sqrt =", peps_sqrt, sep="\n")
        sqrt_inv = peps_sqrt.conj()
    else:
        print("no peps_sqrt")
        peps_sqrt = None
        sqrt_inv = None
    if obs2_s[0] != "0":
        compute_rho2 = True
        dobs["obs2_s"] = obs2_s
        dobs["obs2L"] = []
        obs2_mat = [fin[s] for s in obs2_s]
    if obs3_s[0] != "0":
        compute_rho3 = True
        dobs["obs3_s"] = obs3_s
        dobs["obs3L"] = []
        obs3_mat = [fin[s] for s in obs3_s]
    if Sz_s != "0":
        compute_SzSz = True
        Sz = fin[Sz_s]
        dobs["SzSz"] = []
    if S_op_s != "0":
        compute_dim = True
        S_op = fin[S_op_s]
        dobs["dimers"] = []

# add 1j and peps_sqrt
a_i = a_i0.copy()
for i in range(len(A_i)):
    if peps_sqrt is not None:
        A_i[i] = mvirt(A_i[i], peps_sqrt)
    if cplxL[i]:
        A_i[i] = 1j * A_i[i]
    if not sparse:
        a_i[i] = a_i[i] / lg.norm(A_i[i])

if sparse:
    ctm = sparseCTM(a_i, A_i, 4, pcolD, force_real=force_real, alternate=alternate)
else:
    A = np.dot(np.array(A_i).transpose(1, 2, 3, 4, 5, 0), a_i)
    if Z2:
        ctm = CTM_Z2(A, 4, pcolD, force_real=force_real, alternate=alternate)
    else:
        ctm = CTMRG(A, 4, force_real=force_real, alternate=alternate)

# check files can be opened, remove duplicates and sort by chi
print("\nraw files = ", end="")
files_dic = {}
with open(argv[2], "r") as fnames:
    for line in fnames:
        f = line.strip()
        if f:  # avoid empty string
            print(f, end=", ")
            with np.load(f) as fin:
                files_dic[len(fin["s"])] = f

print("\nAfter duplicate removal and sorting by chi, files are:")
chiL0 = sorted(files_dic.keys())
for chi in chiL0:
    print(files_dic[chi], ", chi = ", chi, sep="")

print("For each CTM data file, compute observables and save them in file", saveObs)
print("with keys:", *dctm.keys(), *dobs.keys(), *dtsp)
chiL = []  # expect crash any time, save only analyzed chi


###############################################################################
for chi in chiL0:
    f = files_dic[chi]
    print("", "#" * 79, f"analyze file {f}, chi = {chi}", sep="\n")
    with np.load(f) as fin:
        if Z2:
            ctm.set_sT(fin["s"], fin["T"], fin["pcol"])
        else:
            ctm.set_sT(fin["s"], fin["T"])
    print("check SU(N): boundSpec2 degen =", ctm.check_boundSpec2(tol=tol)[1])
    chiL.append(chi)
    if compute_xi:
        if compute_tsp:
            print("compute transfer matrix spectrum", end="... ", flush=True)
            t = time()
            tsp = ctm.transmat_spec()
            print(f"done, time = {time()-t:.1f}")
            dtsp[f"tsp_{ctm.chi}"] = tsp
            tsp = np.sort(np.abs(tsp))[::-1]
            xi = 1 / np.log(tsp[0] / tsp[1])
        else:
            xi = ctm.compute_corr_length(maxiter=500)
        dobs["xiL"].append(xi)
        print(f"xi = {xi:.1f}")
    if compute_s_ent:
        s_ent = ctm.compute_entropy(maxiter=500)
        dobs["s_entL"].append(s_ent)
        print(f"s_ent = {s_ent:.3f}")
    if compute_rho2:
        rho2 = ctm.compute_density_matrix(tol=tol)
        obs2_val = [np.einsum("ij,ij", m, rho2).real for m in obs2_mat]
        dobs["obs2L"].append(obs2_val)
        print(f"2-sites observables = {obs2_val}")
    if compute_rho3:
        rho3 = ctm.compute_rho3(tol=tol)
        obs3_val = [np.einsum("ij,ij", m, rho3).real for m in obs3_mat]
        dobs["obs3L"].append(obs3_val)
        print(f"3-sites observables = {obs3_val}")
    if compute_SzSz:
        print(f"compute SzSz correlator for r <= {rmax_Sz}", end="... ", flush=True)
        t = time()
        c = ctm.corr_SzSz(Sz, rmax_Sz)
        dobs["SzSz"].append(c)
        print(f"done, time = {time()-t:.1f}")
        print("<SzSz> =", c, sep="\n")
    if compute_dim:
        print(f"compute dimer correlator for r <= {rmax_dim}", end="... ", flush=True)
        t = time()
        c = ctm.corr_dimer(S_op, rmax_dim)
        dobs["dimers"].append(c)
        print(f"done, time = {time()-t:.1f}")
        print("<(S.S)(S.S)> - <S.S>^2 =", c, sep="\n")
    ###########################################################################
    np.savez_compressed(saveObs, chiL=chiL, **dctm, **dobs, **dtsp)
    print("obs data saved in file", saveObs)


###############################################################################
print("\n", "#" * 79, "Analyze completed.", sep="\n")
print(f"A_i = {A_i_s}")
print(f"a_i = {a_i0}")
print(f"chiL = {chiL}")
