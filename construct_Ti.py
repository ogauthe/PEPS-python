#!/usr/bin/env python3
import numpy as np
import scipy.linalg as lg
from exact_diagonalization import Lie, codiagh, diagHamilt1D
from gen_SUN import genSU4_4, colorsSU4_4, colorsSU4_6


###############################################################################
#
# construct SU(4) generalized RVB tensors for irrep 6
# S = 6
# D = 7
# V = 6 + 1
#
###############################################################################


tol = 1e-13
#################################################
#    First : construct generators of gen6 6
#################################################

# Cartan operators with convenient definition
H1mat = np.diag(colorsSU4_4[:, 0])
H2mat = np.diag(colorsSU4_4[:, 1])
H3mat = np.diag(colorsSU4_4[:, 2])

# Ladder operators
S1m, S2m, S3m = np.zeros((3, 4, 4), dtype=np.int8)
S1m[1, 0] = 1  # root (-2,1,0)
S2m[2, 1] = 1  # root (1,-2,1)
S3m[3, 2] = 1  # root (0,1,-2)

# commutation relations tensor
tensSU4 = np.empty((15, 15, 15), dtype=complex)  # pure imaginary
for i in range(15):
    for j in range(15):
        for k in range(15):
            tensSU4[i, j, k] = np.trace(genSU4_4[i] @ Lie(genSU4_4[j], genSU4_4[k]))

temp = np.rint(tensSU4.imag)
print(lg.norm(tensSU4 - 1j * temp))
tensSU4 = temp


def w4(v):
    """
    root diagram of fundamental irrep
    """
    return v.conj() @ H1mat @ v, v.conj() @ H2mat @ v, v.conj() @ H3mat @ v


hw4 = np.zeros(4, dtype=np.int8)
hw4[0] = 1
print("root diagram of SU(4) fundamental irrep:")
print(w4(hw4))
print(w4(S1m @ hw4))
print(w4(S2m @ S1m @ hw4))
print(w4(S3m @ S2m @ S1m @ hw4))


H44 = sum(np.kron(mat, mat).real for mat in genSU4_4)
spec44, momentum44, colors44, base44 = diagHamilt1D(H44, 2, colorsSU4_4)

# we started with Totsuka convention for vector order. They do not follow any
# lexsort order, which is regrettable, but stick to them for retrocompatibility
order = np.array(
    [[tuple(c) for c in colors44[10:]].index(tuple(c)) for c in colorsSU4_6]
)
p44to6 = base44[:, 10 + order].real.copy()

# ladder operators on 4x4
S1m44 = np.kron(np.eye(4), S1m) + np.kron(S1m, np.eye(4))
S2m44 = np.kron(np.eye(4), S2m) + np.kron(S2m, np.eye(4))
S3m44 = np.kron(np.eye(4), S3m) + np.kron(S3m, np.eye(4))

# set the same phase for every vectors: bruteforce because this is easy. Clever
# solution = apply Sminus operators to highest weight (0,1,0)
temp = p44to6.copy()
i = 0
while (
    np.min(p44to6.T @ S1m44 @ p44to6) < -tol
    or np.min(p44to6.T @ S2m44 @ p44to6) < -tol
    or np.min(p44to6.T @ S3m44 @ p44to6) < -tol
):
    i += 1
    p44to6 = temp.copy()
    for j in range(6):
        if i // (2**j) % 2:
            p44to6[:, j] *= -1


gen6 = np.empty((15, 6, 6), dtype=complex)
for i in range(15):
    gen6[i] = (
        p44to6.T
        @ (np.kron(genSU4_4[i], np.eye(4)) + np.kron(np.eye(4), genSU4_4[i]))
        @ p44to6
    )

print(lg.norm(gen6 - np.rint(gen6 * 2) / 2))
gen6 = np.rint(gen6 * 2) / 2

H1mat6 = np.diag(colorsSU4_6[:, 0])
H2mat6 = np.diag(colorsSU4_6[:, 1])
H3mat6 = np.diag(colorsSU4_6[:, 2])
S1m6 = np.rint(p44to6.T @ S1m44 @ p44to6).astype(int)
S2m6 = np.rint(p44to6.T @ S2m44 @ p44to6).astype(int)
S3m6 = np.rint(p44to6.T @ S3m44 @ p44to6).astype(int)


gram = np.empty((15, 15), dtype=complex)
for i in range(15):
    for j in range(15):
        gram[i, j] = np.trace(gen6[i] @ gen6[j])

assert sum(lg.norm(m - m.T.conj()) for m in gen6) < tol, "gen6 not hermitian"
assert sum(abs(np.trace(m)) for m in gen6) < tol, "gen6 not traceless"
assert lg.norm(gram - 2 * np.eye(15)) < tol, "gen6 not orthogonal"

Tcheck = np.empty((15, 15, 15), dtype=complex)
for i in range(15):
    for j in range(15):
        for k in range(15):
            Tcheck[i, j, k] = (
                np.trace(gen6[i] @ Lie(gen6[j], gen6[k])) / 2
            )  # 2 from Gram

assert lg.norm(Tcheck - 1j * tensSU4) < tol, "commutation tensor is not tensSU4"

H66 = sum(np.kron(mat, mat).real for mat in gen6)
spec66, momentum66, colors66, base66 = diagHamilt1D(H66, 2, colorsSU4_6)
print(lg.norm(base66[:, 0] - np.rint(np.sqrt(6) * base66[:, 0]) / np.sqrt(6)))
print("\np66to1 =", np.rint(np.sqrt(6) * base66[:, 0]).reshape(6, 6), sep="\n")


# root diagram of irrep 6
def w6(v):
    return v.conj() @ H1mat6 @ v, v.conj() @ H2mat6 @ v, v.conj() @ H3mat6 @ v


hw6 = np.zeros(6, dtype=int)
hw6[3] = 1
print("root diagram of SU(4) [0,1,0] irrep:")
print(w6(hw6))  # 3
print(w6(S2m6 @ hw6))  # 1
print(w6(S3m6 @ S2m6 @ hw6))  # 0
print(w6(S1m6 @ S2m6 @ hw6))  # 5
print(w6(S3m6 @ S1m6 @ S2m6 @ hw6))  # 4
print(w6(S2m6 @ S3m6 @ S1m6 @ S2m6 @ hw6))  # 2


# construct of projection tensor 6*6*6 --> 6
H666 = (
    sum(np.kron(np.eye(6), np.kron(mat, mat)) for mat in gen6)
    + sum(np.kron(mat, np.kron(np.eye(6), mat)) for mat in gen6)
    + sum(np.kron(mat, np.kron(mat, np.eye(6))) for mat in gen6)
).real
H1mat666 = (
    np.kron(np.eye(6), np.kron(H1mat6, np.eye(6)))
    + np.kron(np.eye(36), H1mat6)
    + np.kron(H1mat6, np.eye(36))
)
H2mat666 = (
    np.kron(np.eye(6), np.kron(H2mat6, np.eye(6)))
    + np.kron(np.eye(36), H2mat6)
    + np.kron(H2mat6, np.eye(36))
)
H3mat666 = (
    np.kron(np.eye(6), np.kron(H3mat6, np.eye(6)))
    + np.kron(np.eye(36), H3mat6)
    + np.kron(H3mat6, np.eye(36))
)

order6 = np.diag([-3, -2, -1, 1, 2, 3])
order666 = (
    np.kron(np.eye(6), np.kron(order6, np.eye(6)))
    + np.kron(np.eye(36), order6)
    + np.kron(order6, np.eye(36))
)  # get sure to keep vector order when diagonalize

# represent C_3v local group
rot1, rot2, refl1, refl2, refl3 = np.zeros((5, 6**3, 6**3), dtype=np.int8)
for i in range(6**3):
    f1, f2, f3 = i // 36, i // 6 % 6, i % 6
    rot1[i, f1 * 6 + f2 + f3 * 36] = 1
    rot2[i, f1 + f2 * 36 + f3 * 6] = 1
    refl1[i, f1 * 36 + f2 + f3 * 6] = 1
    refl2[i, f1 + f2 * 6 + f3 * 36] = 1
    refl3[i, f1 * 6 + f2 * 36 + f3] = 1


# get irreps with local group symmetry in one line
eigVals666, base666 = codiagh(H666, rot1 + rot2, refl1, order666)

# choose sign conventions identical to gen6  (use S1m, S2m and S3m to find them)
# global sign is irrelevant
S1m666 = (
    np.kron(S1m6, np.eye(36))
    + np.kron(np.kron(np.eye(6), S1m6), np.eye(6))
    + np.kron(np.eye(36), S1m6)
)
S2m666 = (
    np.kron(S2m6, np.eye(36))
    + np.kron(np.kron(np.eye(6), S2m6), np.eye(6))
    + np.kron(np.eye(36), S2m6)
)
S3m666 = (
    np.kron(S3m6, np.eye(36))
    + np.kron(np.kron(np.eye(6), S3m6), np.eye(6))
    + np.kron(np.eye(36), S3m6)
)
M = S1m666 + S2m666 + S3m666


def fixSign(proj):
    """
    brute force to ensure each column has the same phase
    """
    i = 0
    p = proj.copy()
    pow2 = 2 ** np.arange(6)[::-1]
    while lg.norm(S1m6 + S2m6 + S3m6 - p.T @ M @ p) > tol:
        i += 1
        p = proj.copy()
        p[:, (i // pow2 % 2).nonzero()[0]] *= -1
    return p


# this is fine:
proj6A1 = fixSign(base666[:, 12:18])
proj6A1 *= np.sign(proj6A1.sum())  # fix global sign with sum
assert lg.norm(proj6A1.T @ rot1 @ proj6A1 - np.eye(6)) < tol, "proj not A1"
assert lg.norm(proj6A1.T @ refl1 @ proj6A1 - np.eye(6)) < tol, "proj not A1"


# first tensor, obtained from rotation of tensor A1 C_3v
print(lg.norm(proj6A1 - np.rint(2 * np.sqrt(6) * proj6A1) / 2 / np.sqrt(6)))
tensorA1_C3v = (
    np.rint(2 * np.sqrt(6) * proj6A1).astype(np.int8).swapaxes(0, 1).reshape(6, 6, 6, 6)
)

tensorA1_C4v = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
tensorA1_C4v[:, 6, :6, :6, :6] += tensorA1_C3v
tensorA1_C4v[:, :6, 6, :6, :6] += tensorA1_C3v
tensorA1_C4v[:, :6, :6, 6, :6] += tensorA1_C3v
tensorA1_C4v[:, :6, :6, :6, 6] += tensorA1_C3v


# second tensor, obtained from tensor C_s symmetric (part of irrep E of C_3v)
proj6E_CsAp = fixSign(base666[:, 6:12])
proj6E_CsAp *= -np.sign(proj6E_CsAp[5, 0])  # fix global sign with 1st non-zero
print(lg.norm(proj6E_CsAp - np.rint(np.sqrt(30) * proj6E_CsAp) / np.sqrt(30)))
tensorAp_Cs = (
    np.rint(np.sqrt(30) * proj6E_CsAp)
    .astype(np.int8)
    .swapaxes(0, 1)
    .reshape(6, 6, 6, 6)
)

tensorA1_C4v2 = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
tensorA1_C4v2[:, :6, :6, 6, :6] += tensorAp_Cs
tensorA1_C4v2[:, 6, :6, :6, :6] += tensorAp_Cs.swapaxes(1, 2)
tensorA1_C4v2[:, :6, 6, :6, :6] += tensorAp_Cs.swapaxes(1, 3)
tensorA1_C4v2[:, :6, :6, :6, 6] += tensorAp_Cs.swapaxes(1, 2)


# third tensor, with A2 symmetry, from C_v A'
proj6E_CsApp = fixSign(base666[:, :6])
proj6E_CsApp *= np.sign(proj6E_CsApp[5, 0])  # fix global sign with 1st non-zero
print(lg.norm(proj6E_CsApp - np.rint(np.sqrt(10) * proj6E_CsApp) / np.sqrt(10)))
tensorApp_Cs = (
    np.rint(np.sqrt(10) * proj6E_CsApp)
    .astype(np.int8)
    .swapaxes(0, 1)
    .reshape(6, 6, 6, 6)
)

tensorA2_C4v = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
tensorA2_C4v[:, :6, :6, 6, :6] += tensorApp_Cs
tensorA2_C4v[:, 6, :6, :6, :6] += np.moveaxis(tensorApp_Cs, [1, 2, 3], [2, 3, 1])
tensorA2_C4v[:, :6, 6, :6, :6] += np.moveaxis(tensorApp_Cs, [1, 2, 3], [3, 1, 2])
tensorA2_C4v[:, :6, :6, :6, 6] += np.moveaxis(tensorApp_Cs, [1, 2, 3], [2, 3, 1])


# check symmetries
charTable = {
    "A1": np.ones(7),
    "A2": np.array([1, 1, 1, -1, -1, -1, -1]),
    "B1": np.array([-1, 1, -1, +1, +1, -1, -1]),
    "B2": np.array([-1, 1, -1, -1, -1, 1, 1]),
}


def checkSym(T, signs):
    assert (
        lg.norm(T - signs[0] * np.moveaxis(T, [1, 2, 3, 4], [2, 3, 4, 1])) < tol
    ), "C_4"
    assert (
        lg.norm(T - signs[1] * np.moveaxis(T, [1, 2, 3, 4], [3, 4, 1, 2])) < tol
    ), "C_2"
    assert (
        lg.norm(T - signs[2] * np.moveaxis(T, [1, 2, 3, 4], [4, 1, 2, 3])) < tol
    ), "C_4"
    assert lg.norm(T - signs[3] * np.moveaxis(T, [1, 3], [3, 1])) < tol, "sigma_v"
    assert lg.norm(T - signs[4] * np.moveaxis(T, [2, 4], [4, 2])) < tol, "sigma_v"
    assert (
        lg.norm(T - signs[5] * np.moveaxis(T, [1, 2, 3, 4], [2, 1, 4, 3])) < tol
    ), "sigma_d"
    assert (
        lg.norm(T - signs[6] * np.moveaxis(T, [1, 2, 3, 4], [4, 3, 2, 1])) < tol
    ), "sigma_d"


checkSym(tensorA1_C4v, charTable["A1"])
checkSym(tensorA1_C4v2, charTable["A1"])
checkSym(tensorA2_C4v, charTable["A2"])

# check orthogonality
assert (
    lg.norm(np.tensordot(tensorA1_C4v, tensorA1_C4v2, ((1, 2, 3, 4), (1, 2, 3, 4))))
    < tol
), "loose of orthogonality in A1 tensors"


# to get B1 and B2 tensors, just put minus sign on rotations of A1 and A2 tensors.
tensorB1_C4v1 = np.zeros((6, 7, 7, 7, 7), dtype=int)
tensorB1_C4v1[:, 6, :6, :6, :6] += tensorA1_C3v
tensorB1_C4v1[:, :6, 6, :6, :6] -= tensorA1_C3v
tensorB1_C4v1[:, :6, :6, 6, :6] += tensorA1_C3v
tensorB1_C4v1[:, :6, :6, :6, 6] -= tensorA1_C3v

tensorB1_C4v2 = np.zeros((6, 7, 7, 7, 7), dtype=int)
tensorB1_C4v2[:, :6, :6, 6, :6] += tensorAp_Cs
tensorB1_C4v2[:, 6, :6, :6, :6] += tensorAp_Cs.swapaxes(1, 2)
tensorB1_C4v2[:, :6, 6, :6, :6] -= tensorAp_Cs.swapaxes(1, 3)
tensorB1_C4v2[:, :6, :6, :6, 6] -= tensorAp_Cs.swapaxes(1, 2)

tensorB2_C4v = np.zeros((6, 7, 7, 7, 7), dtype=int)
tensorB2_C4v[:, 6, :6, :6, :6] += np.moveaxis(tensorApp_Cs, [1, 2, 3], [2, 3, 1])
tensorB2_C4v[:, :6, 6, :6, :6] -= np.moveaxis(tensorApp_Cs, [1, 2, 3], [3, 1, 2])
tensorB2_C4v[:, :6, :6, 6, :6] += tensorApp_Cs
tensorB2_C4v[:, :6, :6, :6, 6] -= np.moveaxis(tensorApp_Cs, [1, 2, 3], [2, 3, 1])

checkSym(tensorB1_C4v1, charTable["B1"])
checkSym(tensorB1_C4v2, charTable["B1"])
checkSym(tensorB2_C4v, charTable["B2"])

# check orthogonality
assert (
    lg.norm(np.tensordot(tensorB1_C4v1, tensorB1_C4v2, ((1, 2, 3, 4), (1, 2, 3, 4))))
    < tol
), "loose of orthogonality in B1 tensors"


def colorsIrrep6(Nv, D=6):
    """
    get colors of Nv sites of irrep 6
    """
    holes = [0] * (D - 6)
    H1diag = np.array([1, 1, 0, 0, -1, -1] + holes, dtype=np.int16)
    H2diag = np.array([0, -1, -1, 1, 1, 0] + holes, dtype=np.int16)
    H3diag = np.array([-1, 1, 0, 0, -1, 1] + holes, dtype=np.int16)
    colors = np.empty((D**Nv, 3), dtype=np.int16)  # colors are short int
    for i in range(D**Nv):
        ibaseD = [i // D**f % D for f in reversed(range(Nv))]
        colors[i] = H1diag[ibaseD].sum(), H2diag[ibaseD].sum(), H3diag[ibaseD].sum()
    return colors


def writeTensor(T, f, name):
    f.write(name + " = np.zeros({},dtype=np.{})\n\n".format(T.shape, T.dtype))
    it = np.nditer(T, flags=["multi_index"])
    while not it.finished:
        if it[0]:
            f.write(
                name
                + "["
                + ",".join(map(str, it.multi_index))
                + "] = {}\n".format(it[0])
            )
        it.iternext()


with open("tensors_SU4_6_C4v.py", "w") as f:
    f.write("import numpy as np\n")
    f.write("\n# first A1 C4v projector (6+1)^4 --> 6\n")
    writeTensor(tensorA1_C4v, f, "T1")
    f.write("\n# second A1 C4v projector (6+1)^4 --> 6\n")
    writeTensor(tensorA1_C4v2, f, "T2")
    f.write("\n# A2 C4v projector (6+1)^4 --> 6\n")
    writeTensor(tensorA2_C4v, f, "T3")
