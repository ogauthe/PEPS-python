import numpy as np


################################################################################
# SU(2)
################################################################################
pauliMat = np.array([[[0, 1], [1, 0]], [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]])
# fundamental spin 1/2 irrep: S = pauliMat/2


def construct_genSU2_s(s):
    """
    Construct generator for spin-s irrep of SU(2)
    """
    if s < 0 or int(2 * s) != 2 * s:
        raise ValueError("s must be a positive half integer")

    d = int(2 * s) + 1
    basis = np.arange(s, -s - 1, -1)
    Sm = np.zeros((d, d))
    Sm[np.arange(1, d), np.arange(d - 1)] = np.sqrt(
        s * (s + 1) - basis[:-1] * (basis[:-1] - 1)
    )
    Sp = Sm.T
    gen = np.empty((3, d, d), dtype=complex)
    gen[0] = (Sp + Sm) / 2  # Sx
    gen[1] = (Sp - Sm) / 2j  # Sy
    gen[2] = np.diag(basis)  # Sz
    return gen


################################################################################
# SU(3)
################################################################################
gellMannMat = np.array(
    [
        [[0, 1, 0], [1, 0, 0], [0, 0, 0]],
        [[0, -1j, 0], [1j, 0, 0], [0, 0, 0]],
        [[1, 0, 0], [0, -1, 0], [0, 0, 0]],
        [[0, 0, 1], [0, 0, 0], [1, 0, 0]],
        [[0, 0, -1j], [0, 0, 0], [1j, 0, 0]],
        [[0, 0, 0], [0, 0, 1], [0, 1, 0]],
        [[0, 0, 0], [0, 0, -1j], [0, 1j, 0]],
        [[1, 0, 0], [0, 1, 0], [0, 0, -2]] / np.sqrt(3),
    ]
)
# fundamental irrep: S = gellMannMat/2
# conjugate irrep: S = -gellMannMat.conj()/2


################################################################################
# SU(4)
################################################################################
# irrep 4. Convention: eigenvalues are +/- 1/2 <=> dot(gen_i, gen_j) = delta_i,j
genSU4_4 = np.empty((15, 4, 4), dtype=complex)
genSU4_4[0] = np.kron(pauliMat[0] / 2, np.eye(2))
genSU4_4[1] = np.kron(pauliMat[1] / 2, np.eye(2))
genSU4_4[2] = np.kron(pauliMat[2] / 2, np.eye(2))
genSU4_4[3] = np.kron(np.eye(2), pauliMat[0] / 2)
genSU4_4[4] = np.kron(np.eye(2), pauliMat[1] / 2)
genSU4_4[5] = np.kron(np.eye(2), pauliMat[2] / 2)
genSU4_4[6] = 2 * np.kron(pauliMat[0] / 2, pauliMat[0] / 2)
genSU4_4[7] = 2 * np.kron(pauliMat[1] / 2, pauliMat[1] / 2)
genSU4_4[8] = 2 * np.kron(pauliMat[2] / 2, pauliMat[2] / 2)
genSU4_4[9] = 2 * np.kron(pauliMat[1] / 2, pauliMat[2] / 2)
genSU4_4[10] = 2 * np.kron(pauliMat[2] / 2, pauliMat[1] / 2)
genSU4_4[11] = 2 * np.kron(pauliMat[2] / 2, pauliMat[0] / 2)
genSU4_4[12] = 2 * np.kron(pauliMat[0] / 2, pauliMat[2] / 2)
genSU4_4[13] = 2 * np.kron(pauliMat[0] / 2, pauliMat[1] / 2)
genSU4_4[14] = 2 * np.kron(pauliMat[1] / 2, pauliMat[0] / 2)

# irrep 4bar: S = -genSU4_4.conj()
colorsSU4_4 = np.array([[1, 0, 0], [-1, 1, 0], [0, -1, 1], [0, 0, -1]], dtype=np.int8)


# irrep 6
# convention: eigenvalues are +/- 1/2 <=> dot(gen_i, gen_j) = delta_i,j
colorsSU4_6 = np.array(
    [[1, 0, -1], [1, -1, 1], [0, -1, 0], [0, 1, 0], [-1, 1, -1], [-1, 0, 1]],
    dtype=np.int8,
)

genSU4_6 = np.zeros((15, 6, 6), dtype=complex)
genSU4_6[0, 0, 2] = 0.5
genSU4_6[0, 0, 3] = 0.5
genSU4_6[0, 2, 0] = 0.5
genSU4_6[0, 2, 5] = -0.5
genSU4_6[0, 3, 0] = 0.5
genSU4_6[0, 3, 5] = -0.5
genSU4_6[0, 5, 2] = -0.5
genSU4_6[0, 5, 3] = -0.5
genSU4_6[1, 0, 2] = -0.5j
genSU4_6[1, 0, 3] = 0.5j
genSU4_6[1, 2, 0] = 0.5j
genSU4_6[1, 2, 5] = -0.5j
genSU4_6[1, 3, 0] = -0.5j
genSU4_6[1, 3, 5] = 0.5j
genSU4_6[1, 5, 2] = 0.5j
genSU4_6[1, 5, 3] = -0.5j
genSU4_6[2, 2, 2] = -1.0
genSU4_6[2, 3, 3] = 1.0
genSU4_6[3, 0, 1] = 0.5
genSU4_6[3, 0, 4] = 0.5
genSU4_6[3, 1, 0] = 0.5
genSU4_6[3, 1, 5] = 0.5
genSU4_6[3, 4, 0] = 0.5
genSU4_6[3, 4, 5] = 0.5
genSU4_6[3, 5, 1] = 0.5
genSU4_6[3, 5, 4] = 0.5
genSU4_6[4, 0, 1] = 0.5j
genSU4_6[4, 0, 4] = -0.5j
genSU4_6[4, 1, 0] = -0.5j
genSU4_6[4, 1, 5] = -0.5j
genSU4_6[4, 4, 0] = 0.5j
genSU4_6[4, 4, 5] = 0.5j
genSU4_6[4, 5, 1] = 0.5j
genSU4_6[4, 5, 4] = -0.5j
genSU4_6[5, 1, 1] = 1.0
genSU4_6[5, 4, 4] = -1.0
genSU4_6[6, 1, 2] = -0.5
genSU4_6[6, 1, 3] = 0.5
genSU4_6[6, 2, 1] = -0.5
genSU4_6[6, 2, 4] = 0.5
genSU4_6[6, 3, 1] = 0.5
genSU4_6[6, 3, 4] = -0.5
genSU4_6[6, 4, 2] = 0.5
genSU4_6[6, 4, 3] = -0.5
genSU4_6[7, 1, 2] = 0.5
genSU4_6[7, 1, 3] = 0.5
genSU4_6[7, 2, 1] = 0.5
genSU4_6[7, 2, 4] = 0.5
genSU4_6[7, 3, 1] = 0.5
genSU4_6[7, 3, 4] = 0.5
genSU4_6[7, 4, 2] = 0.5
genSU4_6[7, 4, 3] = 0.5
genSU4_6[8, 0, 0] = 1.0
genSU4_6[8, 5, 5] = -1.0
genSU4_6[9, 0, 2] = -0.5j
genSU4_6[9, 0, 3] = -0.5j
genSU4_6[9, 2, 0] = 0.5j
genSU4_6[9, 2, 5] = 0.5j
genSU4_6[9, 3, 0] = 0.5j
genSU4_6[9, 3, 5] = 0.5j
genSU4_6[9, 5, 2] = -0.5j
genSU4_6[9, 5, 3] = -0.5j
genSU4_6[10, 0, 1] = -0.5j
genSU4_6[10, 0, 4] = -0.5j
genSU4_6[10, 1, 0] = 0.5j
genSU4_6[10, 1, 5] = -0.5j
genSU4_6[10, 4, 0] = 0.5j
genSU4_6[10, 4, 5] = -0.5j
genSU4_6[10, 5, 1] = 0.5j
genSU4_6[10, 5, 4] = 0.5j
genSU4_6[11, 0, 1] = -0.5
genSU4_6[11, 0, 4] = 0.5
genSU4_6[11, 1, 0] = -0.5
genSU4_6[11, 1, 5] = 0.5
genSU4_6[11, 4, 0] = 0.5
genSU4_6[11, 4, 5] = -0.5
genSU4_6[11, 5, 1] = 0.5
genSU4_6[11, 5, 4] = -0.5
genSU4_6[12, 0, 2] = 0.5
genSU4_6[12, 0, 3] = -0.5
genSU4_6[12, 2, 0] = 0.5
genSU4_6[12, 2, 5] = 0.5
genSU4_6[12, 3, 0] = -0.5
genSU4_6[12, 3, 5] = -0.5
genSU4_6[12, 5, 2] = 0.5
genSU4_6[12, 5, 3] = -0.5
genSU4_6[13, 1, 2] = 0.5j
genSU4_6[13, 1, 3] = -0.5j
genSU4_6[13, 2, 1] = -0.5j
genSU4_6[13, 2, 4] = -0.5j
genSU4_6[13, 3, 1] = 0.5j
genSU4_6[13, 3, 4] = 0.5j
genSU4_6[13, 4, 2] = 0.5j
genSU4_6[13, 4, 3] = -0.5j
genSU4_6[14, 1, 2] = 0.5j
genSU4_6[14, 1, 3] = 0.5j
genSU4_6[14, 2, 1] = -0.5j
genSU4_6[14, 2, 4] = 0.5j
genSU4_6[14, 3, 1] = -0.5j
genSU4_6[14, 3, 4] = 0.5j
genSU4_6[14, 4, 2] = -0.5j
genSU4_6[14, 4, 3] = -0.5j


# fundamental irrep for any N, general formula (generalization of Gell-Mann)
genN = [None] * 10
genN[2] = pauliMat
for N in range(3, 7):
    genN[N] = np.zeros((N**2 - 1, N, N), dtype=complex)
    k = (N - 1) ** 2 - 1
    genN[N][:k, : N - 1, : N - 1] = genN[N - 1]
    for i in range(N - 1):
        genN[N][k + 2 * i, [i, N - 1], [N - 1, i]] = 1, 1
        genN[N][k + 2 * i + 1, [i, N - 1], [N - 1, i]] = -1j, 1j
    genN[N][-1] = np.diag([1] * (N - 1) + [1 - N]) * np.sqrt(2 / (N * (N - 1)))
