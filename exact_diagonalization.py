import numpy as np
import scipy.sparse as sp
import scipy.linalg as lg
from itertools import chain  # fastest way to flatten list of list

# Exact diagonalization algorithms, from the least to the most symmetric operator


def check_ED(H, spec, basis, disp=False):
    """
    Check the given basis is unitary and diagonalizes the Hamiltonian.
    Not all eigenvectors are needed and basis need not to be square.
    """
    if sp.issparse(H) and sp.issparse(basis):
        mcheck = sp.eye(spec.shape[0])
        r1 = lg.norm((basis.getH() @ basis - mcheck).data)
        mcheck.data *= spec
        r2 = lg.norm((basis.getH() @ H @ basis - mcheck).data)
    else:
        r1 = lg.norm(basis.T.conj() @ basis - np.eye(spec.shape[0]))
        r2 = lg.norm(H @ basis - basis * spec)  # since basis is unitary, optimize
    if disp:  # display value in orange
        print(f"norm(basis.H @ basis - Id) = \033[33m{r1:.1e}\033[0m")
        print(f"norm(basis.H @ H @ basis - diag(spec)) = \033[33m{r2:.1e}\033[0m")
    return r1, r2


def Lie(A, B):
    return A @ B - B @ A


def codiagh(*matrices, tol=1e-11):
    """
    Codiagonalise a given set of Hermitian matrices that commute with each other. No
    symmetry is assumed.

    Parameters
    ----------
    matrices : sequence of (N, N) ndarrays
      Real symmetric or complex Hermitian matrices to codiagonalize. They have to
      commute.
    tol : float, optional
      Tolerance to consider two eigenvalues degenerated.

    Returns
    -------
    eigVals : (len(matrices),N) float ndarray
      Eigenvalues of the input matrices.
    basis : (N,N) ndarray
      Codiagonalization basis of matrices.
    """

    N = matrices[0].shape[0]
    eigVals = np.empty((len(matrices), N))
    for i, mat in enumerate(matrices):  # input validation
        assert mat.shape == (N, N), "Matrix number {} has non-consistent shape".format(
            i
        )
    eigVals[0], basis = lg.eigh(matrices[0])  # diagonalize first matrix
    for n, mat in enumerate(matrices[1:]):
        # diagonalize each matrix in the blocks of the previous matrices eigenspaces
        # intersections. Cast to complex matrix only if needed.
        if np.isrealobj(basis) and np.iscomplexobj(mat):
            basis = basis.astype(complex)  # different data type sizes => copy needed
        m = mat.astype(basis.dtype, copy=False)  # cast only once
        bInds = [
            0,
            *(np.abs(eigVals[: n + 1, 1:] - eigVals[: n + 1, :-1]) > tol)
            .any(axis=0)
            .nonzero()[0]
            + 1,
            N,
        ]  # list of eigenspaces blocks indices
        for i, j in zip(bInds, bInds[1:]):  # diagonalize blocks
            eigVals[n + 1, i:j], vec = lg.eigh(
                basis[:, i:j].T.conj() @ m @ basis[:, i:j]
            )
            basis[:, i:j] = basis[:, i:j] @ vec  # update basis
    assert (
        lg.norm(basis.T.conj() @ basis - np.eye(N)) < tol
    ), "final basis is not unitary"
    return eigVals, basis


def diagU1(H, site_colors, eigvals_only=False):
    """
    Diagonalize a real symmetric or complex Hermitian U(1)^ncol symmetric operator
    acting on Nv sites using U(1) symmetry. No additional symmetry is assumed, neither
    lattice or SU(N).

    Parameters
    ----------
    H : (M,M) ndarray or sparse matrix
      Real symmetric or complex Hermitian U(1)^ncol-symmetric Hamiltonian to
      diagonalize.
    site_colors : list of size Nv made of integer array of shape (d_i,ncol)
      U(1) colors of each site. Typically the N-1 Cartan values for SU(N). The number of
      colors ncol has to be the same on every sites but the number of vectors d_i
      (=dimension of local basis) may change.
    eigvals_only : bool, optional
      Whether to calculate only eigenvalues and no eigenvectors.

    Returns
    -------
    spec : (M,) float ndarray
      Spectrum of H, sorted by algebraic value.
    colors : (M,ncol) int8 ndarray
      Colors of the eigenvectors of H.
    if eigvals_only is False:
    basis : (M, M) matrix, same density and dtype as H.
      Diagonalization basis.
    """
    M = H.shape[0]
    if H.shape != (M, M):
        raise ValueError("Expected square matrix")

    # construct color of every state from local site colors
    ncol = site_colors[0].shape[1]
    colors = site_colors[0]
    for c in site_colors[1:]:
        colors = (colors[:, None] + c).reshape(len(colors) * len(c), ncol)

    if len(colors) != M:
        raise ValueError("incompatible H and colors shapes")

    # find color blocks  (faster than np.diff)
    colorOrder = np.lexsort(colors.T)
    colors = np.ascontiguousarray(colors[colorOrder])
    bInds = [0, *((colors[:-1] != colors[1:]).any(axis=1).nonzero()[0] + 1), M]

    # diagonalize blocks
    sparse = sp.issparse(H)
    spec = np.empty(M)
    if not eigvals_only:
        if sparse:
            basis = sp.dok_matrix((M, M), dtype=H.dtype)
        else:
            basis = np.zeros((M, M), dtype=H.dtype)

    for i, j in zip(bInds, bInds[1:]):
        m = H[colorOrder[i:j, None], colorOrder[i:j]]
        if sparse:
            m = m.toarray()
        if eigvals_only:
            spec[i:j] = lg.eigvalsh(m, overwrite_a=True)
        else:
            spec[i:j], basis[colorOrder[i:j], i:j] = lg.eigh(m, overwrite_a=True)

    if eigvals_only:
        return spec, colors
    if sparse:
        basis = basis.tocsr()
    return spec, colors, basis


def diagU1trans(
    H,
    Nv,
    siteColors,
    alternate=False,
    colorMajor=True,
    eigvals_only=False,
    col_sector=None,
):
    """
    Diagonalize a 1D translation invariant and SU(N) symmetric Hamiltonian acting on Nv
    sites. Codiagonalize H with Cartan operators and impulsion.

    Parameters
    ----------
    H : (D**Nv, D**Nv) ndarray or sparse matrix
      Real symmetric or complex Hermitian Hamiltonian to diagonalize. D is the dimension
      of the local variable on a given site.
    Nv : positive integer
      Number of sites on which the Hamiltonian acts.
    siteColors: (D,ncol) integer ndarray
      SU(N) colors of each site of the product basis. For SU(N), there are N-1 Cartan
      operators, but there may be more colors for additional symmetry.
    alternate: bool, optional
      If True, consider staggered conjugate representations of SU(N) on odd and even
      sites. The system is invariant by a 2-sites translation only.
    colorMajor: bool, optional
      If alternate is True, colors and momentum cannot be codiagonalized. If True,
      codiagonalize colors and translation over 2 sites (only half of k sectors). If
      False, define colors up to a global sign and consider all k sectors.
    eigvals_only: bool, optional
      Whether to calculate only eigenvalues and no eigenvectors.
    col_sector: (ncol,) integer array-like, optional
      Only consider one color sector. If None, all sectors are diagonalized.

    Returns
    -------
    spec : (Nvec,) float ndarray
      Spectrum of H, ordered first by colors, then by momentum, finally by energy. Nvec
      is the dimension of the color sector, D**Nv if col_sector is None.
    momentum : (Nvec,) float ndarray
      Momentum of the eigenvectors of H, ordered from -pi+2pi/Nv to pi in each color
      sector. If alternate and colorMajor, defined for 2-site translation.
    colors : (Nvec,ncol) int8 ndarray
      Colors of the eigenvectors of H. If alternate and not colorMajor, defined only up
      to a sign.
    if eigvals_only is False:
    basis : (D**Nv, Nvec) matrix, sparse if H is sparse
      Diagonalization basis. Data type is real if impulsion are only 0 and pi and H is
      real, else complex.
    """

    # Algorithm:
    # First select color blocks by permuting canonical basis and stay inside them
    # then construct cycles [a,b,c] -> [b,c,a] -> [c,a,b]
    # construct vectors with fixed impulsion by Fourier transform of cycles
    #
    # in this basis, translation is diagonal, colors are diagonal; blocks are
    # ---------------
    # |k1k1         |
    # |k1k1         |
    # |    k2k2k2   |
    # |    k2k2k2   |  color 1
    # |    k2k2k2   |
    # |           k3|
    # |--------------------
    #               | k1  |
    #               |   k2|  color 2
    #               -------
    #
    # diagonalize H in these blocks, then finally reorder states
    # to get blocks of SU(N) multiplets (many multiplets can be degenerated)
    # with fixed energies and momentum.

    # 0) input check
    D = siteColors.shape[0]
    sparse = sp.issparse(H)
    if H.shape != (D**Nv, D**Nv):
        raise ValueError("inconsistant shape for H")
    if alternate and Nv % 2:
        raise ValueError("cannot be alternate with odd site number")

    # 1) find color blocks
    ntrans = 1
    altNv = Nv
    baseD = D ** np.array(
        [np.arange(i + Nv, i, -1) % Nv for i in reversed(range(0, Nv, ntrans))]
    )
    vec = np.arange(D**Nv)[:, None] // baseD[0] % D  # decomp can basis
    if alternate:  # staggered color signs
        colors = siteColors[vec].swapaxes(1, 2) @ (
            np.arange(Nv, dtype=np.int8) % 2 * 2 - 1
        )
        if colorMajor:  # impulsion is only defined modulo pi
            ntrans = 2
            altNv = Nv // 2
        else:  # colors are only defined up to a sign
            for c in colors:
                nn = c.nonzero()[0]
                if len(nn):
                    c *= np.sign(colors[nn[0]])  # set sign of first non-zero to +
    else:
        colors = siteColors[vec].sum(axis=1, dtype=np.int8)
    del vec
    if col_sector is None:  # consider all color sectors
        colorOrder = np.lexsort(colors.T)  # lexsort is stable
    else:  # consider only 1 color sector
        colorOrder = (colors == col_sector).all(axis=1).nonzero()[0]
    Nvec = len(colorOrder)  # number of eigenvalues to compute, default D**Nv
    colors = np.ascontiguousarray(colors[colorOrder])
    cblocks = [0, *((colors[:-1] != colors[1:]).any(axis=1).nonzero()[0] + 1), Nvec]

    # 2) define and precompute stuff before color block splitting
    # fix types (casting can be time expensive)
    if H.dtype == np.complex128:
        dtype = np.complex128

        def fix_h_dtype(h):
            return h, h

    elif H.dtype == np.complex64:
        dtype = np.complex128

        def fix_h_dtype(h):
            h1 = h.astype(np.complex128)
            return h1, h1

    elif H.dtype == np.float64:
        if altNv < 2:
            dtype = np.float64

            def fix_h_dtype(h):
                return h, None  # no hc used if k restricted to 0,pi

        else:
            dtype = np.complex128

            def fix_h_dtype(h):
                return h, h.astype(np.complex128)

    else:
        if altNv < 2:
            dtype = np.float64

            def fix_h_dtype(h):
                return h.astype(np.float64), None  # no hc used if k restricted to 0,pi

        else:
            dtype = np.complex128

            def fix_h_dtype(h):
                h1 = h.astype(np.float64)
                return h1, h1.astype(np.complex128)

    ksectors = range(1 - altNv % 2 - altNv // 2, altNv // 2 + 1)  # shift for +pi
    if altNv < 3:  # real dtype
        fourierMat = [
            None,
            np.ones((1, 1)),
            np.array([[1.0, 1.0], [1.0, -1.0]]) / np.sqrt(2),
        ]
    else:
        fourierMat = [None] * (altNv + 1)  # precompute fourier transform matrices
        for lc in range(1, int(np.sqrt(altNv)) + 1):
            if altNv % lc == 0:  # cycle length divises Nv (Lagrange)
                div = altNv // lc
                fourierMat[lc] = np.fft.fft(np.eye(lc), norm="ortho")
                fourierMat[div] = np.fft.fft(np.eye(div), norm="ortho")

    spec = np.empty(Nvec)
    momentum = np.empty(Nvec)
    if not eigvals_only:  # consider eigenvectors in col_sector onlt
        basis = sp.dok_matrix((D**Nv, Nvec), dtype=dtype)

    # 3) construct cycles of transposed states. Never get out of a color block
    for i1, i2 in zip(cblocks, cblocks[1:]):
        # construct Hamiltonian in color sector and cast it only once
        h, hc = fix_h_dtype(H[colorOrder[i1:i2, None], colorOrder[i1:i2]])

        # Only one loop over cycles, but need to construct whole and complex kBasis,
        # no efficient way to construct only some ksectors here - or to use directly
        # reals for k=0,pi
        ind = 0
        kblocks = [[] for k in ksectors]  # blocks of impulsion k in ]-Nv/2,Nv/2]*2pi
        kBasis = sp.dok_matrix(
            h.shape, dtype=dtype
        )  # diagonalization basis for impulsion
        cycles = []
        states_sec = set(colorOrder[i1:i2])  # states in the considered sector
        while states_sec:
            cyc = states_sec.pop() // baseD % D @ baseD[0]
            lc = altNv
            for i, s in enumerate(cyc[1:]):
                if s == cyc[0]:  # if cycle length < Nv
                    lc = i + 1
                    cyc = cyc[:lc]
                    break
                states_sec.remove(s)
            cycles.extend(cyc)
            kBasis[ind : ind + lc, ind : ind + lc] = fourierMat[lc]
            for i in range(lc):
                k = (
                    i * altNv // lc + (altNv - 1) // 2
                ) % altNv  # shift to start at pi-2pi/Nv
                kblocks[k].append(ind)  # keep track where is which state
                ind += 1

        # 4) digaonalize thoses cycles using Fourier transfer matrix

        # permute basis lines to fit cycles and columns to sort by momentum
        # kBasis.getH() and kBasis will be 1 csc and 1 csr. Choose csc for kBasis for
        # easy col slicing + dense @ sparse is (sparse.T @ dense.T).T
        kBasis = kBasis.tocsc()[
            np.argsort(cycles)[:, None], list(chain.from_iterable(kblocks))
        ]
        del cycles

        # 5) diagonalize matrix in color and k-basis
        j = 0
        for k in ksectors:
            dimk = len(kblocks[k + (altNv - 1) // 2])
            if dimk:  # k may not exist in this color sector
                i, j = j, j + dimk
                momentum[i1 + i : i1 + j] = 2 * k * np.pi / altNv
                if (
                    k == 0 or k == altNv / 2
                ):  # odd altNv => no pi sector, integer k is never altNv/2
                    kProj = kBasis[
                        :, i:j
                    ].real.copy()  # real projectors on 0,pi sectors
                    m = kProj.T @ h @ kProj
                else:
                    kProj = kBasis[:, i:j]  # projector on k sector
                    m = kProj.getH() @ hc @ kProj  # complex stuff

                if sparse:  # else product with dense h created dense m
                    m = m.toarray()
                if eigvals_only:
                    spec[i1 + i : i1 + j] = lg.eigvalsh(m, overwrite_a=True)
                else:
                    spec[i1 + i : i1 + j], vec = lg.eigh(m, overwrite_a=True)
                    basis[colorOrder[i1:i2], i1 + i : i1 + j] = (
                        kProj @ vec
                    )  # construct diag basis

    if eigvals_only:
        return spec, momentum, colors
    if sparse:
        basis = basis.tocsr()
    else:
        basis = basis.toarray()
    return spec, momentum, colors, basis


def construct_reflexion_matrix(n, shift=False):
    """
    Construct reflection matrix of size n. Columns are eigenvectors of reflection. First
    column is even k=0 state. If n is even, the last column is even sectors with k=pi
    (no odd)
    The other columns are symmetric and anti-sym combination of k,-k vectors.
    shift allows to shift starting angle by -k*pi/n.
    """
    m = np.empty((n, n))
    m[:, 0] = 1  # normalization is not the same
    temp = np.arange(-shift, 2 * n - shift, 2) * np.pi / n
    for k in range(1, (n + 1) // 2):
        m[:, 2 * k - 1] = np.cos(k * temp) * np.sqrt(2)  # (|k>+|-k>)/sqrt(2)
        m[:, 2 * k] = np.sin(k * temp) * np.sqrt(2)  # (|k>-|-k>)/i/sqrt(2)
    if not n % 2:  # k = pi, no sin
        m[:, -1] = 1 - np.arange(n) % 2 * 2  # different normalization
    m[np.abs(m) < 1e-14] = 0  # plenty of 0
    # (0,cos(2pi/n),sin(2pi/n),cos(4pi/n),sin(4pi/n),...,cos(pi))
    return m / np.sqrt(n)


def reflexion2cycles(n):
    """
    Construct reflection matrix of 2 conjugate cycles of same length.
    """
    # |k_cyc> + |-k_cyc> + |k_cycR> + |-k_cycR> /2
    # |k_cyc> - |-k_cyc> + |k_cycR> - |-k_cycR> /2i
    # |k_cyc> + |-k_cyc> - |k_cycR> - |-k_cycR> /2
    # |k_cyc> - |-k_cyc> - |k_cycR> + |-k_cycR> /2i
    R = construct_reflexion_matrix(n) / np.sqrt(2)
    m = np.empty((2 * n, 2 * n))
    m[:n, :n] = R
    m[n:, :n] = R
    m[:n, n:] = R
    m[n:, n:] = -R
    # (0,e),[(cos k,e),(sin k,e)],(cos pi,e),(0,o),[(cos k,o),(sin k,e)],(cos pi,o)
    return m


def diagU1refl(H, Nv, siteColors, eigvals_only=False, col_sector=None, verbosity=0):
    """
    Diagonalize a 1D translation and reflection invariant and SU(N) symmetric
    Hamiltonian acting on Nv sites. Codiagonalize H with Cartan operators and
    reflection. Only accept real Hamiltonians (expect complex ones to break reflection)

    Parameters
    ----------
    H : (D**Nv, D**Nv) ndarray or sparse matrix
      Real symmetric Hamiltonian to diagonalize. D is the dimension of the local
      variable on each site.
    Nv : positive integer
      Number of sites on which the Hamiltonian acts.
    siteColors: (D,ncol) integer ndarray
      SU(N) colors of each site of the product basis. For SU(N), there are N-1 Cartan
      operators, but there may be more colors for additional symmetry.
    eigvals_only: bool, optional
      Whether to calculate only eigenvalues and no eigenvectors.
    col_sector: (ncol,) integer array-like, optional
      Only consider one color sector. If None, all sectors are diagonalized.
    verbosity: integer, optional.
      Amount of information to display. Default is 0.

    Returns
    -------
    spec : (Nvec,) float ndarray
      Spectrum of H, ordered first by colors, then by momentum, finally by energy.
      Nvec is the dimension of the color sector, D**Nv if col_sector is None.
    momentum : (Nvec,) float ndarray
      Momentum of the eigenvectors of H, defined up to a sign for k!=0,pi (even and odd
      combinations of |k>, -|k>)
    reflection : (Nvec,) int8 ndarray
      Eigenvalues +/- 1 of the reflection operator acting on H eigenvectors
    colors : (Nvec,ncol) int8 ndarray
      Colors of H eigenvectors.
    if eigvals_only is False:
    basis : (D**Nv, Nvec) float matrix, sparse if H is sparse
      Diagonalization basis.
    """

    # Algorithm:
    # First select color blocks by permuting canonical basis and stay inside them
    # then construct translation cycles [a,b,c] -> [b,c,a] -> [c,a,b]
    # and the reflected cycle [R.a,R.b,R.c]
    # 3 cases to consider:
    #   1. cyc = cycR and there exists 1 (odd cycle length) or 2 (even) fixed pts
    #      then shift cycle to set fixed point as 1st state: reflexion axis
    #      becomes x=0 axis, cos(k) is even and sin(k) is odd under reflection
    #   2. cyc = cycR and there is no fixed point.
    #      then shift cycle to set x=0 reflexion axis between 2 neigboring states
    #      and consider even cos(k+k*pi/Nv) and odd sin(k+k*pi/Nv)
    #   3. cyc != cycR
    #      simply construct cos(k) and sin(k) even and odd blocks acting on 2
    #      cycles. Note that T.R.a = R.c: reflection changes k sign in cycR.
    #
    # use reflection matrices with cos(k) and sin(k) columns to construct vectors
    # with fixed |k| and reflection eigenvalue
    #
    # in this basis, reflexion is diagonal and impulsion is defined up to a sign
    # -------------------------------------
    # |[k=0]                              |
    # |[r=1]                              |
    # |     [+/-2kpi/Nv]                  |
    # |     [    r=1   ]                  |
    # |     [          ]                  |
    # |                 [k=0 ]            |  color 1
    # |                 [r=-1]            |
    # |                       [+/-2kpi/Nv]|
    # |                       [   r=-1   ]|
    # |                       [          ]|
    # |------------------------------------------
    #                                     | kr  |
    #                                     |   kr|  color 2
    #                                     -------
    #
    # Performances:
    # for eigvals_only=True, more than 95% of time is inside m.toarray() + eigvalsh(m)
    # expect even more for eigenvectors computation, hence optimization is not
    # really needed in blocks and kr_basis construction.
    #
    # Possible enhancements:
    # 1) could specify k sector, but only if col_sector is specified
    #    else Nvec cannot be known before entering color loop
    #    even if k does not exist in col_sector, no crash
    #    yet still need to define Nvec afterwards or to refactor code.
    # 2) could compute (sparse) matrix in a given block and return it to give
    #    it as input to any diagonalization method - typically Lanczos. Note that
    #    block computation is cheap and k=0 and k=pi sectors are small since
    #    they are splitted between odd and even, therefore much larger sizes can
    #    be obtained if we only want GS (expected to lie in these sectors).
    # 3) could implement charge-conjugation symmetry. For SU(2), element of SU(2)
    #    that sends Sz to -Sz (only consider half of Sz values) and split Sz=0
    #    block into 2 even and odd parts. Tricky for SU(N), not a groupe element
    #    and only works for self-conjugate local representations.
    # 4) could construct matrix directly into color blocks instead of splitting a
    #    very large sparse matrix that had to be constructed first. Not very well
    #    suited for python and model-dependant, requires a specialized code.
    # 5) for k != 0,pi, vectors |±k,even> = |cosk> and|±k,odd> = |sink> belong to
    #    2-dimensional irrep E. No need to diagonalize it two time: just generate
    #    the first vector block and get the second by acting with translation as
    #    (basis[T,i1:i2] - np.cos(momentum[i1])*basis[:,i1:i2])/np.sin(momentum[i1]),
    #    T = np.arange(D**Nv)[:,None]//D**((1+np.arange(Nv))%Nv)%D @ D**np.arange(Nv)
    #    or (basis[T,i1:i2] - basis[T^-1,i1:i2])/(2*np.sin(momentum[i1]))
    #    no need to even construct the block. Would need to adapt reflection matrices
    #    the whole odd sectors with k != 0,pi disappears.
    #    maybe construct |cosk> + |sink> in reflMat and use a reflection to get
    #    2nd vector |cosk> - |sink> (avoid dividing by sin(k))
    #    then no more eigenvalue of reflection and refl is not defined
    #    or refl is just the character and set to 0.

    # 0) input check
    D = siteColors.shape[0]
    sparse = sp.issparse(H)
    if H.dtype != np.float64:
        raise TypeError("function only accepts float64 dtype")
    if H.shape != (D**Nv, D**Nv):
        raise ValueError("inconsistant shape for H")

    # 1) find color blocks
    baseD = D ** np.array([np.arange(i + Nv, i, -1) % Nv for i in reversed(range(Nv))])
    colors = siteColors[np.arange(D**Nv)[:, None] // baseD[0] % D].sum(
        axis=1, dtype=np.int8
    )
    if col_sector is None:  # consider all color sectors
        colorOrder = np.lexsort(colors.T)  # lexsort is stable
    else:  # consider only 1 color sector
        colorOrder = (colors == col_sector).all(axis=1).nonzero()[0]
    Nvec = len(colorOrder)  # number of eigenvalues to compute, default D**Nv
    colors = np.ascontiguousarray(colors[colorOrder])
    cblocks = [0, *((colors[:-1] != colors[1:]).any(axis=1).nonzero()[0] + 1), Nvec]

    # 2) define and precompute stuff before color block splitting
    kr_sectors = range(Nv + 2 - Nv % 2)  # sorting: (0,e), (+/-k,e), (0,o), (+/-k,o)
    reflMat2cyc = [None] * (Nv + 1)  # precompute reflection diagonalization basis
    reflMat = [None] * (Nv + 1)  # self-reflected cycles
    reflMatShift = [None] * (Nv + 1)  # self-reflected cycle with shifted origin
    for lc in range(1, int(np.sqrt(Nv)) + 1):
        div, mod = divmod(Nv, lc)
        if mod == 0:  # cycle length divises Nv (Lagrange)
            reflMat[lc] = construct_reflexion_matrix(lc)
            reflMat[div] = construct_reflexion_matrix(div)
            reflMatShift[lc] = construct_reflexion_matrix(lc, shift=True)
            reflMatShift[div] = construct_reflexion_matrix(div, shift=True)
            reflMat2cyc[lc] = reflexion2cycles(lc)
            reflMat2cyc[div] = reflexion2cycles(div)

    spec = np.empty(Nvec)
    momentum = np.empty(Nvec)
    reflection = np.empty(Nvec, dtype=np.int8)
    if not eigvals_only:  # consider eigenvectors in col_sector only
        basis = sp.dok_matrix((D**Nv, Nvec))

    # 3) construct cycles of transposed states. Never get out of a color block
    for i1, i2 in zip(cblocks, cblocks[1:]):
        if verbosity > 0:
            print(f"color = {colors[i1]}, dimension {i2-i1}")
        ind = 0
        kr_blocks = [[] for k in kr_sectors]  # blocks with fixed r and |k|
        kr_basis = sp.dok_matrix((i2 - i1, i2 - i1))  # diagonalization basis
        cycles = []
        states_sec = set(colorOrder[i1:i2])  # states in this color sector
        while states_sec:
            siteStates = states_sec.pop() // baseD % D  # cycle in product basis
            cyc = siteStates @ baseD[0]
            cyc_set = set(cyc)
            states_sec -= cyc_set
            lc = len(cyc_set)
            cyc = cyc[:lc]
            cycR = siteStates[:lc, ::-1] @ baseD[0]  # reflected cycle
            if cycR[0] in cyc:  # cycle is self-reflected
                # shift to set reflexion axis as x=0 axis
                fixed_points = (cyc == cycR).nonzero()[0]
                if fixed_points.size:  # there exists a fixed point
                    shift = fixed_points[0]  # shift to set fixed point as origin x=0
                    kr_basis[ind : ind + lc, ind : ind + lc] = reflMat[lc]
                    index_pi = Nv // 2  # irrep B1
                else:  # there is no fixed point
                    shift = (cyc[:-1] == cycR[1:]).argmax()  # refl. axis between 2 pts
                    kr_basis[ind : ind + lc, ind : ind + lc] = reflMatShift[lc]
                    index_pi = -1  # irrep B2
                kr_blocks[0].append(ind)  # k=0, even = irrep A1
                ind += 1
                for i in range(1, (lc + 1) // 2):  # irreps E_k
                    kr_blocks[i * Nv // lc].append(ind)  # cos k, even
                    kr_blocks[i * Nv // lc + Nv // 2 + 1].append(ind + 1)  # sin k, odd
                    ind += 2
                if not lc % 2:  # pi, even/odd = irreps B1 or B2
                    kr_blocks[index_pi].append(ind)
                    ind += 1
                cycles.extend(
                    cyc[np.arange(shift, lc + shift) % lc]
                )  # remember cycle order
            else:  # cycR != cyc: 2-cycles matrix
                states_sec -= set(cycR)
                cycles.extend(cyc)
                cycles.extend(cycR)
                kr_basis[ind : ind + 2 * lc, ind : ind + 2 * lc] = reflMat2cyc[lc]
                kr_blocks[0].append(ind)  # k=0, even = irrep A1
                kr_blocks[Nv // 2 + 1].append(ind + lc)  # k=0, odd = irrep A2
                ind += 1
                for i in range(1, (lc + 1) // 2):  # irreps E_k
                    kr_blocks[i * Nv // lc].append(ind)  # cos(k), cos(k) => even
                    kr_blocks[i * Nv // lc].append(ind + 1)  # sin(k), sin(-k) => even
                    kr_blocks[i * Nv // lc + Nv // 2 + 1].append(
                        ind + lc
                    )  # cos(k), -cos(-k) => odd
                    kr_blocks[i * Nv // lc + Nv // 2 + 1].append(
                        ind + lc + 1
                    )  # sin(k), -sin(-k) => odd
                    ind += 2
                if not lc % 2:
                    kr_blocks[Nv // 2].append(ind)  # pi, even
                    kr_blocks[-1].append(ind + lc)  # pi, odd
                    ind += 1
                ind += lc
        assert ind == i2 - i1, "state enumeration is wrong"

        # 4) Construct basis change matrix to blocks of fixed |k| and r
        kr_basis = kr_basis.tocsc()[
            np.argsort(cycles)[:, None], list(chain.from_iterable(kr_blocks))
        ]
        del cycles

        # 5) diagonalize matrix in color and kr-basis
        h = H[colorOrder[i1:i2, None], colorOrder[i1:i2]]  # H in color sector
        j = 0
        for kr in kr_sectors:
            dimkr = len(kr_blocks[kr])
            if dimkr:  # k may not exist in this color sector
                i, j = j, j + dimkr
                momentum[i1 + i : i1 + j] = (
                    kr % (Nv // 2 + 1) * 2 * np.pi / Nv
                )  # defined UP TO A SIGN
                reflection[i1 + i : i1 + j] = 1 - kr // (Nv // 2 + 1) * 2
                kr_proj = kr_basis[:, i:j]  # projector on k sector
                if verbosity > 1:
                    print(
                        f"mom = ±{kr%(Nv//2+1)}*2pi/{Nv},",
                        f"refl = {reflection[i1+i]}, dimkr = {dimkr}",
                    )
                m = kr_proj.T @ h @ kr_proj
                if sparse:  # else product with dense h created dense m
                    m = m.toarray()
                if eigvals_only:
                    spec[i1 + i : i1 + j] = lg.eigvalsh(m, overwrite_a=True)
                else:
                    spec[i1 + i : i1 + j], vec = lg.eigh(m, overwrite_a=True)
                    basis[colorOrder[i1:i2], i1 + i : i1 + j] = kr_proj @ vec

    assert i1 + j == Nvec, "Number of state is invalid"
    if eigvals_only:
        return spec, momentum, reflection, colors
    if sparse:
        basis = basis.tocsr()
    else:
        basis = basis.toarray()
    return spec, momentum, reflection, colors, basis
