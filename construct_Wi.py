import numpy as np
import scipy.linalg as lg
from exact_diagonalization import diagU1refl
from gen_SUN import genN
from sym_tensors_tools import rep_C4v, sum_4sites, c2_4sites


gen6 = genN[6].copy()
del genN

D = 13
Nv = 4
g66b1 = np.zeros((35, D, D), dtype=complex)
g66b1[:, :6, :6] = gen6
g66b1[:, 6:12, 6:12] = -gen6.conj()

colors6 = np.array(
    [
        [1, 0, 0, 0, 0],
        [-1, 1, 0, 0, 0],
        [0, -1, 1, 0, 0],
        [0, 0, -1, 1, 0],
        [0, 0, 0, -1, 1],
        [0, 0, 0, 0, -1],
    ],
    dtype=np.int8,
)

colors66b1 = np.zeros((D, 7), dtype=np.int8)
colors66b1[:6, :5] = colors6
colors66b1[6:12, :5] = -colors6
colors66b1[:12, 5] = [1] * 6 + [-1] * 6  # easily count number of 6 and 6b
colors66b1[12, 6] = 1

cas = c2_4sites(g66b1)
spec3h, mom3h, refl3h, col3h, basis3h = diagU1refl(
    cas, 4, colors66b1, col_sector=np.array([1, 0, 0, 0, 0, 1, 3])
)
spec1h, mom1h, refl1h, col1h, basis1h = diagU1refl(
    cas, 4, colors66b1, col_sector=np.array([1, 0, 0, 0, 0, 1, 1])
)

print(
    f"exactify spec: {lg.norm(np.rint(spec1h*3)/3 - spec1h):.1e},",
    f"{lg.norm(np.rint(spec3h*3)/3 - spec3h):.1e}",
)
spec1h = np.rint(spec1h * 3) / 3
spec3h = np.rint(spec3h * 3) / 3

print("select irrep 6 and momentum 0")
sRVB_0 = np.logical_not(mom3h.astype(bool) | (spec3h - 35 / 3).astype(bool)).nonzero()[
    0
]
sA1_1h = np.logical_not(
    mom1h.astype(bool) | (refl1h - 1).astype(bool) | (spec1h - 35 / 3).astype(bool)
).nonzero()[0]
sA2_1h = np.logical_not(
    mom1h.astype(bool) | (refl1h + 1).astype(bool) | (spec1h - 35 / 3).astype(bool)
).nonzero()[0]

print(f"{len(sA1_1h)} tensors A1\n{len(sA2_1h)} tensors A2")

pRVB_0 = basis3h[:, sRVB_0].toarray()
pW_A1_0 = basis1h[:, sA1_1h].toarray()
pW_A2_0 = basis1h[:, sA2_1h].toarray()

# act with Sminus to get other vectors.
S1m66b1, S2m66b1, S3m66b1, S4m66b1, S5m66b1 = np.zeros((5, D, D))
S1m66b1[1, 0] = 1
S1m66b1[6, 7] = -1
S2m66b1[2, 1] = 1
S2m66b1[7, 8] = -1
S3m66b1[3, 2] = 1
S3m66b1[8, 9] = -1
S4m66b1[4, 3] = 1
S4m66b1[9, 10] = -1
S5m66b1[5, 4] = 1
S5m66b1[10, 11] = -1

S1m4 = sum_4sites(S1m66b1)
pRVB_1 = S1m4 @ pRVB_0
pW_A1_1 = S1m4 @ pW_A1_0
pW_A2_1 = S1m4 @ pW_A2_0

S2m4 = sum_4sites(S2m66b1)
pRVB_2 = S2m4 @ pRVB_1
pW_A1_2 = S2m4 @ pW_A1_1
pW_A2_2 = S2m4 @ pW_A2_1

S3m4 = sum_4sites(S3m66b1)
pRVB_3 = S3m4 @ pRVB_2
pW_A1_3 = S3m4 @ pW_A1_2
pW_A2_3 = S3m4 @ pW_A2_2

S4m4 = sum_4sites(S4m66b1)
pRVB_4 = S4m4 @ pRVB_3
pW_A1_4 = S4m4 @ pW_A1_3
pW_A2_4 = S4m4 @ pW_A2_3

S5m4 = sum_4sites(S5m66b1)
pRVB_5 = S5m4 @ pRVB_4
pW_A1_5 = S5m4 @ pW_A1_4
pW_A2_5 = S5m4 @ pW_A2_4

tRVB = (
    np.array([pRVB_0, pRVB_1, pRVB_2, pRVB_3, pRVB_4, pRVB_5])
    .transpose(2, 0, 1)
    .reshape(6, D, D, D, D)
)

tA1 = (
    np.array([pW_A1_0, pW_A1_1, pW_A1_2, pW_A1_3, pW_A1_4, pW_A1_5])
    .transpose(2, 0, 1)
    .reshape(len(sA1_1h), 6, D, D, D, D)
)

tA2 = (
    np.array([pW_A2_0, pW_A2_1, pW_A2_2, pW_A2_3, pW_A2_4, pW_A2_5])
    .transpose(2, 0, 1)
    .reshape(len(sA2_1h), 6, D, D, D, D)
)

np.savez_compressed("save_t66b1_tensors.npz", tRVB=tRVB, tA1=tA1, tA2=tA2)
print("data saved in file save_t66b1_tensors.npz, keys = tRVB, tA1, tA2")

rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd2 = rep_C4v(D)


def check(A, parity=1):
    Av = np.sqrt(6) * A.reshape(6, D**4) / lg.norm(A)
    print(f"unitary: {lg.norm(Av @ Av.T - np.eye(6)):.1e}")
    print(f"irrep projector: {lg.norm(Av @ cas @ Av.T - 35/3*np.eye(6)):.1e}")
    print(f"rotation invariant: {lg.norm(Av @ rot1 @ Av.T - np.eye(6)):.1e}")
    print(f"reflection invariant: {lg.norm(Av @ sigv1 @ Av.T - parity*np.eye(6)):.1e}")


print("\ncheck RVB:")
check(tRVB)

print("\ncheck tA1:")
for A in tA1:
    check(A)

print("\ncheck tA2:")
for A in tA2:
    check(A, parity=-1)

# TODO: define W1 as S_4 invariant
# split W1 and W2. W3 has differnet structure.
