#!/usr/bin/env python3

###############################################################################
#
# construct SU(2) generalized RVB tensors
# S = 2
# D = 3
# V = 2 + 1
#
###############################################################################

import numpy as np
import scipy.linalg as lg
from exact_diagonalization import diagU1refl
from gen_SUN import pauliMat
from sym_tensors_tools import sum_4sites, c2_4sites


g2p1 = np.zeros((3, 3, 3), dtype=complex)  # generators S of SU(2) on representation 2+1
g2p1[:, :2, :2] = pauliMat / 2
colors2p1 = np.array(
    [[1, 0], [-1, 0], [0, 1]], dtype=np.int8  # first column is 2*Sz
)  # add anther color to select by hole number


cas81 = c2_4sites(g2p1)  # Casimir operator on 4 sites
spec, momentum, refl, colors, basis = diagU1refl(cas81, 4, colors2p1)
# diagU1refl does all the work: it diagonalizes the Casimir in C_4v basis (4-site chain
# with PBC == 2x2 square with PBC). Eigenstates are sorted by rotation
# (linear translation <-> C4v rotation) and reflection. Implusion 0 becomes A1 and A2,
# split with respect to reflection. Same for impulsion pi and B1/B2.

spins = np.rint((-1 + np.sqrt(1 + 4 * spec))) / 2  # s(s+1) --> s
print(lg.norm(spins * (spins + 1) - spec))  # check


s1half_A = np.logical_not(
    momentum.astype(bool) | np.rint(spins * 2 - 1).astype(bool)
).nonzero()[
    0
]  # select C4v A and spin 1/2
vRVB_up = np.intersect1d(
    s1half_A, np.logical_not((colors - np.array([1, 3])).any(axis=1)).nonzero()[0]
)  # select hole number=3, Sz=up
vT1_up = np.intersect1d(
    s1half_A,
    np.logical_not(
        (refl - 1).astype(bool) | (colors - np.array([1, 1])).any(axis=1)
    ).nonzero()[0],
)  # select hole number=1, Sz=up, reflection even
vT2_up = np.intersect1d(
    s1half_A,
    np.logical_not(
        (refl + 1).astype(bool) | (colors - np.array([1, 1])).any(axis=1)
    ).nonzero()[0],
)  # select hole number=1, Sz=up, reflection odd

p0_up = basis[:, vRVB_up].toarray()  # selected eigenvector, only 1 A1
p1_up = basis[:, vT1_up].toarray()  # selected eigenvectors, 1 A1
p2_up = basis[:, vT2_up].toarray()  # selected eigenvectors, 1 A2


# act with Sminus to get second eigenvector with the same phase
Sm2p1 = (g2p1[0] - 1j * g2p1[1]).real.copy()
Sm81 = sum_4sites(Sm2p1)
p0_down = Sm81 @ p0_up
p1_down = Sm81 @ p1_up
p2_down = Sm81 @ p2_up

# exactify tensors
tRVB = np.array([p0_up, p0_down]).reshape(2, 3, 3, 3, 3)
print(lg.norm(tRVB - np.rint(tRVB * 2) / 2))
tRVB = np.rint(tRVB * 2).astype(np.int8)

T_A1 = np.array([p1_up, p1_down]).reshape(2, 3, 3, 3, 3)
T_A2 = np.array([p2_up, p2_down]).reshape(2, 3, 3, 3, 3)

tRVB2_31_A1 = np.rint(T_A1 * 2 * np.sqrt(6)).astype(np.int8)
print(lg.norm(T_A1 - tRVB2_31_A1 / 2 / np.sqrt(6)))
tRVB2_31_A2 = np.rint(T_A2 * np.sqrt(8)).astype(np.int8)
print(lg.norm(T_A2 - tRVB2_31_A2 / np.sqrt(8)))
