#!/usr/bin/env python3

###############################################################################
#
# CTMRG algorithm for SU(6) symmetric tensors Wi
# input: i, i=1,2,3, tensor Wi is used

###############################################################################


import numpy as np
from sys import argv
from CTMRG import sparseCTM
from tensors.tensors_SU6_Wi import W1, W2, W3
from gen_SUN import genSU4_6

if len(argv) < 2:
    print("No input file given, abort")
    exit()

i = int(argv[1])
saveCTM0 = f"data_CTMWi_W{i}_chi0_"
saveObs = f"data_obs_W{i}.npz"


print("\n" + "#" * 79 + f"\nCTMRG for tensor A=W{i}, SU(6) 6-6b with D=13\n" + "#" * 79)
D = 13
chi0L = [80, 110, 140]
tol = 1e-11
niter_check = 7

pcolD = np.zeros((13, 8), dtype=np.int8)
for j in range(6):
    pcolD[j, j] = 1
    pcolD[j + 6, j] = 1

pcolD[12, 6] = 1
pcolD[:6, 7] = 1
pcolD = 2 * pcolD - 1


H = np.empty((2, 1296))
H[0] = sum(np.kron(mat, -mat.conj()).real for mat in genSU4_6).copy().reshape(1296)
H[1] = (H[0].reshape(36, 36) @ H[0].reshape(36, 36)).reshape(1296) / 4

T0_13 = np.zeros((6, 13, 13, 13, 13), dtype=np.int8)  # RVB SU(6) 6-6b with D=13
for j in range(6):
    T0_13[j, j, 12, 12, 12] = 1
    T0_13[j, 12, j, 12, 12] = 1
    T0_13[j, 12, 12, j, 12] = 1
    T0_13[j, 12, 12, 12, j] = 1

print(f"pcolD = {pcolD}")
A_L = [T0_13, W1, W2, W3]
chiL, epsL, xiL, s_entL = [], [], [], []

###############################################################################
#                                computation starts
###############################################################################
print(f"\nBegin CTMRG loop, tol = {tol}, niter_check = {niter_check}")
print("restart CTMRG at each chi0 to avoid precision loss")
print(f"At each step, save CTM in file {saveCTM0}$chi0.npz")
print("with keys: s, T, pcol, chi0, i")

for chi0 in chi0L:
    # 1) CTMRG loop
    print("", "#" * 79, f"set chi0 to {chi0}, iterate CTMRG...", sep="\n")
    ctm = sparseCTM([1], A_L[i : i + 1], chi0L[0], pcolD, alternate=True)
    ctm.chi0 = chi0
    ctm.converge(tol, warmup=100, niter_check=niter_check, stuck=50, verbose=True)
    if ctm.chi in chiL:
        print("same fixed point as last iteration, do not recompute observables")
    # 2) compute observables
    else:
        saveCTM = saveCTM0 + f"{chi0}.npz"
        np.savez_compressed(saveCTM, s=ctm.s, T=ctm.T, pcol=ctm.pcol, chi0=chi0, i=i)
        print("CTM data saved in file", saveCTM)
        ctm.check(tol=tol)
        chiL.append(ctm.chi)
        print(f"\ncompute energies, correlation length and s_ent at chi = {ctm.chi}")
        rho = ctm.compute_density_matrix()  # hermitian rho + real H => real eps
        epsL.append(np.dot(H, rho.reshape(1296)).real)  # fast Tr(H0,H1 @ rho)
        print(f"energies(thetat=0, theta=pi/2) = {epsL[-1]}")
        xiL.append(ctm.compute_corr_length(maxiter=500))
        s_entL.append(ctm.compute_entropy(maxiter=500))
        print(f"xi = {xiL[-1]:.1f}    s_ent = {s_entL[-1]:.3f}")

        # 3) save results
        np.savez_compressed(
            saveObs,
            i=i,
            chiL=np.array(chiL),
            eps0pi2=np.array(epsL),
            xiL=np.array(xiL),
            s_entL=np.array(s_entL),
        )
        print(f"data saved in file {saveObs}")
        print("with keys: i, chiL, eps0pi2, xiL, s_entL")


print("", "#" * 79, "program finished", "#" * 79, sep="\n")
