import numpy as np

# spin 2 AKLT tensor on the square lattice
tAKLT2 = np.zeros((5, 2, 2, 2, 2), dtype=np.float64)
tAKLT2[0, 0, 0, 0, 0] = 1.0
tAKLT2[1, 0, 0, 0, 1] = 0.5
tAKLT2[1, 0, 0, 1, 0] = 0.5
tAKLT2[1, 0, 1, 0, 0] = 0.5
tAKLT2[1, 1, 0, 0, 0] = 0.5
tAKLT2[2, 0, 0, 1, 1] = 1.0 / np.sqrt(6.0)
tAKLT2[2, 0, 1, 0, 1] = 1.0 / np.sqrt(6.0)
tAKLT2[2, 0, 1, 1, 0] = 1.0 / np.sqrt(6.0)
tAKLT2[2, 1, 0, 0, 1] = 1.0 / np.sqrt(6.0)
tAKLT2[2, 1, 0, 1, 0] = 1.0 / np.sqrt(6.0)
tAKLT2[2, 1, 1, 0, 0] = 1.0 / np.sqrt(6.0)
tAKLT2[3, 0, 1, 1, 1] = 0.5
tAKLT2[3, 1, 0, 1, 1] = 0.5
tAKLT2[3, 1, 1, 0, 1] = 0.5
tAKLT2[3, 1, 1, 1, 0] = 0.5
tAKLT2[4, 1, 1, 1, 1] = 1.0
