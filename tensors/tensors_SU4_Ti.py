###############################################################################
#
# define SU(4) symmetric tensors Ti with
# d = 6        physical variable
# D = 7        bond dimension
# V = 6+1      virtual space

# T0: n_occ = {1,3}, C4v A1 (same tensor as SU(6) RVB 6-6bar)
# T1: n_occ = {3,1}, C4v A1 (positive tensor with symmetric group S4 sym)
# T2: n_occ = {3,1}, C4v A1
# T3: n_occ = {3,1}, C4v A2
#
###############################################################################


import numpy as np

# projector (6+1)^2 -> 1. Factor 4 for complex integer sqrt.
p77to1 = np.zeros((7, 7))
p77to1[0, 5] = 4
p77to1[1, 4] = -4
p77to1[2, 3] = 4
p77to1[3, 2] = 4
p77to1[4, 1] = -4
p77to1[5, 0] = 4
p77to1[6, 6] = 4


T0 = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
for i in range(6):
    T0[i, i, 6, 6, 6] = 1
    T0[i, 6, i, 6, 6] = 1
    T0[i, 6, 6, i, 6] = 1
    T0[i, 6, 6, 6, i] = 1

T1 = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
T1[0, 0, 0, 5, 6] = 2
T1[0, 0, 0, 6, 5] = 2
T1[0, 0, 1, 4, 6] = -1
T1[0, 0, 1, 6, 4] = -1
T1[0, 0, 2, 3, 6] = 1
T1[0, 0, 2, 6, 3] = 1
T1[0, 0, 3, 2, 6] = 1
T1[0, 0, 3, 6, 2] = 1
T1[0, 0, 4, 1, 6] = -1
T1[0, 0, 4, 6, 1] = -1
T1[0, 0, 5, 0, 6] = 2
T1[0, 0, 5, 6, 0] = 2
T1[0, 0, 6, 0, 5] = 2
T1[0, 0, 6, 1, 4] = -1
T1[0, 0, 6, 2, 3] = 1
T1[0, 0, 6, 3, 2] = 1
T1[0, 0, 6, 4, 1] = -1
T1[0, 0, 6, 5, 0] = 2
T1[0, 1, 0, 4, 6] = -1
T1[0, 1, 0, 6, 4] = -1
T1[0, 1, 4, 0, 6] = -1
T1[0, 1, 4, 6, 0] = -1
T1[0, 1, 6, 0, 4] = -1
T1[0, 1, 6, 4, 0] = -1
T1[0, 2, 0, 3, 6] = 1
T1[0, 2, 0, 6, 3] = 1
T1[0, 2, 3, 0, 6] = 1
T1[0, 2, 3, 6, 0] = 1
T1[0, 2, 6, 0, 3] = 1
T1[0, 2, 6, 3, 0] = 1
T1[0, 3, 0, 2, 6] = 1
T1[0, 3, 0, 6, 2] = 1
T1[0, 3, 2, 0, 6] = 1
T1[0, 3, 2, 6, 0] = 1
T1[0, 3, 6, 0, 2] = 1
T1[0, 3, 6, 2, 0] = 1
T1[0, 4, 0, 1, 6] = -1
T1[0, 4, 0, 6, 1] = -1
T1[0, 4, 1, 0, 6] = -1
T1[0, 4, 1, 6, 0] = -1
T1[0, 4, 6, 0, 1] = -1
T1[0, 4, 6, 1, 0] = -1
T1[0, 5, 0, 0, 6] = 2
T1[0, 5, 0, 6, 0] = 2
T1[0, 5, 6, 0, 0] = 2
T1[0, 6, 0, 0, 5] = 2
T1[0, 6, 0, 1, 4] = -1
T1[0, 6, 0, 2, 3] = 1
T1[0, 6, 0, 3, 2] = 1
T1[0, 6, 0, 4, 1] = -1
T1[0, 6, 0, 5, 0] = 2
T1[0, 6, 1, 0, 4] = -1
T1[0, 6, 1, 4, 0] = -1
T1[0, 6, 2, 0, 3] = 1
T1[0, 6, 2, 3, 0] = 1
T1[0, 6, 3, 0, 2] = 1
T1[0, 6, 3, 2, 0] = 1
T1[0, 6, 4, 0, 1] = -1
T1[0, 6, 4, 1, 0] = -1
T1[0, 6, 5, 0, 0] = 2
T1[1, 0, 1, 5, 6] = 1
T1[1, 0, 1, 6, 5] = 1
T1[1, 0, 5, 1, 6] = 1
T1[1, 0, 5, 6, 1] = 1
T1[1, 0, 6, 1, 5] = 1
T1[1, 0, 6, 5, 1] = 1
T1[1, 1, 0, 5, 6] = 1
T1[1, 1, 0, 6, 5] = 1
T1[1, 1, 1, 4, 6] = -2
T1[1, 1, 1, 6, 4] = -2
T1[1, 1, 2, 3, 6] = 1
T1[1, 1, 2, 6, 3] = 1
T1[1, 1, 3, 2, 6] = 1
T1[1, 1, 3, 6, 2] = 1
T1[1, 1, 4, 1, 6] = -2
T1[1, 1, 4, 6, 1] = -2
T1[1, 1, 5, 0, 6] = 1
T1[1, 1, 5, 6, 0] = 1
T1[1, 1, 6, 0, 5] = 1
T1[1, 1, 6, 1, 4] = -2
T1[1, 1, 6, 2, 3] = 1
T1[1, 1, 6, 3, 2] = 1
T1[1, 1, 6, 4, 1] = -2
T1[1, 1, 6, 5, 0] = 1
T1[1, 2, 1, 3, 6] = 1
T1[1, 2, 1, 6, 3] = 1
T1[1, 2, 3, 1, 6] = 1
T1[1, 2, 3, 6, 1] = 1
T1[1, 2, 6, 1, 3] = 1
T1[1, 2, 6, 3, 1] = 1
T1[1, 3, 1, 2, 6] = 1
T1[1, 3, 1, 6, 2] = 1
T1[1, 3, 2, 1, 6] = 1
T1[1, 3, 2, 6, 1] = 1
T1[1, 3, 6, 1, 2] = 1
T1[1, 3, 6, 2, 1] = 1
T1[1, 4, 1, 1, 6] = -2
T1[1, 4, 1, 6, 1] = -2
T1[1, 4, 6, 1, 1] = -2
T1[1, 5, 0, 1, 6] = 1
T1[1, 5, 0, 6, 1] = 1
T1[1, 5, 1, 0, 6] = 1
T1[1, 5, 1, 6, 0] = 1
T1[1, 5, 6, 0, 1] = 1
T1[1, 5, 6, 1, 0] = 1
T1[1, 6, 0, 1, 5] = 1
T1[1, 6, 0, 5, 1] = 1
T1[1, 6, 1, 0, 5] = 1
T1[1, 6, 1, 1, 4] = -2
T1[1, 6, 1, 2, 3] = 1
T1[1, 6, 1, 3, 2] = 1
T1[1, 6, 1, 4, 1] = -2
T1[1, 6, 1, 5, 0] = 1
T1[1, 6, 2, 1, 3] = 1
T1[1, 6, 2, 3, 1] = 1
T1[1, 6, 3, 1, 2] = 1
T1[1, 6, 3, 2, 1] = 1
T1[1, 6, 4, 1, 1] = -2
T1[1, 6, 5, 0, 1] = 1
T1[1, 6, 5, 1, 0] = 1
T1[2, 0, 2, 5, 6] = 1
T1[2, 0, 2, 6, 5] = 1
T1[2, 0, 5, 2, 6] = 1
T1[2, 0, 5, 6, 2] = 1
T1[2, 0, 6, 2, 5] = 1
T1[2, 0, 6, 5, 2] = 1
T1[2, 1, 2, 4, 6] = -1
T1[2, 1, 2, 6, 4] = -1
T1[2, 1, 4, 2, 6] = -1
T1[2, 1, 4, 6, 2] = -1
T1[2, 1, 6, 2, 4] = -1
T1[2, 1, 6, 4, 2] = -1
T1[2, 2, 0, 5, 6] = 1
T1[2, 2, 0, 6, 5] = 1
T1[2, 2, 1, 4, 6] = -1
T1[2, 2, 1, 6, 4] = -1
T1[2, 2, 2, 3, 6] = 2
T1[2, 2, 2, 6, 3] = 2
T1[2, 2, 3, 2, 6] = 2
T1[2, 2, 3, 6, 2] = 2
T1[2, 2, 4, 1, 6] = -1
T1[2, 2, 4, 6, 1] = -1
T1[2, 2, 5, 0, 6] = 1
T1[2, 2, 5, 6, 0] = 1
T1[2, 2, 6, 0, 5] = 1
T1[2, 2, 6, 1, 4] = -1
T1[2, 2, 6, 2, 3] = 2
T1[2, 2, 6, 3, 2] = 2
T1[2, 2, 6, 4, 1] = -1
T1[2, 2, 6, 5, 0] = 1
T1[2, 3, 2, 2, 6] = 2
T1[2, 3, 2, 6, 2] = 2
T1[2, 3, 6, 2, 2] = 2
T1[2, 4, 1, 2, 6] = -1
T1[2, 4, 1, 6, 2] = -1
T1[2, 4, 2, 1, 6] = -1
T1[2, 4, 2, 6, 1] = -1
T1[2, 4, 6, 1, 2] = -1
T1[2, 4, 6, 2, 1] = -1
T1[2, 5, 0, 2, 6] = 1
T1[2, 5, 0, 6, 2] = 1
T1[2, 5, 2, 0, 6] = 1
T1[2, 5, 2, 6, 0] = 1
T1[2, 5, 6, 0, 2] = 1
T1[2, 5, 6, 2, 0] = 1
T1[2, 6, 0, 2, 5] = 1
T1[2, 6, 0, 5, 2] = 1
T1[2, 6, 1, 2, 4] = -1
T1[2, 6, 1, 4, 2] = -1
T1[2, 6, 2, 0, 5] = 1
T1[2, 6, 2, 1, 4] = -1
T1[2, 6, 2, 2, 3] = 2
T1[2, 6, 2, 3, 2] = 2
T1[2, 6, 2, 4, 1] = -1
T1[2, 6, 2, 5, 0] = 1
T1[2, 6, 3, 2, 2] = 2
T1[2, 6, 4, 1, 2] = -1
T1[2, 6, 4, 2, 1] = -1
T1[2, 6, 5, 0, 2] = 1
T1[2, 6, 5, 2, 0] = 1
T1[3, 0, 3, 5, 6] = 1
T1[3, 0, 3, 6, 5] = 1
T1[3, 0, 5, 3, 6] = 1
T1[3, 0, 5, 6, 3] = 1
T1[3, 0, 6, 3, 5] = 1
T1[3, 0, 6, 5, 3] = 1
T1[3, 1, 3, 4, 6] = -1
T1[3, 1, 3, 6, 4] = -1
T1[3, 1, 4, 3, 6] = -1
T1[3, 1, 4, 6, 3] = -1
T1[3, 1, 6, 3, 4] = -1
T1[3, 1, 6, 4, 3] = -1
T1[3, 2, 3, 3, 6] = 2
T1[3, 2, 3, 6, 3] = 2
T1[3, 2, 6, 3, 3] = 2
T1[3, 3, 0, 5, 6] = 1
T1[3, 3, 0, 6, 5] = 1
T1[3, 3, 1, 4, 6] = -1
T1[3, 3, 1, 6, 4] = -1
T1[3, 3, 2, 3, 6] = 2
T1[3, 3, 2, 6, 3] = 2
T1[3, 3, 3, 2, 6] = 2
T1[3, 3, 3, 6, 2] = 2
T1[3, 3, 4, 1, 6] = -1
T1[3, 3, 4, 6, 1] = -1
T1[3, 3, 5, 0, 6] = 1
T1[3, 3, 5, 6, 0] = 1
T1[3, 3, 6, 0, 5] = 1
T1[3, 3, 6, 1, 4] = -1
T1[3, 3, 6, 2, 3] = 2
T1[3, 3, 6, 3, 2] = 2
T1[3, 3, 6, 4, 1] = -1
T1[3, 3, 6, 5, 0] = 1
T1[3, 4, 1, 3, 6] = -1
T1[3, 4, 1, 6, 3] = -1
T1[3, 4, 3, 1, 6] = -1
T1[3, 4, 3, 6, 1] = -1
T1[3, 4, 6, 1, 3] = -1
T1[3, 4, 6, 3, 1] = -1
T1[3, 5, 0, 3, 6] = 1
T1[3, 5, 0, 6, 3] = 1
T1[3, 5, 3, 0, 6] = 1
T1[3, 5, 3, 6, 0] = 1
T1[3, 5, 6, 0, 3] = 1
T1[3, 5, 6, 3, 0] = 1
T1[3, 6, 0, 3, 5] = 1
T1[3, 6, 0, 5, 3] = 1
T1[3, 6, 1, 3, 4] = -1
T1[3, 6, 1, 4, 3] = -1
T1[3, 6, 2, 3, 3] = 2
T1[3, 6, 3, 0, 5] = 1
T1[3, 6, 3, 1, 4] = -1
T1[3, 6, 3, 2, 3] = 2
T1[3, 6, 3, 3, 2] = 2
T1[3, 6, 3, 4, 1] = -1
T1[3, 6, 3, 5, 0] = 1
T1[3, 6, 4, 1, 3] = -1
T1[3, 6, 4, 3, 1] = -1
T1[3, 6, 5, 0, 3] = 1
T1[3, 6, 5, 3, 0] = 1
T1[4, 0, 4, 5, 6] = 1
T1[4, 0, 4, 6, 5] = 1
T1[4, 0, 5, 4, 6] = 1
T1[4, 0, 5, 6, 4] = 1
T1[4, 0, 6, 4, 5] = 1
T1[4, 0, 6, 5, 4] = 1
T1[4, 1, 4, 4, 6] = -2
T1[4, 1, 4, 6, 4] = -2
T1[4, 1, 6, 4, 4] = -2
T1[4, 2, 3, 4, 6] = 1
T1[4, 2, 3, 6, 4] = 1
T1[4, 2, 4, 3, 6] = 1
T1[4, 2, 4, 6, 3] = 1
T1[4, 2, 6, 3, 4] = 1
T1[4, 2, 6, 4, 3] = 1
T1[4, 3, 2, 4, 6] = 1
T1[4, 3, 2, 6, 4] = 1
T1[4, 3, 4, 2, 6] = 1
T1[4, 3, 4, 6, 2] = 1
T1[4, 3, 6, 2, 4] = 1
T1[4, 3, 6, 4, 2] = 1
T1[4, 4, 0, 5, 6] = 1
T1[4, 4, 0, 6, 5] = 1
T1[4, 4, 1, 4, 6] = -2
T1[4, 4, 1, 6, 4] = -2
T1[4, 4, 2, 3, 6] = 1
T1[4, 4, 2, 6, 3] = 1
T1[4, 4, 3, 2, 6] = 1
T1[4, 4, 3, 6, 2] = 1
T1[4, 4, 4, 1, 6] = -2
T1[4, 4, 4, 6, 1] = -2
T1[4, 4, 5, 0, 6] = 1
T1[4, 4, 5, 6, 0] = 1
T1[4, 4, 6, 0, 5] = 1
T1[4, 4, 6, 1, 4] = -2
T1[4, 4, 6, 2, 3] = 1
T1[4, 4, 6, 3, 2] = 1
T1[4, 4, 6, 4, 1] = -2
T1[4, 4, 6, 5, 0] = 1
T1[4, 5, 0, 4, 6] = 1
T1[4, 5, 0, 6, 4] = 1
T1[4, 5, 4, 0, 6] = 1
T1[4, 5, 4, 6, 0] = 1
T1[4, 5, 6, 0, 4] = 1
T1[4, 5, 6, 4, 0] = 1
T1[4, 6, 0, 4, 5] = 1
T1[4, 6, 0, 5, 4] = 1
T1[4, 6, 1, 4, 4] = -2
T1[4, 6, 2, 3, 4] = 1
T1[4, 6, 2, 4, 3] = 1
T1[4, 6, 3, 2, 4] = 1
T1[4, 6, 3, 4, 2] = 1
T1[4, 6, 4, 0, 5] = 1
T1[4, 6, 4, 1, 4] = -2
T1[4, 6, 4, 2, 3] = 1
T1[4, 6, 4, 3, 2] = 1
T1[4, 6, 4, 4, 1] = -2
T1[4, 6, 4, 5, 0] = 1
T1[4, 6, 5, 0, 4] = 1
T1[4, 6, 5, 4, 0] = 1
T1[5, 0, 5, 5, 6] = 2
T1[5, 0, 5, 6, 5] = 2
T1[5, 0, 6, 5, 5] = 2
T1[5, 1, 4, 5, 6] = -1
T1[5, 1, 4, 6, 5] = -1
T1[5, 1, 5, 4, 6] = -1
T1[5, 1, 5, 6, 4] = -1
T1[5, 1, 6, 4, 5] = -1
T1[5, 1, 6, 5, 4] = -1
T1[5, 2, 3, 5, 6] = 1
T1[5, 2, 3, 6, 5] = 1
T1[5, 2, 5, 3, 6] = 1
T1[5, 2, 5, 6, 3] = 1
T1[5, 2, 6, 3, 5] = 1
T1[5, 2, 6, 5, 3] = 1
T1[5, 3, 2, 5, 6] = 1
T1[5, 3, 2, 6, 5] = 1
T1[5, 3, 5, 2, 6] = 1
T1[5, 3, 5, 6, 2] = 1
T1[5, 3, 6, 2, 5] = 1
T1[5, 3, 6, 5, 2] = 1
T1[5, 4, 1, 5, 6] = -1
T1[5, 4, 1, 6, 5] = -1
T1[5, 4, 5, 1, 6] = -1
T1[5, 4, 5, 6, 1] = -1
T1[5, 4, 6, 1, 5] = -1
T1[5, 4, 6, 5, 1] = -1
T1[5, 5, 0, 5, 6] = 2
T1[5, 5, 0, 6, 5] = 2
T1[5, 5, 1, 4, 6] = -1
T1[5, 5, 1, 6, 4] = -1
T1[5, 5, 2, 3, 6] = 1
T1[5, 5, 2, 6, 3] = 1
T1[5, 5, 3, 2, 6] = 1
T1[5, 5, 3, 6, 2] = 1
T1[5, 5, 4, 1, 6] = -1
T1[5, 5, 4, 6, 1] = -1
T1[5, 5, 5, 0, 6] = 2
T1[5, 5, 5, 6, 0] = 2
T1[5, 5, 6, 0, 5] = 2
T1[5, 5, 6, 1, 4] = -1
T1[5, 5, 6, 2, 3] = 1
T1[5, 5, 6, 3, 2] = 1
T1[5, 5, 6, 4, 1] = -1
T1[5, 5, 6, 5, 0] = 2
T1[5, 6, 0, 5, 5] = 2
T1[5, 6, 1, 4, 5] = -1
T1[5, 6, 1, 5, 4] = -1
T1[5, 6, 2, 3, 5] = 1
T1[5, 6, 2, 5, 3] = 1
T1[5, 6, 3, 2, 5] = 1
T1[5, 6, 3, 5, 2] = 1
T1[5, 6, 4, 1, 5] = -1
T1[5, 6, 4, 5, 1] = -1
T1[5, 6, 5, 0, 5] = 2
T1[5, 6, 5, 1, 4] = -1
T1[5, 6, 5, 2, 3] = 1
T1[5, 6, 5, 3, 2] = 1
T1[5, 6, 5, 4, 1] = -1
T1[5, 6, 5, 5, 0] = 2

T2 = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
T2[0, 0, 0, 5, 6] = -1
T2[0, 0, 0, 6, 5] = -1
T2[0, 0, 1, 4, 6] = -1
T2[0, 0, 1, 6, 4] = 2
T2[0, 0, 2, 3, 6] = 1
T2[0, 0, 2, 6, 3] = -2
T2[0, 0, 3, 2, 6] = 1
T2[0, 0, 3, 6, 2] = -2
T2[0, 0, 4, 1, 6] = -1
T2[0, 0, 4, 6, 1] = 2
T2[0, 0, 5, 0, 6] = 2
T2[0, 0, 5, 6, 0] = -1
T2[0, 0, 6, 0, 5] = 2
T2[0, 0, 6, 1, 4] = -1
T2[0, 0, 6, 2, 3] = 1
T2[0, 0, 6, 3, 2] = 1
T2[0, 0, 6, 4, 1] = -1
T2[0, 0, 6, 5, 0] = -1
T2[0, 1, 0, 4, 6] = 2
T2[0, 1, 0, 6, 4] = -1
T2[0, 1, 4, 0, 6] = -1
T2[0, 1, 4, 6, 0] = -1
T2[0, 1, 6, 0, 4] = -1
T2[0, 1, 6, 4, 0] = 2
T2[0, 2, 0, 3, 6] = -2
T2[0, 2, 0, 6, 3] = 1
T2[0, 2, 3, 0, 6] = 1
T2[0, 2, 3, 6, 0] = 1
T2[0, 2, 6, 0, 3] = 1
T2[0, 2, 6, 3, 0] = -2
T2[0, 3, 0, 2, 6] = -2
T2[0, 3, 0, 6, 2] = 1
T2[0, 3, 2, 0, 6] = 1
T2[0, 3, 2, 6, 0] = 1
T2[0, 3, 6, 0, 2] = 1
T2[0, 3, 6, 2, 0] = -2
T2[0, 4, 0, 1, 6] = 2
T2[0, 4, 0, 6, 1] = -1
T2[0, 4, 1, 0, 6] = -1
T2[0, 4, 1, 6, 0] = -1
T2[0, 4, 6, 0, 1] = -1
T2[0, 4, 6, 1, 0] = 2
T2[0, 5, 0, 0, 6] = -1
T2[0, 5, 0, 6, 0] = 2
T2[0, 5, 6, 0, 0] = -1
T2[0, 6, 0, 0, 5] = -1
T2[0, 6, 0, 1, 4] = -1
T2[0, 6, 0, 2, 3] = 1
T2[0, 6, 0, 3, 2] = 1
T2[0, 6, 0, 4, 1] = -1
T2[0, 6, 0, 5, 0] = 2
T2[0, 6, 1, 0, 4] = 2
T2[0, 6, 1, 4, 0] = -1
T2[0, 6, 2, 0, 3] = -2
T2[0, 6, 2, 3, 0] = 1
T2[0, 6, 3, 0, 2] = -2
T2[0, 6, 3, 2, 0] = 1
T2[0, 6, 4, 0, 1] = 2
T2[0, 6, 4, 1, 0] = -1
T2[0, 6, 5, 0, 0] = -1
T2[1, 0, 1, 5, 6] = -2
T2[1, 0, 1, 6, 5] = 1
T2[1, 0, 5, 1, 6] = 1
T2[1, 0, 5, 6, 1] = 1
T2[1, 0, 6, 1, 5] = 1
T2[1, 0, 6, 5, 1] = -2
T2[1, 1, 0, 5, 6] = 1
T2[1, 1, 0, 6, 5] = -2
T2[1, 1, 1, 4, 6] = 1
T2[1, 1, 1, 6, 4] = 1
T2[1, 1, 2, 3, 6] = 1
T2[1, 1, 2, 6, 3] = -2
T2[1, 1, 3, 2, 6] = 1
T2[1, 1, 3, 6, 2] = -2
T2[1, 1, 4, 1, 6] = -2
T2[1, 1, 4, 6, 1] = 1
T2[1, 1, 5, 0, 6] = 1
T2[1, 1, 5, 6, 0] = -2
T2[1, 1, 6, 0, 5] = 1
T2[1, 1, 6, 1, 4] = -2
T2[1, 1, 6, 2, 3] = 1
T2[1, 1, 6, 3, 2] = 1
T2[1, 1, 6, 4, 1] = 1
T2[1, 1, 6, 5, 0] = 1
T2[1, 2, 1, 3, 6] = -2
T2[1, 2, 1, 6, 3] = 1
T2[1, 2, 3, 1, 6] = 1
T2[1, 2, 3, 6, 1] = 1
T2[1, 2, 6, 1, 3] = 1
T2[1, 2, 6, 3, 1] = -2
T2[1, 3, 1, 2, 6] = -2
T2[1, 3, 1, 6, 2] = 1
T2[1, 3, 2, 1, 6] = 1
T2[1, 3, 2, 6, 1] = 1
T2[1, 3, 6, 1, 2] = 1
T2[1, 3, 6, 2, 1] = -2
T2[1, 4, 1, 1, 6] = 1
T2[1, 4, 1, 6, 1] = -2
T2[1, 4, 6, 1, 1] = 1
T2[1, 5, 0, 1, 6] = 1
T2[1, 5, 0, 6, 1] = 1
T2[1, 5, 1, 0, 6] = -2
T2[1, 5, 1, 6, 0] = 1
T2[1, 5, 6, 0, 1] = -2
T2[1, 5, 6, 1, 0] = 1
T2[1, 6, 0, 1, 5] = -2
T2[1, 6, 0, 5, 1] = 1
T2[1, 6, 1, 0, 5] = 1
T2[1, 6, 1, 1, 4] = 1
T2[1, 6, 1, 2, 3] = 1
T2[1, 6, 1, 3, 2] = 1
T2[1, 6, 1, 4, 1] = -2
T2[1, 6, 1, 5, 0] = 1
T2[1, 6, 2, 1, 3] = -2
T2[1, 6, 2, 3, 1] = 1
T2[1, 6, 3, 1, 2] = -2
T2[1, 6, 3, 2, 1] = 1
T2[1, 6, 4, 1, 1] = 1
T2[1, 6, 5, 0, 1] = 1
T2[1, 6, 5, 1, 0] = -2
T2[2, 0, 2, 5, 6] = -2
T2[2, 0, 2, 6, 5] = 1
T2[2, 0, 5, 2, 6] = 1
T2[2, 0, 5, 6, 2] = 1
T2[2, 0, 6, 2, 5] = 1
T2[2, 0, 6, 5, 2] = -2
T2[2, 1, 2, 4, 6] = 2
T2[2, 1, 2, 6, 4] = -1
T2[2, 1, 4, 2, 6] = -1
T2[2, 1, 4, 6, 2] = -1
T2[2, 1, 6, 2, 4] = -1
T2[2, 1, 6, 4, 2] = 2
T2[2, 2, 0, 5, 6] = 1
T2[2, 2, 0, 6, 5] = -2
T2[2, 2, 1, 4, 6] = -1
T2[2, 2, 1, 6, 4] = 2
T2[2, 2, 2, 3, 6] = -1
T2[2, 2, 2, 6, 3] = -1
T2[2, 2, 3, 2, 6] = 2
T2[2, 2, 3, 6, 2] = -1
T2[2, 2, 4, 1, 6] = -1
T2[2, 2, 4, 6, 1] = 2
T2[2, 2, 5, 0, 6] = 1
T2[2, 2, 5, 6, 0] = -2
T2[2, 2, 6, 0, 5] = 1
T2[2, 2, 6, 1, 4] = -1
T2[2, 2, 6, 2, 3] = 2
T2[2, 2, 6, 3, 2] = -1
T2[2, 2, 6, 4, 1] = -1
T2[2, 2, 6, 5, 0] = 1
T2[2, 3, 2, 2, 6] = -1
T2[2, 3, 2, 6, 2] = 2
T2[2, 3, 6, 2, 2] = -1
T2[2, 4, 1, 2, 6] = -1
T2[2, 4, 1, 6, 2] = -1
T2[2, 4, 2, 1, 6] = 2
T2[2, 4, 2, 6, 1] = -1
T2[2, 4, 6, 1, 2] = 2
T2[2, 4, 6, 2, 1] = -1
T2[2, 5, 0, 2, 6] = 1
T2[2, 5, 0, 6, 2] = 1
T2[2, 5, 2, 0, 6] = -2
T2[2, 5, 2, 6, 0] = 1
T2[2, 5, 6, 0, 2] = -2
T2[2, 5, 6, 2, 0] = 1
T2[2, 6, 0, 2, 5] = -2
T2[2, 6, 0, 5, 2] = 1
T2[2, 6, 1, 2, 4] = 2
T2[2, 6, 1, 4, 2] = -1
T2[2, 6, 2, 0, 5] = 1
T2[2, 6, 2, 1, 4] = -1
T2[2, 6, 2, 2, 3] = -1
T2[2, 6, 2, 3, 2] = 2
T2[2, 6, 2, 4, 1] = -1
T2[2, 6, 2, 5, 0] = 1
T2[2, 6, 3, 2, 2] = -1
T2[2, 6, 4, 1, 2] = -1
T2[2, 6, 4, 2, 1] = 2
T2[2, 6, 5, 0, 2] = 1
T2[2, 6, 5, 2, 0] = -2
T2[3, 0, 3, 5, 6] = -2
T2[3, 0, 3, 6, 5] = 1
T2[3, 0, 5, 3, 6] = 1
T2[3, 0, 5, 6, 3] = 1
T2[3, 0, 6, 3, 5] = 1
T2[3, 0, 6, 5, 3] = -2
T2[3, 1, 3, 4, 6] = 2
T2[3, 1, 3, 6, 4] = -1
T2[3, 1, 4, 3, 6] = -1
T2[3, 1, 4, 6, 3] = -1
T2[3, 1, 6, 3, 4] = -1
T2[3, 1, 6, 4, 3] = 2
T2[3, 2, 3, 3, 6] = -1
T2[3, 2, 3, 6, 3] = 2
T2[3, 2, 6, 3, 3] = -1
T2[3, 3, 0, 5, 6] = 1
T2[3, 3, 0, 6, 5] = -2
T2[3, 3, 1, 4, 6] = -1
T2[3, 3, 1, 6, 4] = 2
T2[3, 3, 2, 3, 6] = 2
T2[3, 3, 2, 6, 3] = -1
T2[3, 3, 3, 2, 6] = -1
T2[3, 3, 3, 6, 2] = -1
T2[3, 3, 4, 1, 6] = -1
T2[3, 3, 4, 6, 1] = 2
T2[3, 3, 5, 0, 6] = 1
T2[3, 3, 5, 6, 0] = -2
T2[3, 3, 6, 0, 5] = 1
T2[3, 3, 6, 1, 4] = -1
T2[3, 3, 6, 2, 3] = -1
T2[3, 3, 6, 3, 2] = 2
T2[3, 3, 6, 4, 1] = -1
T2[3, 3, 6, 5, 0] = 1
T2[3, 4, 1, 3, 6] = -1
T2[3, 4, 1, 6, 3] = -1
T2[3, 4, 3, 1, 6] = 2
T2[3, 4, 3, 6, 1] = -1
T2[3, 4, 6, 1, 3] = 2
T2[3, 4, 6, 3, 1] = -1
T2[3, 5, 0, 3, 6] = 1
T2[3, 5, 0, 6, 3] = 1
T2[3, 5, 3, 0, 6] = -2
T2[3, 5, 3, 6, 0] = 1
T2[3, 5, 6, 0, 3] = -2
T2[3, 5, 6, 3, 0] = 1
T2[3, 6, 0, 3, 5] = -2
T2[3, 6, 0, 5, 3] = 1
T2[3, 6, 1, 3, 4] = 2
T2[3, 6, 1, 4, 3] = -1
T2[3, 6, 2, 3, 3] = -1
T2[3, 6, 3, 0, 5] = 1
T2[3, 6, 3, 1, 4] = -1
T2[3, 6, 3, 2, 3] = 2
T2[3, 6, 3, 3, 2] = -1
T2[3, 6, 3, 4, 1] = -1
T2[3, 6, 3, 5, 0] = 1
T2[3, 6, 4, 1, 3] = -1
T2[3, 6, 4, 3, 1] = 2
T2[3, 6, 5, 0, 3] = 1
T2[3, 6, 5, 3, 0] = -2
T2[4, 0, 4, 5, 6] = -2
T2[4, 0, 4, 6, 5] = 1
T2[4, 0, 5, 4, 6] = 1
T2[4, 0, 5, 6, 4] = 1
T2[4, 0, 6, 4, 5] = 1
T2[4, 0, 6, 5, 4] = -2
T2[4, 1, 4, 4, 6] = 1
T2[4, 1, 4, 6, 4] = -2
T2[4, 1, 6, 4, 4] = 1
T2[4, 2, 3, 4, 6] = 1
T2[4, 2, 3, 6, 4] = 1
T2[4, 2, 4, 3, 6] = -2
T2[4, 2, 4, 6, 3] = 1
T2[4, 2, 6, 3, 4] = -2
T2[4, 2, 6, 4, 3] = 1
T2[4, 3, 2, 4, 6] = 1
T2[4, 3, 2, 6, 4] = 1
T2[4, 3, 4, 2, 6] = -2
T2[4, 3, 4, 6, 2] = 1
T2[4, 3, 6, 2, 4] = -2
T2[4, 3, 6, 4, 2] = 1
T2[4, 4, 0, 5, 6] = 1
T2[4, 4, 0, 6, 5] = -2
T2[4, 4, 1, 4, 6] = -2
T2[4, 4, 1, 6, 4] = 1
T2[4, 4, 2, 3, 6] = 1
T2[4, 4, 2, 6, 3] = -2
T2[4, 4, 3, 2, 6] = 1
T2[4, 4, 3, 6, 2] = -2
T2[4, 4, 4, 1, 6] = 1
T2[4, 4, 4, 6, 1] = 1
T2[4, 4, 5, 0, 6] = 1
T2[4, 4, 5, 6, 0] = -2
T2[4, 4, 6, 0, 5] = 1
T2[4, 4, 6, 1, 4] = 1
T2[4, 4, 6, 2, 3] = 1
T2[4, 4, 6, 3, 2] = 1
T2[4, 4, 6, 4, 1] = -2
T2[4, 4, 6, 5, 0] = 1
T2[4, 5, 0, 4, 6] = 1
T2[4, 5, 0, 6, 4] = 1
T2[4, 5, 4, 0, 6] = -2
T2[4, 5, 4, 6, 0] = 1
T2[4, 5, 6, 0, 4] = -2
T2[4, 5, 6, 4, 0] = 1
T2[4, 6, 0, 4, 5] = -2
T2[4, 6, 0, 5, 4] = 1
T2[4, 6, 1, 4, 4] = 1
T2[4, 6, 2, 3, 4] = 1
T2[4, 6, 2, 4, 3] = -2
T2[4, 6, 3, 2, 4] = 1
T2[4, 6, 3, 4, 2] = -2
T2[4, 6, 4, 0, 5] = 1
T2[4, 6, 4, 1, 4] = -2
T2[4, 6, 4, 2, 3] = 1
T2[4, 6, 4, 3, 2] = 1
T2[4, 6, 4, 4, 1] = 1
T2[4, 6, 4, 5, 0] = 1
T2[4, 6, 5, 0, 4] = 1
T2[4, 6, 5, 4, 0] = -2
T2[5, 0, 5, 5, 6] = -1
T2[5, 0, 5, 6, 5] = 2
T2[5, 0, 6, 5, 5] = -1
T2[5, 1, 4, 5, 6] = -1
T2[5, 1, 4, 6, 5] = -1
T2[5, 1, 5, 4, 6] = 2
T2[5, 1, 5, 6, 4] = -1
T2[5, 1, 6, 4, 5] = 2
T2[5, 1, 6, 5, 4] = -1
T2[5, 2, 3, 5, 6] = 1
T2[5, 2, 3, 6, 5] = 1
T2[5, 2, 5, 3, 6] = -2
T2[5, 2, 5, 6, 3] = 1
T2[5, 2, 6, 3, 5] = -2
T2[5, 2, 6, 5, 3] = 1
T2[5, 3, 2, 5, 6] = 1
T2[5, 3, 2, 6, 5] = 1
T2[5, 3, 5, 2, 6] = -2
T2[5, 3, 5, 6, 2] = 1
T2[5, 3, 6, 2, 5] = -2
T2[5, 3, 6, 5, 2] = 1
T2[5, 4, 1, 5, 6] = -1
T2[5, 4, 1, 6, 5] = -1
T2[5, 4, 5, 1, 6] = 2
T2[5, 4, 5, 6, 1] = -1
T2[5, 4, 6, 1, 5] = 2
T2[5, 4, 6, 5, 1] = -1
T2[5, 5, 0, 5, 6] = 2
T2[5, 5, 0, 6, 5] = -1
T2[5, 5, 1, 4, 6] = -1
T2[5, 5, 1, 6, 4] = 2
T2[5, 5, 2, 3, 6] = 1
T2[5, 5, 2, 6, 3] = -2
T2[5, 5, 3, 2, 6] = 1
T2[5, 5, 3, 6, 2] = -2
T2[5, 5, 4, 1, 6] = -1
T2[5, 5, 4, 6, 1] = 2
T2[5, 5, 5, 0, 6] = -1
T2[5, 5, 5, 6, 0] = -1
T2[5, 5, 6, 0, 5] = -1
T2[5, 5, 6, 1, 4] = -1
T2[5, 5, 6, 2, 3] = 1
T2[5, 5, 6, 3, 2] = 1
T2[5, 5, 6, 4, 1] = -1
T2[5, 5, 6, 5, 0] = 2
T2[5, 6, 0, 5, 5] = -1
T2[5, 6, 1, 4, 5] = -1
T2[5, 6, 1, 5, 4] = 2
T2[5, 6, 2, 3, 5] = 1
T2[5, 6, 2, 5, 3] = -2
T2[5, 6, 3, 2, 5] = 1
T2[5, 6, 3, 5, 2] = -2
T2[5, 6, 4, 1, 5] = -1
T2[5, 6, 4, 5, 1] = 2
T2[5, 6, 5, 0, 5] = 2
T2[5, 6, 5, 1, 4] = -1
T2[5, 6, 5, 2, 3] = 1
T2[5, 6, 5, 3, 2] = 1
T2[5, 6, 5, 4, 1] = -1
T2[5, 6, 5, 5, 0] = -1

T3 = np.zeros((6, 7, 7, 7, 7), dtype=np.int8)
T3[0, 0, 0, 5, 6] = -1
T3[0, 0, 0, 6, 5] = 1
T3[0, 0, 1, 4, 6] = 1
T3[0, 0, 2, 3, 6] = -1
T3[0, 0, 3, 2, 6] = -1
T3[0, 0, 4, 1, 6] = 1
T3[0, 0, 5, 6, 0] = -1
T3[0, 0, 6, 1, 4] = -1
T3[0, 0, 6, 2, 3] = 1
T3[0, 0, 6, 3, 2] = 1
T3[0, 0, 6, 4, 1] = -1
T3[0, 0, 6, 5, 0] = 1
T3[0, 1, 0, 6, 4] = -1
T3[0, 1, 4, 0, 6] = -1
T3[0, 1, 4, 6, 0] = 1
T3[0, 1, 6, 0, 4] = 1
T3[0, 2, 0, 6, 3] = 1
T3[0, 2, 3, 0, 6] = 1
T3[0, 2, 3, 6, 0] = -1
T3[0, 2, 6, 0, 3] = -1
T3[0, 3, 0, 6, 2] = 1
T3[0, 3, 2, 0, 6] = 1
T3[0, 3, 2, 6, 0] = -1
T3[0, 3, 6, 0, 2] = -1
T3[0, 4, 0, 6, 1] = -1
T3[0, 4, 1, 0, 6] = -1
T3[0, 4, 1, 6, 0] = 1
T3[0, 4, 6, 0, 1] = 1
T3[0, 5, 0, 0, 6] = 1
T3[0, 5, 6, 0, 0] = -1
T3[0, 6, 0, 0, 5] = -1
T3[0, 6, 0, 1, 4] = 1
T3[0, 6, 0, 2, 3] = -1
T3[0, 6, 0, 3, 2] = -1
T3[0, 6, 0, 4, 1] = 1
T3[0, 6, 1, 4, 0] = -1
T3[0, 6, 2, 3, 0] = 1
T3[0, 6, 3, 2, 0] = 1
T3[0, 6, 4, 1, 0] = -1
T3[0, 6, 5, 0, 0] = 1
T3[1, 0, 1, 6, 5] = 1
T3[1, 0, 5, 1, 6] = 1
T3[1, 0, 5, 6, 1] = -1
T3[1, 0, 6, 1, 5] = -1
T3[1, 1, 0, 5, 6] = -1
T3[1, 1, 1, 4, 6] = 1
T3[1, 1, 1, 6, 4] = -1
T3[1, 1, 2, 3, 6] = -1
T3[1, 1, 3, 2, 6] = -1
T3[1, 1, 4, 6, 1] = 1
T3[1, 1, 5, 0, 6] = -1
T3[1, 1, 6, 0, 5] = 1
T3[1, 1, 6, 2, 3] = 1
T3[1, 1, 6, 3, 2] = 1
T3[1, 1, 6, 4, 1] = -1
T3[1, 1, 6, 5, 0] = 1
T3[1, 2, 1, 6, 3] = 1
T3[1, 2, 3, 1, 6] = 1
T3[1, 2, 3, 6, 1] = -1
T3[1, 2, 6, 1, 3] = -1
T3[1, 3, 1, 6, 2] = 1
T3[1, 3, 2, 1, 6] = 1
T3[1, 3, 2, 6, 1] = -1
T3[1, 3, 6, 1, 2] = -1
T3[1, 4, 1, 1, 6] = -1
T3[1, 4, 6, 1, 1] = 1
T3[1, 5, 0, 1, 6] = 1
T3[1, 5, 0, 6, 1] = -1
T3[1, 5, 1, 6, 0] = 1
T3[1, 5, 6, 1, 0] = -1
T3[1, 6, 0, 5, 1] = 1
T3[1, 6, 1, 0, 5] = -1
T3[1, 6, 1, 1, 4] = 1
T3[1, 6, 1, 2, 3] = -1
T3[1, 6, 1, 3, 2] = -1
T3[1, 6, 1, 5, 0] = -1
T3[1, 6, 2, 3, 1] = 1
T3[1, 6, 3, 2, 1] = 1
T3[1, 6, 4, 1, 1] = -1
T3[1, 6, 5, 0, 1] = 1
T3[2, 0, 2, 6, 5] = 1
T3[2, 0, 5, 2, 6] = 1
T3[2, 0, 5, 6, 2] = -1
T3[2, 0, 6, 2, 5] = -1
T3[2, 1, 2, 6, 4] = -1
T3[2, 1, 4, 2, 6] = -1
T3[2, 1, 4, 6, 2] = 1
T3[2, 1, 6, 2, 4] = 1
T3[2, 2, 0, 5, 6] = -1
T3[2, 2, 1, 4, 6] = 1
T3[2, 2, 2, 3, 6] = -1
T3[2, 2, 2, 6, 3] = 1
T3[2, 2, 3, 6, 2] = -1
T3[2, 2, 4, 1, 6] = 1
T3[2, 2, 5, 0, 6] = -1
T3[2, 2, 6, 0, 5] = 1
T3[2, 2, 6, 1, 4] = -1
T3[2, 2, 6, 3, 2] = 1
T3[2, 2, 6, 4, 1] = -1
T3[2, 2, 6, 5, 0] = 1
T3[2, 3, 2, 2, 6] = 1
T3[2, 3, 6, 2, 2] = -1
T3[2, 4, 1, 2, 6] = -1
T3[2, 4, 1, 6, 2] = 1
T3[2, 4, 2, 6, 1] = -1
T3[2, 4, 6, 2, 1] = 1
T3[2, 5, 0, 2, 6] = 1
T3[2, 5, 0, 6, 2] = -1
T3[2, 5, 2, 6, 0] = 1
T3[2, 5, 6, 2, 0] = -1
T3[2, 6, 0, 5, 2] = 1
T3[2, 6, 1, 4, 2] = -1
T3[2, 6, 2, 0, 5] = -1
T3[2, 6, 2, 1, 4] = 1
T3[2, 6, 2, 2, 3] = -1
T3[2, 6, 2, 4, 1] = 1
T3[2, 6, 2, 5, 0] = -1
T3[2, 6, 3, 2, 2] = 1
T3[2, 6, 4, 1, 2] = -1
T3[2, 6, 5, 0, 2] = 1
T3[3, 0, 3, 6, 5] = 1
T3[3, 0, 5, 3, 6] = 1
T3[3, 0, 5, 6, 3] = -1
T3[3, 0, 6, 3, 5] = -1
T3[3, 1, 3, 6, 4] = -1
T3[3, 1, 4, 3, 6] = -1
T3[3, 1, 4, 6, 3] = 1
T3[3, 1, 6, 3, 4] = 1
T3[3, 2, 3, 3, 6] = 1
T3[3, 2, 6, 3, 3] = -1
T3[3, 3, 0, 5, 6] = -1
T3[3, 3, 1, 4, 6] = 1
T3[3, 3, 2, 6, 3] = -1
T3[3, 3, 3, 2, 6] = -1
T3[3, 3, 3, 6, 2] = 1
T3[3, 3, 4, 1, 6] = 1
T3[3, 3, 5, 0, 6] = -1
T3[3, 3, 6, 0, 5] = 1
T3[3, 3, 6, 1, 4] = -1
T3[3, 3, 6, 2, 3] = 1
T3[3, 3, 6, 4, 1] = -1
T3[3, 3, 6, 5, 0] = 1
T3[3, 4, 1, 3, 6] = -1
T3[3, 4, 1, 6, 3] = 1
T3[3, 4, 3, 6, 1] = -1
T3[3, 4, 6, 3, 1] = 1
T3[3, 5, 0, 3, 6] = 1
T3[3, 5, 0, 6, 3] = -1
T3[3, 5, 3, 6, 0] = 1
T3[3, 5, 6, 3, 0] = -1
T3[3, 6, 0, 5, 3] = 1
T3[3, 6, 1, 4, 3] = -1
T3[3, 6, 2, 3, 3] = 1
T3[3, 6, 3, 0, 5] = -1
T3[3, 6, 3, 1, 4] = 1
T3[3, 6, 3, 3, 2] = -1
T3[3, 6, 3, 4, 1] = 1
T3[3, 6, 3, 5, 0] = -1
T3[3, 6, 4, 1, 3] = -1
T3[3, 6, 5, 0, 3] = 1
T3[4, 0, 4, 6, 5] = 1
T3[4, 0, 5, 4, 6] = 1
T3[4, 0, 5, 6, 4] = -1
T3[4, 0, 6, 4, 5] = -1
T3[4, 1, 4, 4, 6] = -1
T3[4, 1, 6, 4, 4] = 1
T3[4, 2, 3, 4, 6] = 1
T3[4, 2, 3, 6, 4] = -1
T3[4, 2, 4, 6, 3] = 1
T3[4, 2, 6, 4, 3] = -1
T3[4, 3, 2, 4, 6] = 1
T3[4, 3, 2, 6, 4] = -1
T3[4, 3, 4, 6, 2] = 1
T3[4, 3, 6, 4, 2] = -1
T3[4, 4, 0, 5, 6] = -1
T3[4, 4, 1, 6, 4] = 1
T3[4, 4, 2, 3, 6] = -1
T3[4, 4, 3, 2, 6] = -1
T3[4, 4, 4, 1, 6] = 1
T3[4, 4, 4, 6, 1] = -1
T3[4, 4, 5, 0, 6] = -1
T3[4, 4, 6, 0, 5] = 1
T3[4, 4, 6, 1, 4] = -1
T3[4, 4, 6, 2, 3] = 1
T3[4, 4, 6, 3, 2] = 1
T3[4, 4, 6, 5, 0] = 1
T3[4, 5, 0, 4, 6] = 1
T3[4, 5, 0, 6, 4] = -1
T3[4, 5, 4, 6, 0] = 1
T3[4, 5, 6, 4, 0] = -1
T3[4, 6, 0, 5, 4] = 1
T3[4, 6, 1, 4, 4] = -1
T3[4, 6, 2, 3, 4] = 1
T3[4, 6, 3, 2, 4] = 1
T3[4, 6, 4, 0, 5] = -1
T3[4, 6, 4, 2, 3] = -1
T3[4, 6, 4, 3, 2] = -1
T3[4, 6, 4, 4, 1] = 1
T3[4, 6, 4, 5, 0] = -1
T3[4, 6, 5, 0, 4] = 1
T3[5, 0, 5, 5, 6] = 1
T3[5, 0, 6, 5, 5] = -1
T3[5, 1, 4, 5, 6] = -1
T3[5, 1, 4, 6, 5] = 1
T3[5, 1, 5, 6, 4] = -1
T3[5, 1, 6, 5, 4] = 1
T3[5, 2, 3, 5, 6] = 1
T3[5, 2, 3, 6, 5] = -1
T3[5, 2, 5, 6, 3] = 1
T3[5, 2, 6, 5, 3] = -1
T3[5, 3, 2, 5, 6] = 1
T3[5, 3, 2, 6, 5] = -1
T3[5, 3, 5, 6, 2] = 1
T3[5, 3, 6, 5, 2] = -1
T3[5, 4, 1, 5, 6] = -1
T3[5, 4, 1, 6, 5] = 1
T3[5, 4, 5, 6, 1] = -1
T3[5, 4, 6, 5, 1] = 1
T3[5, 5, 0, 6, 5] = -1
T3[5, 5, 1, 4, 6] = 1
T3[5, 5, 2, 3, 6] = -1
T3[5, 5, 3, 2, 6] = -1
T3[5, 5, 4, 1, 6] = 1
T3[5, 5, 5, 0, 6] = -1
T3[5, 5, 5, 6, 0] = 1
T3[5, 5, 6, 0, 5] = 1
T3[5, 5, 6, 1, 4] = -1
T3[5, 5, 6, 2, 3] = 1
T3[5, 5, 6, 3, 2] = 1
T3[5, 5, 6, 4, 1] = -1
T3[5, 6, 0, 5, 5] = 1
T3[5, 6, 1, 4, 5] = -1
T3[5, 6, 2, 3, 5] = 1
T3[5, 6, 3, 2, 5] = 1
T3[5, 6, 4, 1, 5] = -1
T3[5, 6, 5, 1, 4] = 1
T3[5, 6, 5, 2, 3] = -1
T3[5, 6, 5, 3, 2] = -1
T3[5, 6, 5, 4, 1] = 1
T3[5, 6, 5, 5, 0] = -1
