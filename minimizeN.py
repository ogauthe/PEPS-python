#!/usr/bin/env python3

import numpy as np
import scipy.linalg as lg
from sys import argv
from time import time
from scipy.optimize import minimize
from PEPS_global import mvirt
from CTMRG import sparseCTM, CTM_Z2


print("#" * 79, "Optimize tensor A = sum a_i*A_i, with a_0 = 1", sep="\n")
print("under H = cos(theta)*H1 + sin(theta)*H2", "#" * 79, sep="\n")
if len(argv) < 2:
    inp = "input_sample/input_sample_minimizeN.txt"
    print("\nNo input file given, use", inp)
else:
    inp = argv[1]
    print("\nTake input parameters from file", inp)

with open(inp, "r") as fin:
    A_i_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    cplxL = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    peps_sqrt_s = fin.readline().split("=")[1].strip()  # peps_sqrt name or '0'
    pcolDs = fin.readline().split("=")[1].strip()  # pseudo-colors
    force_real = bool(int(fin.readline().split("=")[1]))
    sparse = bool(int(fin.readline().split("=")[1]))
    H1s, H2s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    thetaopi = float(fin.readline().split("=")[1])  # parameter of Hamilt
    niter_check = int(fin.readline().split("=")[1])  # CTMRG niter_check
    mintol = float(fin.readline().split("=")[1])  # tol to converge minimizer
    ctmvtol = float(fin.readline().split("=")[1])  # CTM tol to compute hval
    ctmgtol = float(fin.readline().split("=")[1])  # CTM tol to compute hval
    epsg = float(fin.readline().split("=")[1])  # step size to compute grad
    a_i0 = np.fromstring(fin.readline().split("=")[1], sep=",")  # initial values
    random_tries = int(fin.readline().split("=")[1])
    chi0L_min = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    chi0L_sc = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    saveCTM0 = fin.readline().split("=")[1].split()[0]
    saveFile = fin.readline().split("=")[1].split()[0]

a_i0[0] = 1  # enforce a_0 = 1
print(f"A_i = {A_i_s}")
print(f"cplxL = {cplxL}")
print(f"peps_sqrt = {peps_sqrt_s}, pcolD = {pcolDs}")
print(f"force_real = {force_real}")
print(f"sparse = {sparse}")
print(f"H1 = {H1s}, H2 = {H2s}")
print(f"theta = {thetaopi}*pi")
print(f"niter_check = {niter_check}")
print(f"minimization tolerance = {mintol}")
print(f"CTMRG tolerance to evalue Hval = {ctmvtol}")
print(f"CTMRG tolerance to evalue gradient = {ctmgtol}")
print(f"finite difference for gradient = {epsg}")
print(f"initial value for a_i0 = {a_i0}")
print(f"number of random tries at low chi0 = {random_tries}")
print(f"minimize H for chi0 in {chi0L_min}")
print(f"operate finite-entanglement scaling for chi0 in {chi0L_sc}")
print(f"save CTM data in file {saveCTM0}$chi0.npz")
print(f"save energies from entanglement scaling in file {saveFile}")


try:
    with np.load("tensors.npz") as fin:
        A_i = fin["tRVB2"]
except FileNotFoundError:
    print("", "#" * 79, "file tensors.npz not found, recompute it", sep="\n")
    import save_tensors

    print("#" * 79, "\n")

with np.load("tensors.npz") as fin:
    A_i = [fin[s] for s in A_i_s]
    pcolD = fin[pcolDs]
    H1 = fin[H1s]
    H2 = fin[H2s]
    if peps_sqrt_s != "0":
        peps_sqrt = fin[peps_sqrt_s]
        print("peps_sqrt =", peps_sqrt, sep="\n")
        sqrt_inv = peps_sqrt.conj()
    else:
        print("no peps_sqrt")
        peps_sqrt = None
        sqrt_inv = None

print("pcolD =", pcolD, sep="\n")
nparam = len(A_i) - 1  # set a0 to 1: N-1 parameters
for i in range(nparam + 1):
    if peps_sqrt is not None:
        A_i[i] = mvirt(A_i[i], peps_sqrt)
    if cplxL[i]:
        A_i[i] = 1j * A_i[i]

da_i = epsg * np.eye(nparam + 1)[1:]
if not sparse:
    A_i = np.array([A / lg.norm(A) for A in A_i]).transpose(1, 2, 3, 4, 5, 0).copy()

H0 = np.array([H1, H2])
thv = np.array([np.cos(thetaopi * np.pi), np.sin(thetaopi * np.pi)])
Ht = np.tensordot(thv, H0, (0, 0))
print("H spectrum:", lg.eigvalsh(Ht), sep="\n")


def Hval(x, ctm):  # converge ctm at new A value and compute energy
    print(f"compute energy at x = {x}", end="... ", flush=True)
    if sparse:
        ctm.set_A([1.0, *x], A_i, force_real=force_real)
    else:
        ctm.set_A(np.dot(A_i, np.array([1.0, *x])), force_real=force_real)
    niter = ctm.converge(ctmvtol, niter_check=niter_check)
    rho = ctm.compute_density_matrix(tol=ctmvtol)
    ctm.hval = np.einsum("ij,ji", Ht, rho.real)  # store it for gradient
    print(f"done, iter = {niter:3d}, chi = {ctm.chi:3d}, val = {ctm.hval:.12f}")
    return ctm.hval


def grad(x, ctm):
    print(f"compute gradient at x = {x}", end="... ", flush=True)
    g = np.empty(nparam)
    niter = 0
    chi = 0
    a_i = np.array([1.0, *x])
    for i in range(nparam):
        if sparse:
            ctm_g = sparseCTM(
                a_i + da_i[i],
                A_i,
                ctm.chi0,
                pcolD,
                s=ctm.s,
                T=ctm.T,
                pcol=ctm.pcol,
                force_real=force_real,
            )
        else:
            ctm_g = CTM_Z2(
                np.dot(A_i, a_i + da_i[i]),
                ctm.chi0,
                pcolD,
                s=ctm.s,
                T=ctm.T,
                pcol=ctm.pcol,
                force_real=force_real,
            )
        niter += ctm_g.converge(ctmgtol, niter_check=niter_check)
        chi = max(chi, ctm_g.chi)
        rho = ctm_g.compute_density_matrix(ctmgtol)
        g[i] = np.einsum("ij,ij", Ht, rho.real)
    print(f"done, iter = {niter}, chi <= {ctm.chi}")
    return (g - ctm.hval) / epsg


def get_min(a_i0, chi0):
    if sparse:
        ctm = sparseCTM(a_i0, A_i, chi0, pcolD, force_real=force_real)
    else:
        ctm = CTM_Z2(np.dot(A_i, a_i0), chi0, pcolD, force_real=force_real)
    t = time()
    np.set_printoptions(precision=6, suppress=True, sign=" ", floatmode="fixed")
    Hval(a_i0[1:], ctm)  # initialize ctm.hval at (a_i,chi0)
    res = minimize(Hval, a_i0[1:], args=(ctm,), method="CG", tol=mintol, jac=grad)
    np.set_printoptions(precision=8, suppress=False, sign="-", floatmode="maxprec")
    print(f"finished, time = {time() - t:.1f}")
    print("Result:\n", res)
    ctm.check(tol=ctmvtol, sqrt_inv=sqrt_inv)
    return res


###############################################################################
#                           seek minimum candidates                           #
###############################################################################
np.set_printoptions(precision=6, suppress=True, sign=" ", floatmode="fixed")
print(
    "\n"
    + "#" * 79
    + "\n#"
    + " " * 27
    + "seek minimum candidates"
    + " " * 27
    + "#\n"
    + "#" * 79
)
chi0 = chi0L_min[0]
pts = []
vals = []
print(f"Minimize Hamilt using conjugate gradient, start with chi0 = {chi0}")
print(f"\ni = 0: start from input initial point a_i0 = {a_i0}")
res = get_min(a_i0, chi0)
pts.append(res.x)
vals.append(res.fun)

print(
    "", "#" * 79, f"start from random guesses, n_tries = {random_tries}", "", sep="\n"
)
for i in range(1, random_tries + 1):
    a_rand = np.random.random(len(a_i0)) * 2 - 1
    a_rand[0] = 1
    print(f"i = {i}: start from a_i = {a_rand}")
    res = get_min(a_rand, chi0)
    pts.append(res.x)
    vals.append(res.fun)
    print("", "#" * 79, "", sep="\n")

print("Done, compare results:")
np.set_printoptions(precision=6, suppress=True, sign=" ", floatmode="fixed")
for i in range(random_tries + 1):
    print(f"i = {i}, x = {pts[i]}, v = {vals[i]}")
np.set_printoptions(precision=8, suppress=False, sign="-", floatmode="maxprec")
i = np.argmin(vals)
a_i = np.array([1, *pts[i]])
print(f"keep best energy from i = {i} with a_i = {a_i}")


###############################################################################
#                          minimize for larger chi0                           #
###############################################################################
print(
    "\n"
    + "#" * 79
    + "\n#"
    + " " * 26
    + "minimize for larger chi0"
    + " " * 27
    + "#\n"
    + "#" * 79
)
print(f"gradually raise chi0 with fresh CTM, loop for chi0 in {chi0L_min[1:]}")
for chi0 in chi0L_min[1:]:
    print("", "#" * 79, "", sep="\n")
    print(f"\nset chi0 to {chi0}")
    res = get_min(a_i, chi0)
    a_i = np.array([1.0, *res.x])

###############################################################################
#                         entanglement scaling                                #
###############################################################################
print(
    "\n"
    + "#" * 79
    + "\n#"
    + " " * 28
    + "entanglement scaling"
    + " " * 29
    + "#\n"
    + "#" * 79
)
print(f"Launch finite entanglement scaling for a_i = {a_i}")
print(f"chi0L_scaling = {chi0L_sc}")
d = {
    "a_i": a_i,
    "A_i_s": A_i_s,
    "cplxL": cplxL,
    "peps_sqrt_s": peps_sqrt_s,
    "pcolDs": pcolDs,
    "force_real": force_real,
    "sparse": sparse,
}
print("At each step, compute energy and xi and save data in file", saveFile)
print(
    "with keys: H1s, H2s, thetaopi, niter_check, mintol, ctmvtol, ctmgtol",
    "epsg, chiL,\n           eps0pi2",
    *d.keys(),
    sep=", ",
)
print(f"and CTM data in file {saveCTM0}$chi0.npz")
print("with keys: s, T, pcol, chi0", *d.keys(), sep=", ")

chiL, eps0pi2 = [], []  # might get twice the same chi or crash, use lists
if sparse:
    ctm = sparseCTM(a_i, A_i, chi0L_sc[0], pcolD, force_real=force_real)
else:
    ctm = CTM_Z2(np.dot(A_i, a_i), chi0L_sc[0], pcolD, force_real=force_real)

for chi0 in chi0L_sc:
    ctm.chi0 = chi0
    print(f"\ncompute energy for chi0 = {chi0}", end="... ", flush=True)
    niter = ctm.converge(ctmvtol, niter_check=niter_check)
    if ctm.chi in chiL:
        print("done, chi = {ctm.chi}, same fixed point as before")
    else:
        rho = ctm.compute_density_matrix(tol=ctmvtol)
        eps0pi2.append(np.tensordot(H0, rho.real, ((1, 2), (0, 1))))
        chiL.append(ctm.chi)
        print(f"done, iter = {niter}, chi = {ctm.chi}, val = {thv@eps0pi2[-1]}")
        boundSpec2, degen = ctm.check_boundSpec2(tol=ctmvtol, sqrt_inv=sqrt_inv)
        print("check SU(N): boundSpec2 degeneracies =", degen)
        np.savez_compressed(
            saveFile,
            H1s=H1s,
            H2s=H2s,
            thetaopi=thetaopi,
            niter_check=niter_check,
            mintol=mintol,
            ctmvtol=ctmvtol,
            epsg=epsg,
            ctmgtol=ctmgtol,
            chiL=chiL,
            eps0pi2=eps0pi2,
            **d,
        )
        print("obs data saved in file", saveFile)
    saveCTM = saveCTM0 + f"{chi0}.npz"
    np.savez_compressed(saveCTM, s=ctm.s, T=ctm.T, pcol=ctm.pcol, chi0=chi0, **d)
    print("CTM data saved in file", saveCTM)

######################################

print("\nFinished,", end=" ")
ctm.check(tol=ctmvtol, sqrt_inv=sqrt_inv)

print("", "#" * 79, "Program completed", sep="\n")
print(f"theta = {thetaopi}*pi")
print(f"a_i = {a_i}")
print(f"chiL = {chiL}")
print("energies =", np.array2string(np.array(eps0pi2) @ thv, separator=", "))
