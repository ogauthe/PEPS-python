#!/usr/bin/env python3

###############################################################################
#
# Save tensors, peps_sqrt and pseud-colors in more convient .npz file
#
###############################################################################

import numpy as np
import scipy.linalg as lg
from tensors.tensors_SU3_AKLT import tAKLT3
from tensors.tensors_SU2_2p1 import tRVB2_31_A1, tRVB2_31_A2
from tensors.tensors_SU4_Ti import p77to1, T0, T1, T2, T3
from tensors.tensors_SU4_XY import p99to1, Xe, Xo, Ye1, Yo1, Ye2, Yo2, Ye3, Yo3
from tensors.tensors_SU4_Zi import Z1, Z2, Z3, Z4, Z5
from tensors.tensors_SU6_Wi import W1, W2, W3
from gen_SUN import genSU4_6, genN

d = {}

# AKLT SU(2) and SU(3) tensors, staggered => no peps_sqrt
tAKLT2 = np.zeros((5, 2, 2, 2, 2))
tAKLT2[0, 0, 0, 0, 0] = 1
tAKLT2[1, 1, 0, 0, 0] = 1 / 2
tAKLT2[1, 0, 1, 0, 0] = 1 / 2
tAKLT2[1, 0, 0, 1, 0] = 1 / 2
tAKLT2[1, 0, 0, 0, 1] = 1 / 2
tAKLT2[2, 1, 1, 0, 0] = 1 / np.sqrt(6)
tAKLT2[2, 1, 0, 1, 0] = 1 / np.sqrt(6)
tAKLT2[2, 1, 0, 0, 1] = 1 / np.sqrt(6)
tAKLT2[2, 0, 1, 1, 0] = 1 / np.sqrt(6)
tAKLT2[2, 0, 1, 0, 1] = 1 / np.sqrt(6)
tAKLT2[2, 0, 0, 1, 1] = 1 / np.sqrt(6)
tAKLT2[3, 0, 1, 1, 1] = 1 / 2
tAKLT2[3, 1, 0, 1, 1] = 1 / 2
tAKLT2[3, 1, 1, 0, 1] = 1 / 2
tAKLT2[3, 1, 1, 1, 0] = 1 / 2
tAKLT2[4, 1, 1, 1, 1] = 1
d["tAKLT2"] = tAKLT2
d["pcol_AKLT2"] = np.array([[1, -1]], dtype=np.int8).T

d["tAKLT3"] = tAKLT3
d["pcol_AKLT3"] = 1 - 2 * np.eye(3, dtype=np.int8)[:, 1:]

# RVB N-Nb tensors, staggered => no peps_sqrt
for N in range(2, 7):
    A = np.zeros((N, N + 1, N + 1, N + 1, N + 1), dtype=np.int8)
    for i in range(N):
        A[i, i, N, N, N] = 1
        A[i, N, i, N, N] = 1
        A[i, N, N, i, N] = 1
        A[i, N, N, N, i] = 1
    d[f"tRVB{N}"] = A
    d[f"pcol_RVB{N}"] = 1 - 2 * np.eye(N + 1, dtype=np.int8)[:, 1:]

# SU(2) extended RVB tensors (2+1)^4 --> 2
d["tRVB2_31_A1"] = tRVB2_31_A1
d["tRVB2_31_A2"] = tRVB2_31_A2
d["pcol_extRVB2"] = np.array([[1, 1, -1]]).T

# SU(4) 6+1, tensors Ti
d["T0"] = T0  # actually same as RVB6
d["T1"] = T1
d["T2"] = T2
d["T3"] = T3
d["sqrt61"] = np.rint(lg.sqrtm(p77to1))
d["pcol61"] = 1 - 2 * np.eye(7, dtype=np.int8)[:, 1:]

# SU(4) 4+4b+1, tensors X/Y
d["Xe"] = Xe
d["Xo"] = Xo
d["Ye1"] = Ye1
d["Yo1"] = Yo1
d["Ye2"] = Ye2
d["Yo2"] = Yo2
d["Ye3"] = Ye3
d["Yo3"] = Yo3
d["tA1_4444b"] = (Xe + Xo) // 2
d["tA2_4444b1"] = (Ye1 + Yo1) // 2
d["tA2_4444b2"] = (Ye2 + Yo2) // 2
ind41 = [0, 1, 2, 3, 8]
d["t41"] = (Ye3 + Yo3)[np.ix_(range(6), ind41, ind41, ind41, ind41)] // 2
d["pcol41"] = (1 - 2 * np.eye(5, dtype=np.int8))[:, 1:]
d["sqrt44b1"] = np.rint(lg.sqrtm(4 * p99to1))
d["sqrt44b"] = d["sqrt44b1"][:8, :8]
d["pcol44b1"] = np.array(
    [
        [1, 1, 1, 1],
        [-1, 1, 1, 1],
        [1, -1, 1, 1],
        [-1, -1, 1, 1],
        [1, 1, -1, 1],
        [-1, 1, -1, 1],
        [1, -1, -1, 1],
        [-1, -1, -1, 1],
        [1, 1, 1, -1],
    ]
)

d["pcol44b"] = np.array(
    [
        [1, 1, 1],
        [-1, 1, 1],
        [1, -1, 1],
        [-1, -1, 1],
        [1, 1, -1],
        [-1, 1, -1],
        [1, -1, -1],
        [-1, -1, -1],
    ]
)

d["pcolXY13"] = np.array(
    [
        [1, 1, 1, 1],
        [-1, 1, 1, 1],
        [1, -1, 1, 1],
        [1, 1, -1, 1],
        [1, 1, 1, -1],
        [-1, 1, 1, -1],
        [1, -1, 1, -1],
        [1, 1, -1, -1],
    ]
)

# SU(4) 4+6, tensors Z
d["pcolZ"] = np.array(
    [
        [1, 1, 1, 1],
        [-1, 1, 1, 1],
        [1, -1, 1, 1],
        [1, 1, -1, 1],
        [1, 1, 1, -1],
        [1, -1, -1, -1],
        [1, -1, 1, -1],
        [-1, 1, -1, -1],
        [-1, 1, 1, -1],
        [-1, -1, -1, -1],
    ]
)
d["Z1"] = Z1
d["Z2"] = Z2
d["Z3"] = Z3
d["Z4"] = Z4
d["Z5"] = Z5

# SU(6) 6+6b+1, tensors Wi
T0_13 = np.zeros((6, 13, 13, 13, 13))  # tensors T0 with D=13
for i in range(6):
    T0_13[i, i, 12, 12, 12] = 1
    T0_13[i, 12, i, 12, 12] = 1
    T0_13[i, 12, 12, i, 12] = 1
    T0_13[i, 12, 12, 12, i] = 1

d["T0_13"] = T0_13
d["W1"] = W1
d["W2"] = W2
d["W3"] = W3

pcol13 = np.ones((13, 7), dtype=np.int8)
pcol13[[1, 2, 3, 4, 5], [0, 1, 2, 3, 4]] = -1
pcol13[[7, 8, 9, 10, 11], [0, 1, 2, 3, 4]] = -1
pcol13[6:12, 5] = -1
pcol13[12, 6] = -1
d["pcol13"] = pcol13
d["pcol13_0i"] = pcol13[:, [0, 1, 2, 3, 4, 6]]  # needed when mixing Wi and T0


# SU(4) Hamiltonians 6-6 and 6-6*
SdS_66 = sum(np.kron(mat, mat).real for mat in genSU4_6)
d["SdS_66"] = SdS_66
d["SdS2_o4_66"] = (SdS_66 @ SdS_66) / 4
SdS_66b = sum(np.kron(mat, -mat.conj()).real for mat in genSU4_6)
d["SdS_66b"] = SdS_66b
d["SdS2_o4_66b"] = (SdS_66b @ SdS_66b) / 4

# SU(4) operators for irrep 6
d["SzSU4_6"] = genSU4_6[2].real
d["genSU4_6"] = genSU4_6

# SU(N) fundamental irrep generators
for N in range(2, 7):
    d[f"SzSU{N}_{N}"] = genN[N][2].real
    d[f"genSU{N}_{N}"] = genN[N]


np.savez_compressed("tensors.npz", **d)
print("saved tensors in file tensors.npz")
print("with keys:", ", ".join(d.keys()))
