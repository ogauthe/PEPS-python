#!/usr/bin/env python3

###############################################################################
#
# construct SU(4) tensors X and Y
# implement SU(4), C_4v and CPT symmetries
# S = 6
# D = 8 + 1
# V = 4 + 4b + 1
#
# There are 8 rotation invariant tensors for (4+4b+1) -> 6.
# decompose them according to parity (P) and charge conjugation (C) symmetries
# and hole number.
# C_4v A_1 => parity even tensors X
# C_4v A_2 => parity odd tensors Y
# C exchanges 4 and 4b, define even (e) and odd (o) tensors
# the number of holes is either 0 or 2
# => tensors are Xo, Xe, A1 with hole number 0
#                Ye1, Ye2, Yo1, Ye2, A2 with hole number 0
#                Ye3, Yo3, A2 with hole number 2
#
# they can all be constructed by diagonalization of (4+4b+1) Casimir:
# but we prefer a less bruteforce way: these tensors can be constructed with
# combination of D = 8 and D = 5.
#
###############################################################################

import numpy as np
import scipy.linalg as lg
from exact_diagonalization import diagHamilt1D
from gen_SUN import genSU4_4, colorsSU4_4
from sym_tensors_tools import rep_C4v, sum_4sites, c2_4sites, rotate2exact


# 1) construct tensors Xe, Xo, Ye12 and Yo12 without hole, D=8
g44b = np.zeros((15, 8, 8), dtype=complex)
g44b[:, :4, :4] = genSU4_4
g44b[:, 4:, 4:] = -genSU4_4.conj()
colors44b = np.zeros((8, 4), dtype=np.int8)
colors44b[:4, :3] = colorsSU4_4
colors44b[4:, :3] = -colorsSU4_4
colors44b[:, 3] = [1] * 4 + [-1] * 4  # easily count number of 4 and 4b

cas4096 = c2_4sites(g44b)
spec, mom, col, base = diagHamilt1D(cas4096, 4, colors44b)  # square 2x2 == line
print(lg.norm(spec - np.rint(spec)))
spec = np.rint(spec)

s8_A = np.logical_not(mom.astype(bool) | (spec - 5).astype(bool)).nonzero()[0]
s8_A_31_0 = np.intersect1d(
    s8_A, np.logical_not((col - np.array([0, 1, 0, 2])).any(axis=1)).nonzero()[0]
)
s8_A_13_0 = np.intersect1d(
    s8_A, np.logical_not((col - np.array([0, 1, 0, -2])).any(axis=1)).nonzero()[0]
)

print(lg.norm(base[:, s8_A].data.imag))  # momentum=0 => real proj (sparse base)
pA31_0 = base[:, s8_A_31_0].real.toarray()
pA13_0 = base[:, s8_A_13_0].real.toarray()

# split A1 and A2
rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd2 = rep_C4v(8)
m31 = pA31_0.T @ sigv1 @ pA31_0
print(lg.norm(m31.T - m31))
s31, U31 = lg.eigh(m31)
indA1_31 = np.rint(s31 + 1).nonzero()[0]
indA2_31 = np.rint(s31 - 1).nonzero()[0]
pA31_0 = pA31_0 @ U31
pA31_0[:, :2] = rotate2exact(pA31_0[:, 0], pA31_0[:, 1]).T

m13 = pA13_0.T @ sigv1 @ pA13_0
print(lg.norm(m13.T - m13))
s13, U13 = lg.eigh(m13)
indA1_13 = np.rint(s13 + 1).nonzero()[0]
indA2_13 = np.rint(s13 - 1).nonzero()[0]
pA13_0 = pA13_0 @ U13
pA13_0[:, :2] = rotate2exact(pA13_0[:, 0], pA13_0[:, 1]).T

# act with Sminus to get other vectors.
S1m44b, S2m44b, S3m44b = np.zeros((3, 8, 8))
S1m44b[1, 0] = 1
S1m44b[4, 5] = -1
S2m44b[2, 1] = 1
S2m44b[5, 6] = -1
S3m44b[3, 2] = 1
S3m44b[6, 7] = -1
S1m4096 = sum_4sites(S1m44b)
S2m4096 = sum_4sites(S2m44b)
S3m4096 = sum_4sites(S3m44b)

pA31_1 = S2m4096 @ pA31_0
pA31_2 = S3m4096 @ pA31_1
pA31_3 = S1m4096 @ pA31_1
pA31_4 = S1m4096 @ pA31_2
pA31_5 = S2m4096 @ pA31_4

pA13_1 = S2m4096 @ pA13_0
pA13_2 = S3m4096 @ pA13_1
pA13_3 = S1m4096 @ pA13_1
pA13_4 = S1m4096 @ pA13_2
pA13_5 = S2m4096 @ pA13_4

# define charge conguation on different representations
cconj6 = np.zeros((6, 6))  # charge conjugation for irrep 6
cconj6[[0, 2, 3, 5], [5, 3, 2, 0]] = 1
cconj6[[1, 4], [4, 1]] = -1
cconj9 = [4, 5, 6, 7, 0, 1, 2, 3, 8]  # charge conjugation for V = 4+4b+1
cconj8 = cconj9[:-1]  # charge conjugation for V = 4+4b

X31 = (
    np.array(
        [
            pA31_2[:, indA1_31],
            pA31_1[:, indA1_31],
            pA31_5[:, indA1_31],
            pA31_0[:, indA1_31],
            pA31_4[:, indA1_31],
            pA31_3[:, indA1_31],
        ]
    )
    .transpose(2, 0, 1)
    .reshape(6, 8, 8, 8, 8)
)
tA2_31 = (
    np.array(
        [
            pA31_2[:, indA2_31],
            pA31_1[:, indA2_31],
            pA31_5[:, indA2_31],
            pA31_0[:, indA2_31],
            pA31_4[:, indA2_31],
            pA31_3[:, indA2_31],
        ]
    )
    .transpose(2, 0, 1)
    .reshape(2, 6, 8, 8, 8, 8)
)
Y31_1 = tA2_31[0]
Y31_2 = tA2_31[1]

# we want A13 and A31 to be charge conjugate from each other.
# diagonalization adds arbitrary sign, so *define* A13 as charge conjugate of
# A31 and check diagonalization gives same result up to a sign
# instead of DOY sign fix with A13 *= -1 "because I know"

X13 = np.tensordot(cconj6, X31, (1, 0))[
    np.ix_(range(6), cconj8, cconj8, cconj8, cconj8)
]
X13_check = (
    -np.array(
        [
            pA13_2[:, indA1_13],
            pA13_1[:, indA1_13],
            pA13_5[:, indA1_13],
            pA13_0[:, indA1_13],
            pA13_4[:, indA1_13],
            pA13_3[:, indA1_13],
        ]
    )
    .transpose(2, 0, 1)
    .reshape(6, 8, 8, 8, 8)
)
if lg.norm(X13 - X13_check) > 1e-12 and lg.norm(X13 + X13_check) > 1e-12:
    raise ValueError("X13 and X13_check differ")

tA2_13 = (
    np.array(
        [
            pA13_2[:, indA2_13],
            pA13_1[:, indA2_13],
            pA13_5[:, indA2_13],
            pA13_0[:, indA2_13],
            pA13_4[:, indA2_13],
            pA13_3[:, indA2_13],
        ]
    )
    .transpose(2, 0, 1)
    .reshape(2, 6, 8, 8, 8, 8)
)
Y13_1 = np.tensordot(cconj6, Y31_1, (1, 0))[
    np.ix_(range(6), cconj8, cconj8, cconj8, cconj8)
]
Y13_2 = np.tensordot(cconj6, Y31_2, (1, 0))[
    np.ix_(range(6), cconj8, cconj8, cconj8, cconj8)
]
if lg.norm(Y13_1 - tA2_13[0]) > 1e-12 and lg.norm(Y13_1 + tA2_13[0]) > 1e-12:
    raise ValueError("Y13_1 and Y13_1check differ")
if lg.norm(Y13_2 - tA2_13[1]) > 1e-12 and lg.norm(Y13_2 + tA2_13[1]) > 1e-12:
    raise ValueError("Y13_2 and Y13_2check differ")

# our tensors 13 and 31 are actually the image of each other by charge conjugaison
# up to a global phase that we already set to 1
# They are expressed in the same base 4-4b, no change to 4b-4.
# define even and odd (e/o) tensors for charge conjugation operation
# A_1 => parity even tensors X
Xe = X31 + X13
Xo = X31 - X13
# A_2 => parity odd tensors Y
Ye1 = Y31_1 + Y13_1
Yo1 = Y31_1 - Y13_1
Ye2 = Y31_2 + Y13_2
Yo2 = Y31_2 - Y13_2

# exactify tensors
print(lg.norm(Xe - np.rint(Xe * np.sqrt(80)) / np.sqrt(80)))
Xe = np.rint(Xe * np.sqrt(80)).astype(np.int8)
print(lg.norm(Xo - np.rint(Xo * np.sqrt(80)) / np.sqrt(80)))
Xo = np.rint(Xo * np.sqrt(80)).astype(np.int8)

print(lg.norm(Ye1 - np.rint(Ye1 * np.sqrt(32)) / np.sqrt(32)))
Ye1 = np.rint(Ye1 * np.sqrt(32)).astype(np.int8)
print(lg.norm(Yo1 - np.rint(Yo1 * np.sqrt(32)) / np.sqrt(32)))
Yo1 = np.rint(Yo1 * np.sqrt(32)).astype(np.int8)

print(lg.norm(Ye2 - np.rint(Ye2 * np.sqrt(160)) / np.sqrt(160)))
Ye2 = np.rint(Ye2 * np.sqrt(160)).astype(np.int8)
print(lg.norm(Yo2 - np.rint(Yo2 * np.sqrt(160)) / np.sqrt(160)))
Yo2 = np.rint(Yo2 * np.sqrt(160)).astype(np.int8)


# 2) construct tensors Ye3 and Yo3 from 4+1 and 4b+1 tensors
g4p1 = np.zeros((15, 5, 5), dtype=complex)
g4p1[:, :4, :4] = genSU4_4
colors4p1 = np.zeros((5, 4), dtype=np.int8)
colors4p1[:4, :3] = colorsSU4_4
colors4p1[4, 3] = 1  # use hole number

cas625 = c2_4sites(g4p1)  # same for 4+1 and 4b+1
spec5, mom5, col5, base5 = diagHamilt1D(cas625, 4, colors4p1)
print(lg.norm(spec5 * 4 - np.rint(4 * spec5)))
spec5 = np.rint(spec5 * 4) / 4

s5_A0 = np.logical_not(
    mom5.astype(bool)
    | (spec5 - 5).astype(bool)
    | (col5 - np.array([0, 1, 0, 2])).any(axis=1)
).nonzero()[0]
print(lg.norm(base5[:, s5_A0].data.imag))
pA2_0 = base5[:, s5_A0].real.toarray()  # can be checked it is C_4v A2

# act with Sminus to get other weights
S1m4p1, S2m4p1, S3m4p1 = np.zeros((3, 5, 5))
S1m4p1[1, 0] = 1
S2m4p1[2, 1] = 1
S3m4p1[3, 2] = 1
S1m625 = sum_4sites(S1m4p1)
S2m625 = sum_4sites(S2m4p1)
S3m625 = sum_4sites(S3m4p1)

pA2_1 = S2m625 @ pA2_0
pA2_2 = S3m625 @ pA2_1
pA2_3 = S1m625 @ pA2_1
pA2_4 = S1m625 @ pA2_2
pA2_5 = S2m625 @ pA2_4

T9_A2 = np.zeros((6, 6, 9, 9, 9, 9))
t41 = [0, 1, 2, 3, 8]
T4411 = np.array([pA2_2, pA2_1, pA2_5, pA2_0, pA2_4, pA2_3]).reshape(6, 5, 5, 5, 5)
# fancy indexing with many non-contiguous sub-arrays works with only np.ix_
T9_A2[np.ix_([0], range(6), t41, t41, t41, t41)] = T4411
T9_A2[1, :, 4:, 4:, 4:, 4:] = np.tensordot(cconj6, T4411, (1, 0))  # charge conjugate
t4b1 = range(4, 9)

Ye3 = T9_A2[0] + T9_A2[1]
Yo3 = T9_A2[0] - T9_A2[1]
print(lg.norm(Ye3 - np.rint(Ye3 * np.sqrt(8)) / np.sqrt(8)))
Ye3 = np.rint(Ye3 * np.sqrt(8)).astype(np.int8)
print(lg.norm(Yo3 - np.rint(Yo3 * np.sqrt(8)) / np.sqrt(8)))
Yo3 = np.rint(Yo3 * np.sqrt(8)).astype(np.int8)


# 3) check everything is fine
rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd2 = rep_C4v(9)  # C_4v rep with D=9

g44b1 = np.zeros((15, 9, 9), dtype=complex)
g44b1[:, :4, :4] = genSU4_4
g44b1[:, 4:8, 4:8] = -genSU4_4.conj()
cas6561 = c2_4sites(g44b1)


def check(A0, parity=1, cconj=1):
    if A0.shape == (6, 8, 8, 8, 8):
        A = np.zeros((6, 9, 9, 9, 9))
        A[:, :8, :8, :8, :8] = A0
    else:
        A = A0
    Av = np.sqrt(6) * A.reshape(6, 9**4) / lg.norm(A)
    print(lg.norm(Av @ cas6561 @ Av.T - 5 * np.eye(6)))
    print(lg.norm(Av @ rot1 @ Av.T - np.eye(6)))
    print(lg.norm(Av @ sigv1 @ Av.T - parity * np.eye(6)))
    print(
        lg.norm(
            A
            - cconj
            * np.tensordot(cconj6, A, (1, 0))[
                np.ix_(range(6), cconj9, cconj9, cconj9, cconj9)
            ]
        )
    )


check(Xe)
check(Xo, cconj=-1)
check(Ye1, parity=-1)
check(Ye2, parity=-1)
check(Ye3, parity=-1)
check(Yo1, parity=-1, cconj=-1)
check(Yo2, parity=-1, cconj=-1)
check(Yo3, parity=-1, cconj=-1)
