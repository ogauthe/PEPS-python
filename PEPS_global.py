import numpy as np
import scipy.sparse as sp
import scipy.linalg as lg
import itertools


def sparse_reshape(m, sh1, nx, sh2=None, copy=False):
    """
    Take a sparse matrix m, reshape it as a sparse tensor with shape sh1
    transpose the axes according to nx and reshape it as matrix with shape sh2
    return sparse matrix with csr storage
    """
    if sh2 is None:
        sh2 = m.shape
    if len(nx) != len(sh1) or (np.sort(nx) != np.arange(len(sh1))).any():
        raise ValueError("axes do not match sh1")
    if len(sh2) != 2:  # csr constructor error is unclear
        raise ValueError("return shape must be a matrix")
    size = m.shape[0] * m.shape[1]
    if np.prod(sh1) != size or np.prod(sh2) != size:
        raise ValueError("invalid shape for matrix size")

    cumprod1 = np.array([1, *sh1[:0:-1]]).cumprod()[::-1]
    cumprod2 = np.array([1, *[sh1[i] for i in nx[:0:-1]]]).cumprod()[::-1]
    if m.shape[1] == 1:  # allow optimization by extern reshape to 1D
        ind1D = m.row
    else:
        dtype = np.int64 if size > 2**32 else np.int32
        ind1D = np.multiply(m.shape[1], m.row, dtype=dtype) + m.col
    ind1D = (ind1D[:, None] // cumprod1 % sh1)[:, nx] @ cumprod2
    return sp.csr_matrix((m.data, np.divmod(ind1D, sh2[1])), shape=sh2, copy=copy)


def hamilt_sum_Nv_sites(h_ij, D, Nv, coordination=None, h_i=None):
    """
    Construct the Hamiltonien H = sum_ij h_ij (+sum_i h_i) on Nv sites from
    2-site term h_ij and 1-site term h_i if provided. If not specified, coordination
    is assumed to be 1-dimensional chain with periodic boundary conditions.

    Parameters
    ----------
    h_ij : (D**2,D**2) numpy array
        2-site term
    D : integer
        local dimension
    Nv : integer
        number of sites to consider
    coordination: (Nv,Nv) boolean array
        coordination matrix. If not provided, consider 1-dimensional chain with periodic
        boundary conditions. Only upper triangular part is read.
    h_i : (D,D) numpy array
        1-site term. If not provided, no single-site term is added.

    Returns
    -------
    H: (D**Nv,D**Nv) sparse csr_matrix
        Hamiltonian acting on Nv sites
    """

    if h_ij.shape != (D**2, D**2):
        raise ValueError("invalid shape for h_ij")

    if coordination is None:  # default  = 1D chain with PBC
        coordination = np.zeros((Nv, Nv), dtype=bool)
        link = list(range(1, Nv)) + [0]
        coordination[link, range(Nv)] = True
        coordination[range(Nv), link] = True
    else:
        if coordination.shape != (Nv, Nv):
            raise ValueError("invalid shape for coordination")
        if coordination.dtype != bool:
            raise TypeError("coordination dtype must be boolean")

    shH = (D**Nv, D**Nv)
    shN = (D,) * (2 * Nv)
    H = sp.csr_matrix(shH, dtype=h_ij.dtype)

    if h_i is not None:  # one-site operators
        if h_i.shape != (D, D):
            raise ValueError("invalid shape for h11")
        temp = (
            sp.kron(h_i, sp.eye(D ** (Nv - 1)))
            .tocoo(copy=False)
            .reshape(D ** (2 * Nv), 1)
        )
        for i in range(Nv):
            nx = np.arange(2 * Nv)
            nx[[0, i, Nv, i + Nv]] = i, 0, i + Nv, Nv  # swap sites 0 and i
            H += sparse_reshape(temp, shN, nx, sh2=shH)

    # 2-site operators
    temp = (
        sp.kron(h_ij, sp.eye(D ** (Nv - 2))).tocoo(copy=False).reshape(D ** (2 * Nv), 1)
    )
    for i, j in itertools.combinations(range(Nv), 2):
        if coordination[i, j]:
            nx = np.arange(2 * Nv)
            nx[[0, i, Nv, Nv + i]] = i, 0, Nv + i, Nv  # swap sites 0 and i
            nx[[nx[1], nx[j], nx[Nv + 1], nx[Nv + j]]] = (
                j,
                1,
                j + Nv,
                Nv + 1,
            )  # swap sites 1 and j
            H += sparse_reshape(temp, shN, nx, sh2=shH)

    H.eliminate_zeros()  # just in case
    return H


def mvirt(A0, m, div=None, stag=False):
    """
    Apply matrix m to every virtual leg of tensor A0 - typically sqrt(projDDto1)
    if stag, apply staggered m/m.conj()
    if div is set, expect A coefficients to be integer multiples of div and divide by
    it.
    """
    n_virt_leg = A0.ndim - 1
    A = A0.transpose(list(range(1, n_virt_leg + 1)) + [0])
    if stag:
        assert not n_virt_leg % 2, "Cannot stagger with odd number of virtual legs"
        mc = m.conj()
        for i in range(n_virt_leg // 2):
            A = np.tensordot(A, m, (0, 0))
            A = np.tensordot(A, mc, (0, 0))
    else:
        for i in range(n_virt_leg):
            A = np.tensordot(A, m, (0, 0))
    if div is not None:
        assert lg.norm(np.rint(A / div) * div - A) < 1e-15, "A is not multiple of div"
        A = np.rint(A / div)
    return A


def dimYT(N, yt):
    """
    compute the dimension of the irrep of SU(N) with Young tableaux yt
    """
    num, den = 1, 1
    for i in range(len(yt)):  # loop on each row
        for j in range(yt[i]):  # loop on each column of the row
            num *= N + j - i
            right = yt[i] - j - 1  # number of boxes right current
            below = 0  # number of boxes below current
            k = i + 1
            while k < len(yt) and yt[k] > j:
                below += 1
                k += 1
            den *= right + below + 1
    return num // den


def doZipperStep(zipperVec, E, Nv):
    """
    use 'zipper' to converge to the leading eigenvector of the transfer matrix
    ie. add tensor E on each leg of a rank-Nv tensor X
      1            Nv-1 ─┐              0 ─┐
      │            Nv-2 ─┤              1 ─┤
    2─E─0           ... ─┤ Xeven      ... ─┤ Xodd
      │               1 ─┤           Nv-2 ─┤
      3               0 ─┘           Nv-1 ─┘
    """
    assert zipperVec.shape == (E.shape[0],) * Nv, "inconsistant shape for zipperVec"
    zipperVec = np.tensordot(E, zipperVec, (0, 0))  # only one leg for 1st tensor
    for i in range(2, Nv):  # add tensor i
        zipperVec = np.tensordot(E, zipperVec, ((3, 0), (0, i + 1)))
    zipperVec = np.tensordot(
        E, zipperVec, ((3, 0, 1), (0, Nv + 1, Nv))
    )  # 3 legs for tensor Nv
    zipperVec /= lg.norm(zipperVec)
    return zipperVec


def cyclesByColor(D, Nv, siteColors, alternate=False, colorMajor=True):
    """
    construct cycles of states and sort them by color
    """
    indices = list(reversed(range(D**Nv)))
    baseD = D ** np.arange(Nv)[::-1]
    cycles, colors = [], []
    if alternate:
        ntrans = (
            2 if colorMajor else 1
        )  # if codiagh alternate rep, momentum is only defined in [0,pi]
        signs = 1 - np.arange(Nv, dtype=np.int8) % 2 * 2
    else:
        ntrans = 1
        signs = np.ones(Nv, dtype=bool)
    while indices:
        icycle = [indices.pop()]
        i0baseD = icycle[0] // baseD % D
        colors.append(signs @ siteColors[i0baseD])
        ibaseD = np.roll(i0baseD, ntrans)
        while (ibaseD != i0baseD).any():
            i = ibaseD @ baseD
            indices.remove(i)
            icycle.append(i)
            ibaseD = np.roll(ibaseD, ntrans)
        cycles += [icycle]
    # if codiagh alternate rep with 2pi momentum, colors are defined up to global sign
    if alternate and not colorMajor:
        for c in colors:
            nn = c.nonzero()[0]
            if len(nn):
                c *= np.sign(colors[nn[0]])  # set sign of first non-zero to +

    colors = np.array(colors, dtype=np.int8)
    colorOrder = np.lexsort(colors.T[::-1])  # order cycles by colors
    cycles = [cycles[i] for i in colorOrder]
    colors = colors[colorOrder]
    return cycles, colors
