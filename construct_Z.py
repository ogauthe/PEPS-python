#!/usr/bin/env python3

###############################################################################
#
# construct SU(4) tensors Z
# implement SU(4), C_4v and charge conjugation symmetries
# S = 6
# D = 10
# V = 4+6 / 4b+6
#
###############################################################################

import numpy as np
import scipy.linalg as lg
import scipy.sparse as sp
from exact_diagonalization import diagHamilt1D
from gen_SUN import genSU4_4, genSU4_6, colorsSU4_4, colorsSU4_6
from sym_tensors_tools import rep_C4v, sum_4sites, c2_4sites
from tensors.tensors_SU4_Ti import p77to1

D = 10
Nv = 4

g46 = np.zeros((15, D, D), dtype=complex)
g46[:, :4, :4] = genSU4_4
g46[:, 4:, 4:] = genSU4_6
colors46 = np.zeros((D, 4), dtype=np.int8)
colors46[:4, :3] = colorsSU4_4
colors46[4:, :3] = colorsSU4_6
colors46[4:, 3] = 1  # color for U(1) particle number

cas = c2_4sites(g46)
spec, mom, col, base = diagHamilt1D(cas, 4, colors46)  # square 2x2 == line

print("exactify spec:", lg.norm(np.rint(spec * 4) / 4 - spec))
spec = np.rint(spec * 4) / 4
r = lg.norm((base @ base.getH() - sp.eye(D**Nv)).data)
print(f"norm(U@U* - Id) = {r}")
r = lg.norm(
    (base.getH() @ cas @ base - sp.csr_matrix((spec, [list(range(D**Nv))] * 2))).data
)
print(f"norm(U@s@U* - H) = {r}")


s6_A_0 = np.logical_not(
    mom.astype(bool)
    | (spec - 5).astype(bool)
    | (col - np.array([0, 1, 0, 2])).any(axis=1)
).nonzero()[0]

print(lg.norm(base[:, s6_A_0].data.imag))  # momentum=0 => real proj (sparse base)
pA_0 = base[:, s6_A_0].real.toarray()

# split A1 and A2
rot1, rot2, rot3, sigv1, sigv2, sigd1, sigd2 = rep_C4v(D)
m = pA_0.T @ sigv1 @ pA_0
print(lg.norm(m.T - m))
s, U = lg.eigh(m)
indA1 = np.rint(s + 1).nonzero()[0]
indA2 = np.rint(s - 1).nonzero()[0]
pA_0 = pA_0 @ U

pA1_0 = pA_0[:, indA1]  # actually indA1 is length 1.
pA2_0 = pA_0[:, indA2]


# act with Sminus to get other vectors.
S1m46, S2m46, S3m46 = np.zeros((3, D, D))
S1m46[1, 0] = 1
S1m46[9, 5] = 1
S1m46[8, 4] = 1
S2m46[2, 1] = 1
S2m46[5, 7] = 1
S2m46[6, 8] = 1
S3m46[3, 2] = 1
S3m46[4, 5] = 1
S3m46[8, 9] = 1
S1m_4s = sum_4sites(S1m46)
S2m_4s = sum_4sites(S2m46)
S3m_4s = sum_4sites(S3m46)

pA1_1 = S2m_4s @ pA1_0
pA1_2 = S3m_4s @ pA1_1
pA1_3 = S1m_4s @ pA1_1
pA1_4 = S1m_4s @ pA1_2
pA1_5 = S2m_4s @ pA1_4

pA2_1 = S2m_4s @ pA2_0
pA2_2 = S3m_4s @ pA2_1
pA2_3 = S1m_4s @ pA2_1
pA2_4 = S1m_4s @ pA2_2
pA2_5 = S2m_4s @ pA2_4

tA1 = (
    np.array([pA1_2, pA1_1, pA1_5, pA1_0, pA1_4, pA1_3])
    .transpose(2, 0, 1)
    .reshape(len(indA1), 6, D**4)
)
Z1 = tA1[0].reshape(6, D, D, D, D)  # unique
print(lg.norm(np.rint(np.sqrt(80) * Z1) / np.sqrt(80) - Z1))
Z1 = np.rint(Z1 * np.sqrt(80)).astype(np.int8)


tA2 = (
    np.array(
        [
            pA2_2[:, indA2],
            pA2_1[:, indA2],
            pA2_5[:, indA2],
            pA2_0[:, indA2],
            pA2_4[:, indA2],
            pA2_3[:, indA2],
        ]
    )
    .transpose(2, 0, 1)
    .reshape(len(indA2), 6, D**4)
)
tA2[np.abs(tA2) < 1e-12] = 0

# check everything is fine
cconj6 = p77to1[:6, :6] / 4
cconj10 = np.kron(np.eye(4), cconj6)  # charge conjugation for V = 4+6 -> 4b+6


def check(A, parity=1, cconj=1):
    Av = np.sqrt(6) * A.reshape(6, D**4) / lg.norm(A)
    print(lg.norm(Av @ cas @ Av.T - 5 * np.eye(6)))
    print(lg.norm(Av @ rot1 @ Av.T - np.eye(6)))
    print(lg.norm(Av @ sigv1 @ Av.T - parity * np.eye(6)))


check(Z1)


def writeTensor(T, f, name):
    f.write(name + " = np.zeros({},dtype=np.{})\n".format(T.shape, T.dtype))
    it = np.nditer(T, flags=["multi_index"])
    while not it.finished:
        if it[0]:
            f.write(
                name
                + "["
                + ",".join(map(str, it.multi_index))
                + "] = {}\n".format(it[0])
            )
        it.iternext()


with open("tensors_Zi.py", "w") as f:
    f.write("import numpy as np\n")
    f.write("\n# A1 C4v projector (4+6)^4 --> 6\n")
    writeTensor(Z1, f, "Z1")
    f.write("\n# A2 C4v projectors (6+1)^4 --> 6\n")
    for i, T in enumerate(tA2):
        writeTensor(T.reshape(6, D, D, D, D), f, f"Z{i+2}")
        f.write("\n")
