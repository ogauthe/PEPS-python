#!/usr/bin/env python3

###############################################################################
#
# Execute CTMRG algorithm for SU(N) symmetric tensors
# Corner matrix C stays hermitian as long as C_4v symmetry is unbroken.
# input: file following input_sample/input_sample_doctm.py pattern
#
###############################################################################


import numpy as np
import scipy.linalg as lg
from sys import argv
from CTMRG import CTM_Z2, sparseCTM
from PEPS_global import mvirt


print("#" * 79, "CTMRG for SU(N) symmetric tensor", sep="\n")
if len(argv) < 2:
    inp = "input_sample/input_sample_doctm.txt"
    print("\nNo input file given, use", inp)
else:
    inp = argv[1]
    print("\nTake input parameters from file", inp)

# allow to compute cheap observables only: 2-sites observables, xi and s_ent
# else call analyze_ctm
with open(inp, "r") as fin:
    A_i_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    a_i0 = np.fromstring(fin.readline().split("=")[1], sep=",")  # initial values
    cplxL = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    peps_sqrt_s = fin.readline().split("=")[1].strip()  # peps_sqrt name or '0'
    pcolDs = fin.readline().split("=")[1].strip()  # pseudo-colors
    force_real = bool(int(fin.readline().split("=")[1]))
    sparse = bool(int(fin.readline().split("=")[1]))
    warmup = int(fin.readline().split("=")[1])  # CTMRG converge warmup
    stuck = int(fin.readline().split("=")[1])
    niter_check = int(fin.readline().split("=")[1])  # CTMRG niter_check
    tol = float(fin.readline().split("=")[1])  # CTMRG tol
    chi0L = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    Hs_s = [s.strip() for s in fin.readline().split("=")[1].split(",")]
    compute_xi = bool(int(fin.readline().split("=")[1]))
    compute_s_ent = bool(int(fin.readline().split("=")[1]))
    saveCTM0 = fin.readline().split("=")[1].strip()
    saveObs = fin.readline().split("=")[1].strip()

if force_real and cplxL.any():
    print("Error: cannot force real with complex tensor")
    exit()

print(f"A_i = {A_i_s}")
print(f"a_i = {a_i0}")
print(f"cplxL = {cplxL}")
print(f"peps_sqrt = {peps_sqrt_s}, pcolD = {pcolDs}")
print(f"force_real = {force_real}")
print(f"sparse = {sparse}")
print(f"Hs = {Hs_s}")  # Hamiltonian list, or any 2-site observable
print(f"warmup = {warmup}")
print(f"stuck = {stuck}")
print(f"niter_check = {niter_check}")
print(f"CTMRG tolerance = {tol}")
print(f"Iterate for chi0 in {chi0L}")
print(f"compute_xi = {compute_xi}")
print(f"compute_s_ent = {compute_s_ent}")
print(f"save CTM data in file {saveCTM0}$chi0.npz")
print(f"save observables in file {saveObs}")


dctm = {
    "a_i": a_i0,
    "A_i_s": A_i_s,
    "cplxL": cplxL,
    "peps_sqrt_s": peps_sqrt_s,
    "pcolDs": pcolDs,
    "force_real": force_real,
    "sparse": sparse,
    "tol": tol,
    "warmup": warmup,
    "niter_check": niter_check,
}
dobs = {}  # set observables in dictionary to save them
if compute_xi:
    dobs["xiL"] = []
if compute_s_ent:
    dobs["s_entL"] = []

# load tensors
try:
    with np.load("tensors.npz") as fin:
        A_i = fin["tRVB2"]
except FileNotFoundError:
    print("", "#" * 79, "file tensors.npz not found, recompute it", sep="\n")
    import save_tensors

    print("#" * 79, "\n")

with np.load("tensors.npz") as fin:
    A_i = [fin[s] for s in A_i_s]
    pcolD = fin[pcolDs]
    print("pcolD =", pcolD, sep="\n")
    if Hs_s[0] != "0":
        compute_energy = True
        Hs = [fin[s] for s in Hs_s]
        dobs["Hs_s"] = Hs_s
        dobs["epsL"] = []
    else:
        compute_energy = False
    if peps_sqrt_s != "0":
        peps_sqrt = fin[peps_sqrt_s]
        print("peps_sqrt =", peps_sqrt, sep="\n")
        sqrt_inv = peps_sqrt.conj()
    else:
        print("no peps_sqrt")
        peps_sqrt = None
        sqrt_inv = None

# add 1j and peps_sqrt
a_i = a_i0.copy()
for i in range(len(A_i)):
    if peps_sqrt is not None:
        A_i[i] = mvirt(A_i[i], peps_sqrt)
    if cplxL[i]:
        A_i[i] = 1j * A_i[i]
    if not sparse:
        a_i[i] = a_i[i] / lg.norm(A_i[i])

chiL = []
if not sparse:
    A_i = np.array(A_i).transpose(1, 2, 3, 4, 5, 0).copy()
    ctm = CTM_Z2(np.dot(A_i, a_i), chi0L[0], pcolD, force_real=force_real)
else:
    ctm = sparseCTM(a_i, A_i, chi0L[0], pcolD, force_real=force_real)


###############################################################################
#                                computation starts
###############################################################################
print("", "#" * 79, f"Begin CTMRG loop, chi0L = {chi0L}", sep="\n")
print(f"at each step, save CTM data in file {saveCTM0}$chi0.npz")
print("with keys: s, T, pcol, chi0", *dctm.keys(), sep=", ")
print(f"and observables in file {saveObs}")
print("with keys: chiL", *dctm.keys(), *dobs.keys(), sep=", ")

for chi0 in chi0L:
    # 1) CTMRG loop
    print("", "#" * 79, f"set chi0 to {chi0}, iterate CTMRG...", sep="\n")
    ctm.chi0 = chi0
    niter = ctm.converge(
        tol, warmup=warmup, niter_check=niter_check, stuck=stuck, verbose=True
    )
    print(f"done, iter = {niter}, chi = {ctm.chi}")
    if ctm.chi in chiL:
        print("same fixed point as before, do not recompute observables")
    else:
        # 2) check results and save CTM
        ctm.check(tol)
        saveCTM = saveCTM0 + f"{chi0}.npz"
        np.savez_compressed(saveCTM, s=ctm.s, T=ctm.T, pcol=ctm.pcol, chi0=chi0, **dctm)
        print("CTM data saved in file", saveCTM)
        # 3) compute observables
        chiL.append(ctm.chi)
        if compute_xi:
            xi = ctm.compute_corr_length(maxiter=500)
            dobs["xiL"].append(xi)
            print(f"xi = {xi:.1f}")
        if compute_s_ent:
            s_ent = ctm.compute_entropy(maxiter=500)
            dobs["s_entL"].append(s_ent)
            print(f"s_ent = {s_ent:.3f}")
        if compute_energy:  # actually any 2-site observable
            rho = ctm.compute_density_matrix(tol=tol)
            eps = [np.einsum("ij,ij", m, rho).real for m in Hs]
            dobs["epsL"].append(eps)
            print(f"energies = {eps}")
        np.savez_compressed(saveObs, chiL=chiL, **dctm, **dobs)
        print("obs data saved in file", saveObs)

###############################################################################
print("\n", "#" * 79, "CTMRG loop finished.", sep="\n")
print(f"A_i = {A_i_s}")
print(f"a_i = {a_i0}")
print(f"chiL = {chiL}")
