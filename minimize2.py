#!/usr/bin/env python3

import numpy as np
import scipy.linalg as lg
from sys import argv
from time import time
from scipy.optimize import minimize_scalar
from PEPS_global import mvirt
from CTMRG import sparseCTM, CTM_Z2


print("#" * 79, "Optimize tensor A = cos(phi)*X + sin(phi)*Y for phi", sep="\n")
print("under H = cos(theta)*H1 + sin(theta)*H2", "#" * 79, sep="\n")
if len(argv) < 2:
    inp = "input_sample/input_sample_minimize2.txt"
    print("\nNo input file given, use", inp)
else:
    inp = argv[1]
    print("\nTake input parameters from file", inp)

with open(inp, "r") as fin:
    Xs, Ys = fin.readline().split("=")[1].split(",")  # string of tensor names
    peps_sqrt_s = fin.readline().split("=")[1].strip()  # peps_sqrt name or '0'
    pcolDs = fin.readline().split("=")[1].strip()  # pseudo-colors
    cplxY = bool(int(fin.readline().split("=")[1]))  # whether add 1j to Y
    force_real = bool(int(fin.readline().split("=")[1]))
    H1s, H2s = fin.readline().split("=")[1].split(",")  # string of H names
    thetaopi = float(fin.readline().split("=")[1])  # parameter of Hamilt
    sparse = bool(int(fin.readline().split("=")[1]))
    niter_check = int(fin.readline().split("=")[1])  # CTMRG niter_check
    ctmvtol = float(fin.readline().split("=")[1])  # CTM tol to compute hval
    chi0L_min = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    chi0L_sc = np.fromstring(fin.readline().split("=")[1], sep=",", dtype=int)
    saveCTM0 = fin.readline().split("=")[1].split()[0]
    saveFile = fin.readline().split("=")[1].split()[0]
# phi0, dphi and mintol are set by minimize_scalar(bounded)

Xs, Ys, H1s, H2s = Xs.strip(), Ys.strip(), H1s.strip(), H2s.strip()

if len(chi0L_min) != 2:
    print("expect chi0L_min of length 2")
    exit()

print(f"X = {Xs}, Y = {Ys}, H1 = {H1s}, H2 = {H2s}")
print(f"pcolD = {pcolDs}, peps_sqrt = {peps_sqrt_s}")
print(f"cplxY = {cplxY}")
print(f"force_real = {force_real}")
print(f"sparse = {sparse}")
print(f"theta = {thetaopi}*pi")
print(f"niter_check = {niter_check}")
print(f"CTMRG tolerance to evalue Hval = {ctmvtol}")
print(f"save CTM data in file {saveCTM0}$chi0.npz")
print(f"save energies from entanglement scaling in file {saveFile}")
print(f"minimize H for chi0 in {chi0L_min}")
print(f"operate finite-entanglement scaling for chi0 in {chi0L_sc}")


try:
    with np.load("tensors.npz") as fin:
        A_i = fin["tRVB2"]
except FileNotFoundError:
    print("", "#" * 79, "file tensors.npz not found, recompute it", sep="\n")
    import save_tensors

    print("#" * 79, "\n")

with np.load("tensors.npz") as fin:
    X = fin[Xs]
    Y = fin[Ys]
    pcolD = fin[pcolDs]
    H1 = fin[H1s]
    H2 = fin[H2s]
    if peps_sqrt_s != "0":
        peps_sqrt = fin[peps_sqrt_s]
        print("peps_sqrt =", peps_sqrt, sep="\n")
        X = mvirt(X, peps_sqrt)
        Y = mvirt(Y, peps_sqrt)
        sqrt_inv = peps_sqrt.conj()
    else:
        print("no peps_sqrt")
        sqrt_inv = None

print("pcolD =", pcolD, sep="\n")
if cplxY:
    Y = 1j * Y

if sparse:
    AL = [X, Y]
else:
    Y = Y / lg.norm(Y)  # stored as int8, type may change
    X = X.astype(Y.dtype) / lg.norm(X)
H0 = np.array([H1, H2])
thv = np.array([np.cos(thetaopi * np.pi), np.sin(thetaopi * np.pi)])
Ht = np.tensordot(thv, H0, (0, 0))
print("H spectrum:", lg.eigvalsh(Ht), sep="\n")


# converge ctm at new A value and compute energy
def Hval(phi, ctm):
    print(f"compute energy at phi = {phi:.10f}", end="... ", flush=True)
    if sparse:
        ctm.set_A([np.cos(phi), np.sin(phi)], AL, force_real=force_real)
    else:
        ctm.set_A(np.cos(phi) * X + np.sin(phi) * Y, force_real=force_real)
    niter = ctm.converge(ctmvtol, niter_check=niter_check)
    rho = ctm.compute_density_matrix(tol=ctmvtol)  # save rho
    hval = np.einsum("ij,ji", Ht, rho.real)  # hermitian rho (check) => real hval
    print(f"done, iter = {niter:3d}, chi = {ctm.chi:3d}, val = {hval:.10f}")
    return hval


print(
    """
###############################################################################
#                       minimization, Brent's algorithm                       #
###############################################################################
"""
)
print(f"First try: phi in [0,pi] with chi0 = {chi0L_min[0]}")  # E(A) = E(-A)
phi = np.random.random() * np.pi

if sparse:
    ctm = sparseCTM(
        [np.cos(phi), np.sin(phi)], AL, chi0L_min[0], pcolD, force_real=force_real
    )
else:
    ctm = CTM_Z2(
        np.cos(phi) * X + np.sin(phi) * Y, chi0L_min[0], pcolD, force_real=force_real
    )

t = time()
res = minimize_scalar(Hval, args=(ctm,), bounds=(0, np.pi), method="bounded")
print(f"finished, time = {time()-t:.1f} s")
print("Result:\n", res)
ctm.check(tol=ctmvtol, sqrt_inv=sqrt_inv)
phi = res.x

print(f"\nSecond try: phi in [{phi-0.2},{phi+0.2}] with chi0 = {chi0L_min[1]}")
print("restart with fresh CTM to limit precion loss")
if sparse:
    ctm = sparseCTM(
        [np.cos(phi), np.sin(phi)], AL, chi0L_min[1], pcolD, force_real=force_real
    )
else:
    ctm = CTM_Z2(
        np.cos(phi) * X + np.sin(phi) * Y, chi0L_min[1], pcolD, force_real=force_real
    )
t = time()
res = minimize_scalar(
    Hval, args=(ctm,), bounds=(phi - 0.2, phi + 0.2), method="bounded"
)
print(f"finished, time = {time()-t:.1f} s")
print("Result:\n", res)
ctm.check(tol=ctmvtol, sqrt_inv=sqrt_inv)
phi = res.x


print(
    """
###############################################################################
#                         entanglement scaling                                #
###############################################################################
"""
)
print(f"Launch finite entanglement scaling for phi = {phi}")
print(f"chi0L_scaling = {chi0L_sc}")
d = {
    "Xs": Xs,
    "Ys": Ys,
    "peps_sqrt_s": peps_sqrt_s,
    "phi": phi,
    "cplxY": cplxY,
    "pcolDs": pcolDs,
    "force_real": force_real,
    "sparse": sparse,
}
print("At each step, save observable data in file", saveFile)
print(
    "with keys: H1s, H2s, thetaopi, niter_check, ctmvtol, chiL, eps0pi2",
    *d.keys(),
    sep=", ",
)
print(f"and CTM data in file {saveCTM0}$chi0.npz")
print("with keys: s, T, pcol, chi0", *d.keys(), sep=", ")

chiL, eps0pi2 = [], []  # might get twice the same chi or crash, use lists
if sparse:
    ctm = sparseCTM(
        [np.cos(phi), np.sin(phi)], AL, chi0L_sc[0], pcolD, force_real=force_real
    )
else:
    ctm = CTM_Z2(
        np.cos(phi) * X + np.sin(phi) * Y, chi0L_sc[0], pcolD, force_real=force_real
    )

for i, chi0 in enumerate(chi0L_sc):
    ctm.chi0 = chi0
    print(f"\nCompute energy for chi0 = {chi0}", end="... ", flush=True)
    niter = ctm.converge(ctmvtol, niter_check=niter_check)
    if ctm.chi in chiL:
        print(f"chi = {ctm.chi}, same fixed point as before")
    else:
        rho = ctm.compute_density_matrix(tol=ctmvtol)
        eps0pi2.append(np.tensordot(H0, rho.real, ((1, 2), (0, 1))))
        chiL.append(ctm.chi)
        print(f"done, iter = {niter}, chi = {ctm.chi}, val = {thv@eps0pi2[-1]}")
        boundSpec2, degen = ctm.check_boundSpec2(tol=ctmvtol, sqrt_inv=sqrt_inv)
        print("check SU(N): boundSpec2 degeneracies =", degen)
        np.savez_compressed(
            saveFile,
            H1s=H1s,
            H2s=H2s,
            thetaopi=thetaopi,
            niter_check=niter_check,
            ctmvtol=ctmvtol,
            chiL=np.array(chiL),
            eps0pi2=np.array(eps0pi2),
            **d,
        )
        print("obs data saved in file", saveFile)
    saveCTM = saveCTM0 + f"{chi0}.npz"
    np.savez_compressed(saveCTM, s=ctm.s, T=ctm.T, pcol=ctm.pcol, chi0=chi0, **d)
    print("CTM data saved in file", saveCTM)

######################################

print("\nFinished,", end=" ")
ctm.check(tol=ctmvtol, sqrt_inv=sqrt_inv)

print("", "#" * 79, "Program completed", sep="\n")
print(f"theta = {thetaopi}*pi")
print(f"phi = {phi}")
print(f"chiL = {chiL}")
print("energies =", np.array2string(np.array(eps0pi2) @ thv, separator=", "))
