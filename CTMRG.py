import numpy as np
import scipy.linalg as lg
import scipy.sparse.linalg as slg
from time import time
from scipy.sparse import dok_matrix, csr_matrix, csc_matrix
from lanczos import Lanczos
from PEPS_global import cyclesByColor


def transferMultLeft(X, up, down):
    """
    Transfer matrix product on a given vector on the left.
    """
    #    -0          1-up-0
    #    X              2
    #    -1             1
    #                0-down-2
    Y = np.dot(X.reshape(up.shape[0], down.shape[0]), down)
    Y = np.dot(up, Y.reshape(up.shape[1], down.shape[0]))
    return Y.flat


class CTMRG(object):
    def __init__(
        self,
        A,
        chi0,
        force_real=False,
        window=50,
        break_rot=False,
        s=None,
        T=None,
        alternate=False,
    ):
        """
        initialize CTMRG algorithm for rotation invariant wavefunction on the square
        lattice.
        A may not be rotation invariant itself.
        """
        # edge tensor T is always considered to be Th, horizontal T
        # define vertical edge tensor Tv only in the methods that use it.
        # Tv = Th unless we break rotation symmetry while playing with peps_sqrt
        # in this case, define Tv = Th.swapaxes(0,1).conj()
        self._D = A.shape[1]
        self._D2 = A.shape[1] ** 2
        self.window = max(window, 3 * self._D2)  # look for chi in [chi0, chimax]
        self._alternate = alternate  # alternate rep. on the square lattice
        self._break_rot = break_rot  # may break rotation, keep Tv = Th.swap.conj()
        self.cuttol = 1 - 1e-6  # look for ratio of consecutive values < cuttol
        self.chi0 = chi0
        self.set_A(A, force_real=force_real)

        if s is not None:  # restart from previously computed s and T
            self.set_sT(s, T)

        else:  # start from E-fixed s and T
            matC = np.einsum(
                "iijjkl->kl",
                self._E.reshape(
                    (self._D, self._D, self._D, self._D, self._D2, self._D2)
                ),
            )
            self._s, U = lg.eigh(matC)
            self._chi = self._D2
            T = np.einsum(
                "iijkl->ljk",
                self._E.reshape((self._D, self._D, self._D2, self._D2, self._D2)),
            )
            self._T = np.tensordot(
                U.conj(), np.tensordot(T, U, (1, 0)), (0, 0)
            ).swapaxes(1, 2)
            while self._chi * self._D2 < chi0:  # iterate while matC is too small to cut
                matC = self.construct_matC()
                self._s, U = lg.eigh(matC)
                self._T = self.construct_newT(U)
                self._chi *= self._D2

    @property
    def chi0(self):
        return self._chi0

    @chi0.setter
    def chi0(self, chi0):  # is CTM is converged, chi0 = chi => same fixed point
        self._chi0 = chi0
        self._chimax = self.window + chi0  # max val for chi is actually chimax-2

    @property
    def chi(self):
        return self._chi

    @property  # memorize successive changes of chi to see whether iteration
    def chichanges(self):  # is stuck between 2 different chi
        return self._chichanges

    @property
    def s(self):
        return self._s

    @property
    def T(self):
        return self._T

    def set_A(self, A, force_real=False):  # force_real => no A.setter
        """
        Set new value for ket-tensor A. Allow to force real double-layer tensor E in
        case of complex A.
        """
        assert A.shape[1:] == (self._D,) * 4, "A shape is not (S,D,D,D,D)"
        if np.isrealobj(A):
            self._A = np.ascontiguousarray(A, dtype=float)  # might get integer A
        else:
            self._A = np.ascontiguousarray(A)
        E = np.tensordot(self._A, self._A.conj(), (0, 0)).transpose(
            0, 4, 1, 5, 2, 6, 3, 7
        )
        if force_real:  # complex A may result in real E
            assert lg.norm(E.imag) / lg.norm(E.real) < 1e-15, "Error: E is not real"
            E = E.real

        self._E = np.ascontiguousarray(E.reshape(self._D2**2, self._D2**2))
        self._evd = lg.lapack.dsyevd if np.isrealobj(E) else lg.lapack.zheevd
        self._chichanges = []  # change in A may change s structure

    def set_sT(self, s, T):  # cannot fix s and T separetly
        assert T.shape == (len(s), len(s), self._D2), "invalid T shape"
        self._s = s
        self._chi = len(s)
        self._T = T

    def construct_matC(self, reshape=True):
        """
        Construct new corner C by adding a tensor E to the old one.
        Final reshape (which includes copy) can be avoided.
        """
        #  Convention: legs are taken counterclockwise
        #    0               0
        #    |               |
        #  1-E-3   1-T-0     Tv-2
        #    |       |       |
        #    2       2       1

        # bypass tensordot to save memory
        # therefore legs 0-1 and 2-3 of E are merged
        k = self._chi * self._D2
        if self._s is None:  # in case C is not diagonal
            matC = np.tensordot(self._T, self._C, ((1,), (0,)))
            del self._C
        else:
            matC = np.einsum("j,ijk->ikj", self._s, self._T).reshape(k, self._chi)
        Tv = self._T.swapaxes(0, 1).conj() if self._break_rot else self._T
        matC = (matC @ Tv.reshape(self._chi, k)).reshape(
            self._chi, self._D2, self._chi, self._D2
        )
        del Tv
        matC = matC.transpose(0, 2, 1, 3).reshape(self._chi**2, self._D2**2)
        matC = matC @ self._E
        if reshape:
            matC = matC.reshape(self._chi, k, self._D2).swapaxes(1, 2).reshape(k, k)
        return matC

    def construct_newT(self, U):
        """
        Renormalize edge tensor T using diagonalization base of the new corner.
        """
        k = self._chi * self._D2  # chi is old chi
        newchi = U.shape[1]
        U = U.reshape(self._chi, self._D2 * newchi)
        T = self._T.transpose(0, 2, 1).reshape(k, self._chi)  # bypass tensordot
        T = (T @ U).reshape(self._chi, self._D2**2, newchi)
        T = T.transpose(0, 2, 1).reshape(self._chi * newchi, self._D2**2)
        T = (T @ self._E).reshape(self._chi, newchi * self._D2, self._D2)
        T = T.transpose(0, 2, 1).reshape(k, newchi * self._D2)
        U = U.reshape(self._chi, self._D2, newchi).transpose(2, 0, 1).reshape(newchi, k)
        T = (U.conj() @ T).reshape(newchi, newchi, self._D2)
        return T / lg.norm(T)

    def iterate(self):
        """
        Renormalize the upper-left corner C by adding a new tensor E.
        """
        matC = self.construct_matC()
        s, U, info = self._evd(matC, overwrite_a=1)
        if info:
            raise lg.LinAlgError("error in LAPACK diagonalization routine.")

        sSort = np.argsort(np.abs(s))[: -self._chimax : -1]  # keep chimax largest ev
        s = s[sSort]
        # cut at smallest value above chi0 where s[i]/s[i-1] < 1-eps
        newchi = (
            np.abs(s[self._chi0 :] / s[self._chi0 - 1 : -1]) < self.cuttol
        ).nonzero()[0][
            0
        ] + self._chi0  # chi0 <= chi <= chimax-2
        U = U[:, sSort[:newchi]].copy()  # keep only relevant cols in conveniant order
        newT = self.construct_newT(U)
        if newchi == self._chi:
            self._chichanges = [newchi]
        else:
            self._chichanges.append(newchi)
        self.set_sT(s[:newchi] / s[0], newT)

    def isometric_gauge(self, ncv=None, maxiter=None, tol=1e-5):
        """
        Isometrically gauge tensor T
        """
        k = self._D2 * self._chi
        up = self._T.reshape(self._chi, k)
        down = self._T.swapaxes(1, 2).conj().reshape(self._chi, k)

        def fp(X):
            return transferMultLeft(X, up, down)

        op = slg.LinearOperator((self._chi**2, self._chi**2), matvec=fp)
        C = slg.eigsh(op, k=1, ncv=ncv, tol=tol, maxiter=maxiter)[1].reshape(
            self._chi, self._chi
        )  # hermitian
        C = lg.sqrtm(C)
        Tt = self._T.transpose(1, 2, 0).reshape(self._chi, k)
        M = np.dot(C.T, Tt).reshape(k, self._chi)
        U, P = lg.polar(M)
        while lg.norm(C - P) > tol:
            down = U.reshape(self._chi, self._D2 * self._chi).conj()
            op = slg.LinearOperator(
                (self._chi**2, self._chi**2), matvec=fp
            )  # NOT hermitian
            C = slg.eigs(op, k=1, ncv=ncv, tol=tol, maxiter=maxiter)[1].reshape(
                self._chi, self._chi
            )
            _, C = lg.polar(C)
            C /= lg.norm(C)
            M = np.dot(C.T, Tt).reshape(k, self._chi)
            U, P = lg.polar(M)
            P /= lg.norm(P)
        return C, U

    def check_isometric_gauge(self, C, U):
        """
        Check isometric gauge is doing its job
        """
        k = self._chi * self._D2
        Tt = self._T.transpose(1, 2, 0).reshape(self._chi, k)
        M = np.dot(C.T, Tt).reshape(k, self._chi)
        M2 = np.dot(U.reshape(k, self._chi), C)
        return lg.norm(M / lg.norm(M) - M2 / lg.norm(M2))

    def fpC(self, X):
        """
        fixed point equation for corner C in FPCM
        C-T-U
        T-E/  =  C
         U
        """
        k = self._chi * self._D2
        newC = X.reshape(self._chi, self._chi)
        newC = np.dot(newC, self._T.reshape(self._chi, k)).reshape(
            self._chi, self._chi, self._D2
        )
        newC = newC.transpose(0, 2, 1).reshape(k, self._chi)
        newC = np.dot(newC, self._Ufp.conj().reshape(self._chi, k)).reshape(
            self._chi, self._D2**2, self._chi
        )
        newC = newC.swapaxes(1, 2).reshape(self._chi**2, self._D2**2)
        newC = (
            newC
            @ self._E.reshape((self._D2,) * 4)
            .transpose(1, 2, 0, 3)
            .reshape(self._E.shape)
        ).reshape(self._chi, self._chi, self._D2, self._D2)
        newC = newC.transpose(1, 3, 0, 2).reshape(k, k)
        newC = np.dot(newC, self._T.transpose(1, 2, 0).reshape(k, self._chi)).reshape(
            self._chi, self._D2, self._chi
        )
        newC = newC.transpose(0, 2, 1).reshape(self._chi, k)
        newC = np.dot(newC, self._Ufp.reshape(k, self._chi))
        newC = newC.T.reshape(newC.size)
        return newC

    def fpT(self, X):
        """
        fixed point equation for edge tensor T in FPCM
        """
        # U*-T-U = T
        #  \-E-/
        k = self._chi * self._D2
        newT = np.dot(
            self._Ufp.conj().transpose(1, 2, 0).reshape(k, self._chi),
            self._T.swapaxes(0, 1).reshape(self._chi, k),
        ).reshape(self._D2, self._chi**2, self._D2)
        newT = newT.transpose(1, 2, 0).reshape(self._chi**2, self._D2**2)
        newT = (newT @ self._E).reshape(self._chi, self._chi, self._D2, self._D2)
        newT = newT.transpose(0, 2, 1, 3).reshape(k, k)
        newT = np.dot(newT, self._Ufp.reshape(k, self._chi)).reshape(
            self._chi, self._D2, self._chi
        )
        newT = newT.transpose(2, 0, 1).reshape(newT.size)
        return newT

    def fpcm_step(self, tol=1e-8, ncv=None, maxiter=500):
        """
        Fixed point corner corner method (accelerate CTM convergence)
        """
        self._s = None
        self._Ufp = self.isometric_gauge(tol=tol)[1].reshape(
            self._chi, self._D2, self._chi
        )
        opC = slg.LinearOperator((self._chi**2, self._chi**2), matvec=self.fpC)
        self._C = slg.eigsh(opC, k=1, ncv=ncv, tol=tol, maxiter=maxiter)[1].reshape(
            self._chi, self._chi
        )
        opT = slg.LinearOperator(
            (self._chi**2 * self._D2, self._chi**2 * self._D2), matvec=self.fpT
        )
        self._T = slg.eigsh(opT, k=1, ncv=ncv, tol=tol, maxiter=maxiter)[1].reshape(
            self._chi, self._chi, self._D2
        )
        del self._Ufp

    def converge(
        self, tol, warmup=0, niter_check=1, niter_max=np.inf, stuck=None, verbose=False
    ):
        """
        Converge CTMRG. Iterate until ||s-s(niter_check)||∞ < tol.
        If CTMRG is stuck between 2 different values of chi, change chi0 to select one.
        """
        if stuck is None:
            stuck = max(2 * niter_check, 20)

        if verbose:
            print(f"Start with {warmup} iterations, chi0 = {self._chi0}")
        t = time()
        for i in range(warmup):  # chi may change a lot during first steps
            self.iterate()
        if verbose:
            print(
                f"Done, chi={self._chi}, time = {time() - t:.0f} s.\nIterate until",
                f"convergence, tol={tol}, niter_check={niter_check}, stuck={stuck}",
            )
        self._chichanges = []

        niter = warmup
        r = 1.0
        while r > tol and niter < niter_max:
            s0 = np.abs(self._s)
            for i in range(niter_check):
                self.iterate()
            niter += niter_check
            msg = f"niter = {niter}, time = {time() - t:.0f} s, "
            if s0.shape[0] == self._chi:
                r = lg.norm(np.abs(self._s) - s0, ord=np.inf)
                msg += f"chi = {self._chi}, ||s - s(niter-{niter_check})||∞ = {r:.1e}"
            else:
                r = 1.0
                msg += f"s0 length = {len(s0)}, s length = {self._chi}"
                if len(self._chichanges) > stuck:  # change chi0
                    self.chi0 = min(self._chichanges) + 1
                    print(
                        f"stuck: chichanges = {self._chichanges},",
                        f"set chi0 to {self._chi0}",
                    )
                    self._chichanges = []
            if verbose:
                print(msg)
        return niter

    def compute_boundMat(self, Nv, canonicalCols, sqrt_inv=None):
        """
        compute boundary matrix sigma of a semi-infinite cylinder with Nv sites from
        edge tensor T. Use translation and SU(N) invariance to compute non-zero
        coefficients only. The reduced density matrix of the system is then given by
        rho = sqrt(sigma_L.T) sigma_R sqrt(sigma_L.T)
        """

        if sqrt_inv is None:
            T = self._T
        else:
            T = np.tensordot(self._T, np.kron(sqrt_inv, sqrt_inv.conj()), (2, 0))
        Topen = (
            T.reshape(self._chi, self._chi, self._D, self._D)
            .transpose(2, 3, 0, 1)
            .copy()
        )  # open bra-ket
        cycles, colors = cyclesByColor(
            self._D, Nv, canonicalCols, alternate=self._alternate
        )
        color_sectors = [
            0,
            *((colors[:-1] != colors[1:]).any(axis=1).nonzero()[0] + 1),
            len(cycles),
        ]

        sigma = dok_matrix((self._D**Nv, self._D**Nv), dtype=Topen.dtype)
        baseD = self._D ** np.arange(Nv)[::-1]

        # sigma is translation and SU(N) invariant.
        # Hence the only non vanishing matrix element correspond to states with same
        # color and same momentum. In real space, this means 2 cycles of length n1 and
        # n2 and same color have d = gcd(n1,n2) different matrix elements, all the
        # others elements can be translated back to 11, 12, ..., 1d
        for imin, imax in zip(color_sectors, color_sectors[1:]):
            for cycL in cycles[imin:imax]:  # for each cycle in a color sector
                nL = len(cycL)
                bra0 = cycL[0] // baseD % self._D
                for cycR in cycles[imin:imax]:
                    vals = []  # compute only few needed values
                    # most cycles are length Nv, avoid calling gcd
                    mod = nL if nL == len(cycR) else np.gcd(nL, len(cycR))
                    for indR in cycR[:mod]:
                        # compute values as a matrix product over T submatrices
                        ket = indR // baseD % self._D
                        M = Topen[bra0[0], ket[0]]
                        for b, k in zip(bra0[1:-1], ket[1:-1]):
                            M = M @ Topen[b, k]
                        # double contraction is faster than trace( M @ lastT)
                        vals.append(
                            np.tensordot(M, Topen[bra0[-1], ket[-1]], ((0, 1), (1, 0)))
                        )

                    for li, indL in enumerate(cycL):
                        for ri, indR in enumerate(cycR):
                            sigma[indL, indR] = vals[
                                (ri - li) % mod
                            ]  # set non-zero coeff
        sigma /= slg.norm(sigma)  # return entMat/norm would imply copy.
        return sigma.tocsc()

    def transferMult(self, X):
        """
        transfer matrix product on a given vector.
        Input and output are 1D vector of length chi^2.
        """
        Y = X.reshape(self._chi, self._chi)  # reshape to 2D
        return np.tensordot(
            np.tensordot(Y, self._T, (0, 0)), self._T, ((0, 2), (1, 2))
        ).flat

    def compute_corr_length(self, ncv=None, maxiter=None, tol=0):
        """
        compute the correlation length xi of the system.
        xi is defined as the inverse of the gap Delta, with Delta = log(abs(v1/v2))
        v1 and v2 being the two leading eigenvalues of the transfer matrix.
        """
        # merging corr_length and entropy computation is a bad idea.
        # One needs 2 eigenvalues, the other 1 eigenvector. Computing 2 eigenvectors
        # fails.
        try:
            op = slg.LinearOperator(
                (self._chi**2, self._chi**2), matvec=self.transferMult
            )
            v2, v1 = slg.eigsh(
                op, k=2, ncv=ncv, tol=tol, maxiter=maxiter, return_eigenvectors=False
            )
        except slg.ArpackNoConvergence:  # eigsh is unstable
            print("ARPACK did not converge, use custom Lanczos")
            L = Lanczos(self.transferMult, self._chi**2)
            v1, v2 = L.eigvals(2, ncv=maxiter)  # not very reliable, iterate a lot
        return 1 / np.log(abs(v1 / v2))

    def compute_entropy(self, ncv=None, maxiter=None, tol=0):
        """
        compute the environment entanglement entropy s of the system.
        s is defined as Tr(rho log rho), where rho is the square of the leading
        eigenvector of the transfert matrix
        """
        op = slg.LinearOperator(
            (self._chi**2, self._chi**2), matvec=self.transferMult
        )
        sigma = slg.eigsh(op, k=1, ncv=ncv, tol=tol, maxiter=maxiter)[1].reshape(
            self._chi, self._chi
        )
        # phase of sigma is arbitrary: remove it.
        t = np.trace(sigma)
        sigma = (t.conj() * sigma).real
        rho = sigma @ sigma
        rho /= np.trace(rho)
        return -np.einsum("ij,ji", rho, lg.logm(rho))  # fastest way for trace(A*B)

    def sandwichA(self, op):
        """
        Take an operator in physical space and return it between A and A.conj() to
        be inserted in the tensor network.
        Real symmetric op may give rise to complex tensor.
        """
        n = op.ndim - 2  # number of legs of operator before matrix (S,S)
        return np.ascontiguousarray(
            np.tensordot(np.tensordot(op, self._A, (n, 0)), self._A.conj(), (n, 0))
            .transpose(*range(n), n, n + 4, n + 1, n + 5, n + 2, n + 6, n + 3, n + 7)
            .reshape(*op.shape[:-2], self._D**4, self._D**4)
        )

    def addCol(self, env, op=None):
        """
        Add a column T-op-T to the environment.
        env : (chi, D^2, chi) array, CTM environment.
        op : (D^4, D^4) array, scalar observable to compute.
        """
        if op is None:
            op = self._E
        elif op.shape != (self._D**4, self._D**4):
            raise ValueError("operator shape inaccurate")
        newEnv = np.tensordot(env, self._T, (0, 1)).transpose(2, 1, 3, 0)
        if np.isrealobj(newEnv) and np.iscomplexobj(op):
            newEnv = newEnv.astype(op.dtype)
        newEnv = np.ascontiguousarray(newEnv.reshape(self._chi**2, self._D**4))
        newEnv = (newEnv @ op).reshape(self._chi, self._chi, self._D2, self._D2)
        newEnv = np.ascontiguousarray(newEnv.transpose(0, 3, 1, 2))
        newEnv = np.tensordot(newEnv, self._T, ((2, 3), (0, 2)))
        return newEnv

    def corr_SzSz(self, Sz, rmax, tol=1e-12):
        """
        Compute Sz(0)Sz(r) correlator for r=1..rmax+1. Then S.S = (N^2-1)*SzSz for an
        SU(N) symmetric tensor. Consider same tensor A on every sites, but the irrep may
        be alternate, then use Sz alt = -Sz.conj()
        memory: 2*chi^2*D^4, same as iterate.
        """
        Tv = self._T.swapaxes(0, 1).conj() if self._break_rot else self._T
        endColumn = np.einsum("i,j,ijk->ikj", self._s, self._s, Tv)  # C-Tv-C
        del Tv
        normEnv = self.addCol(endColumn)
        renorm = lg.norm(normEnv) / lg.norm(endColumn)

        SzEnv = self.addCol(endColumn, self.sandwichA(Sz))  # C-T-C // T-Sz-T
        endSz = [SzEnv.T.copy()] * 2
        if self._alternate:
            endSz[0] = self.addCol(endColumn, self.sandwichA(-Sz.conj())).T.copy()

        endColumn = normEnv.T.copy()  # C-T-C // T-E-T on other end
        corr = np.empty(rmax)
        c0 = np.tensordot(SzEnv, endSz[0], 3) / np.tensordot(normEnv, endColumn, 3)
        if abs(c0.imag / c0.real) > tol:
            raise ValueError("Observable is not real")
        corr[0] = c0.real
        Enorm = self._E / renorm  # set transfertMat T-E-T leading eigval to 1
        for r in range(1, rmax):
            SzEnv = self.addCol(SzEnv, Enorm)
            normEnv = self.addCol(normEnv, Enorm)
            corr[r] = (
                np.tensordot(SzEnv, endSz[r % 2], 3)
                / np.tensordot(normEnv, endColumn, 3)
            ).real  # values smaller than error may not be real

        return corr / corr[0]

    def dimerEnvE(self, S_op):
        """
        construct environment for one dimer C-T-T // Tv-S-S // C-T-T
        Memory use: 2*chi^2*D^4*len(S_op), complex array.
        """
        Tv = self._T.swapaxes(0, 1).conj() if self._break_rot else self._T
        dimerEnv = np.einsum("i,j,ijk->ikj", self._s, self._s, Tv)
        del Tv
        T = self._T.astype(complex, copy=False)
        dimerEnv = np.tensordot(dimerEnv, T, (0, 1))
        S1 = self.sandwichA(S_op).reshape((len(S_op),) + (self._D**2,) * 4)
        S1 = S1.swapaxes(0, 2).copy()
        dimerEnv = np.tensordot(dimerEnv, S1, ((0, 3), (0, 1)))
        del S1  # sandwichA is fast and S1 is not small
        dimerEnv = dimerEnv.transpose(1, 2, 4, 0, 3).copy()  # memory intensive
        dimerEnv = np.tensordot(dimerEnv, T, ((3, 4), (0, 2)))
        dimerEnv = np.tensordot(dimerEnv, T, (0, 1))
        dimerEnv = dimerEnv.transpose(2, 3, 0, 4, 1).copy()  # memory intensive
        if self._alternate:
            S2 = self.sandwichA(-S_op.conj()).reshape(
                (len(S_op),) + (self._D**2,) * 4
            )
        else:
            S2 = self.sandwichA(S_op).reshape((len(S_op),) + (self._D**2,) * 4)
        dimerEnv = np.tensordot(dimerEnv, S2, ((2, 3, 4), (0, 1, 2)))
        dimerEnv = np.tensordot(dimerEnv, T, ((0, 2), (0, 2)))
        return dimerEnv  # small tensor, same size as T

    def dimerEnvAA(self, S_op):
        """
        construct environment for one dimer C-T-T // Tv-S-S // C-T-T
        Memory use: 2*chi^2*D^4*len(A), complex array.
        """
        To = self._T.reshape(self._chi, self._chi, self._D, self._D).astype(
            complex, copy=False
        )
        Tv = To.swapaxes(0, 1).conj() if self._break_rot else To
        dimerEnv = np.einsum("i,j,ijkl->iklj", self._s, self._s, Tv)
        del Tv
        dimerEnv = np.tensordot(dimerEnv, To, (0, 1))
        dimerEnv = np.tensordot(dimerEnv, self._A, ((0, 4), (2, 1)))
        dimerEnv = dimerEnv.transpose(0, 2, 3, 4, 6, 1, 5).copy()  # memory intensive
        To = To.swapaxes(1, 2).copy()
        dimerEnv = np.tensordot(dimerEnv, To, ((5, 6), (0, 1)))  # memory intensive
        dimerEnv = dimerEnv.transpose(1, 4, 5, 3, 2, 0, 6).copy()  # memory intensive
        SA = (
            np.tensordot(S_op, self._A.conj(), (2, 0))
            .transpose(1, 2, 3, 4, 5, 0)
            .copy()
        )
        dimerEnv = np.tensordot(dimerEnv, SA, ((3, 4, 5, 6), (0, 1, 2, 3)))
        if self._alternate:
            SA = np.tensordot(-S_op.conj(), self._A.conj(), (2, 0)).transpose(
                1, 2, 3, 4, 5, 0
            )
        dimerEnv = np.tensordot(dimerEnv, SA, ((3, 4), (2, 5)))
        del SA
        dimerEnv = dimerEnv.transpose(0, 1, 3, 4, 6, 2, 5).copy()  # memory intensive
        To = To.transpose(0, 3, 2, 1).copy()
        dimerEnv = np.tensordot(dimerEnv, To, ((5, 6), (0, 1)))  # memory intensive
        dimerEnv = dimerEnv.transpose(0, 3, 4, 5, 2, 1, 6).copy()  # memory intensive
        dimerEnv = np.tensordot(dimerEnv, self._A, ((4, 5, 6), (0, 2, 3)))
        To = To.transpose(2, 3, 1, 0).copy()
        dimerEnv = np.tensordot(dimerEnv, To, ((0, 4, 1), (0, 1, 2)))
        dimerEnv = dimerEnv.transpose(3, 1, 2, 0).reshape(
            self._chi, self._chi, self._D2
        )
        dimerEnv = dimerEnv.swapaxes(1, 2)
        return dimerEnv  # small tensor, same size as T

    def construct_dimerEnv(self, S_op, tol=1e-12):
        """
        construct environment for one dimer C-T-T // T-S-S // C-T-T
        Memory use: 2*chi^2*D^4*min(len(A),len(S_op)), complex array
        S_op : (N^2-1,S,S) tensor, acting on physical leg representation of SU(N)
        """
        if len(self._A) > len(S_op):  # select method to minimize memory.
            dimerEnv = self.dimerEnvE(S_op)
        else:
            dimerEnv = self.dimerEnvAA(S_op)  # double layer is always faster
        if np.isrealobj(self._T):
            if lg.norm(dimerEnv.imag) / lg.norm(dimerEnv) > tol:
                raise ValueError("Environment is not real")
            dimerEnv = dimerEnv.real
        return np.ascontiguousarray(dimerEnv)

    def corr_dimer(self, S_op, rmax, tol=1e-12):
        """
        Compute (S(0).S(1))(S(r).S(r+1)) correlator for r=2..rmax+2
        S_op : (N^2-1,len(A),len(A)) tensor, acting on physical leg representation of
          SU(N)
        rmax : int, maximum value for r
        Memory use: 2*chi^2*D^4*min(len(A),len(S_op)), complex array.
        """
        # memory intensive part is inside construct_dimerEnv
        dimerEnv = self.construct_dimerEnv(S_op, tol)
        endDimer = dimerEnv.T.copy()
        Tv = self._T.swapaxes(0, 1).conj() if self._break_rot else self._T
        endColumn = np.einsum("i,j,ijk->ikj", self._s, self._s, Tv)  # C-Tv-C
        del Tv
        normEnv = self.addCol(self.addCol(endColumn))  # C-T-C // T-E-T // T-E-T
        renorm = np.sqrt(lg.norm(normEnv) / lg.norm(endColumn))
        connex = np.tensordot(dimerEnv, endColumn.T, 3) / np.tensordot(
            normEnv, endColumn.T, 3
        )  # connex part <S.S>^2
        if abs(connex.imag / connex.real) > tol:
            raise ValueError("Observable is not real")
        connex = connex.real**2
        endColumn = normEnv.T.copy()

        Enorm = self._E / renorm  # set transfertMat T-E-T leading eigval to 1
        c0 = np.tensordot(dimerEnv, endDimer, 3) / np.tensordot(normEnv, endColumn, 3)
        if abs(c0.imag / c0.real) > tol:
            raise ValueError("Observable is not real")
        corr = np.empty(rmax)
        corr[0] = c0.real - connex
        for r in range(1, rmax):
            dimerEnv = self.addCol(dimerEnv, Enorm)  # add col T-E-T while keeping
            normEnv = self.addCol(normEnv, Enorm)  # norm close to 1
            corr[r] = (
                np.tensordot(dimerEnv, endDimer, 3)
                / np.tensordot(normEnv, endColumn, 3)
            ).real - connex

        return corr / corr[0]

    def compute_density_matrix(self, tol=1e-12):
        """
        Compute 2-sites density matrix rho of the wavefunction.
        memory: 2*chi^2*D^4*len(A), optimal.
        CPU: chi^3*D^5*len(A)
        """
        Topen = self._T.reshape(self._chi, self._chi, self._D, self._D)  # open bra-ket
        Tv = Topen.swapaxes(0, 1).conj() if self._break_rot else Topen
        mat = np.einsum("i,j,ijkl->iklj", self._s, self._s, Tv)  # C // Tv // C
        del Tv
        mat = np.tensordot(mat, Topen, (0, 1))  # C-T // Tv // C
        mat = np.tensordot(mat, self._A, ((0, 4), (2, 1)))
        mat = mat.transpose(0, 2, 3, 4, 6, 1, 5).copy()  # saturates memory
        mat = np.tensordot(mat, Topen, ((5, 6), (0, 2)))
        mat = mat.transpose(1, 3, 5, 4, 2, 0, 6).copy()  # saturates memory
        mat = np.tensordot(mat, self._A.conj(), ((4, 5, 6), (1, 2, 3)))
        mat = np.tensordot(mat, mat, ((0, 2, 3, 5), (2, 0, 3, 5)))
        mat = mat.swapaxes(1, 2).reshape(self._A.shape[0] ** 2, self._A.shape[0] ** 2)
        mat /= np.trace(mat)
        if lg.norm(mat - mat.T.conj()) > tol:
            raise ValueError("Density matrix is not hermitian")
        return mat

    def compute_rho3(self, tol=1e-12):
        """
        Compute 3-sites density matrix rho of the wavefunction.
        memory: 2*chi^2*D^4*d^3, optimal.
        CPU: chi^3*D^5*d^3
        """
        d = self._A.shape[0]
        Topen = self._T.reshape(self._chi, self._chi, self._D, self._D)  # open bra-ket
        rhoL = np.einsum("i,j,ijkl->iklj", self._s, self._s, Topen)  # C // Tv // C
        rhoL = np.tensordot(rhoL, Topen, (0, 1))  # C-T // Tv // C
        rhoL = np.tensordot(rhoL, self._A, ((0, 4), (2, 1)))
        rhoL = rhoL.transpose(0, 2, 3, 4, 6, 1, 5).copy()
        rhoL = np.tensordot(rhoL, Topen, ((5, 6), (0, 2)))
        rhoL = rhoL.transpose(1, 3, 5, 4, 2, 0, 6).copy()
        rhoL = np.tensordot(rhoL, self._A.conj(), ((4, 5, 6), (1, 2, 3)))

        rho = np.tensordot(rhoL, Topen, (0, 1))
        rho = np.tensordot(rho, self._A, ((2, 6), (2, 1)))
        rho = rho.transpose(0, 2, 6, 8, 4, 5, 3, 1, 7).copy()  # saturates
        rho = np.tensordot(rho, Topen, ((7, 8), (0, 2)))
        rho = rho.transpose(0, 1, 2, 4, 7, 3, 5, 6, 8).copy()  # saturates
        rho = np.tensordot(rho, self._A.conj(), ((6, 7, 8), (1, 2, 3)))

        rho = np.tensordot(rho, rhoL, ((3, 4, 5, 7), (2, 0, 3, 5)))
        rho = rho.transpose(0, 2, 4, 1, 3, 5).reshape(d**3, d**3)
        rho /= np.trace(rho)
        if lg.norm(rho - rho.T.conj()) > tol:
            raise ValueError("Density matrix is not hermitian")
        return rho

    def compute_rho2x2(self, tol=1e-12):
        """
        Compute 4-site density matrix of 2x2 plaquette
        memory: 2*chi**2*D**4*d^4, optimal.
        """
        d = self._A.shape[0]
        Topen = self._T.reshape(self._chi, self._chi, self._D, self._D)  # open bra-ket
        rho22 = np.einsum("i,ijkl->iklj", self._s, Topen)  # C // T
        rho22 = np.tensordot(rho22, Topen, (0, 1))  # C-T // T // C
        rho22 = np.tensordot(rho22, self._A, ((0, 4), (2, 1)))  # C-T // T-A
        rho22 = np.tensordot(rho22, self._A.conj(), ((0, 3), (2, 1)))
        rho22 = np.tensordot(rho22, rho22, ((1, 4, 7), (0, 3, 6)))
        rho22 = np.tensordot(rho22, rho22, ((0, 2, 4, 5, 7, 9), (5, 7, 9, 0, 2, 4)))
        rho22 = rho22.transpose(0, 2, 6, 4, 1, 3, 7, 5).reshape(d**4, d**4)
        rho22 /= np.trace(rho22)
        if lg.norm(rho22 - rho22.T.conj()) > tol:
            raise ValueError("Density matrix is not hermitian")
        return rho22

    def check_boundSpec2(self, sqrt_inv=None, tol=1e-12):
        """
        Compute the spectrum of the boundary matrix sigma on 2 sites with bruteforce to
        check its degeneracies.
        If sigma_L = sigma_R.T, it is equal to the square root of the reduced density
        matrix spectrum.
        Normalize spectrum with euclidean norm equal to 1.
        """
        if sqrt_inv is None:
            T = self.T
        else:
            T = np.tensordot(self.T, np.kron(sqrt_inv, sqrt_inv.conj()), (2, 0))
        TT = np.tensordot(T, T, ((0, 1), (1, 0))).reshape((self._D,) * 4)
        TT = TT.swapaxes(1, 2).reshape((self._D**2, self._D**2)) / lg.norm(TT)
        if lg.norm(TT - TT.T.conj()) > tol:
            raise ValueError("entMat2 is not hermitian")
        boundSpec2 = lg.eigvalsh(TT)  # cannot use symmetry to check symmetry!
        values = list(np.rint(boundSpec2 / tol) * tol)
        deg = [values.count(v) for v in set(values)]
        deg = [(deg.count(n), n) for n in sorted(set(deg))]
        return boundSpec2, deg


class CTM_Z2(CTMRG):
    def __init__(
        self,
        A,
        chi0,
        pcolD,
        force_real=False,
        s=None,
        T=None,
        pcol=None,
        window=50,
        break_rot=False,
        alternate=False,
    ):
        """
        specialize CTMRG algorithm to use Z_2^n symmetry in renormalization
        """
        self._D = A.shape[1]
        self._D2 = A.shape[1] ** 2
        self.window = max(window, 4 * self._D2)  # look for chi in [chi0, chimax]
        self._alternate = alternate  # alternate rep. on the square lattice
        self._break_rot = break_rot  # may break rotation, keep Tv = Th.swap.conj()
        self.cuttol = 1 - 1e-6  # look for ratio of consecutive values < cuttol

        self.set_A(A, force_real=force_real)
        self._ncol = pcolD.shape[1]
        self._pcolD2 = np.einsum("ik,jk->ijk", pcolD, pcolD).reshape(
            (self._D2, self._ncol)
        )

        if s is not None:
            self.chi0 = chi0
            self.set_sT(s, T, pcol)

        else:
            self.reset(chi0)

    def reset(self, chi0):
        """
        Restart CTMRG from E-based s and T.
        """
        matC = np.einsum(
            "iijjkl->kl",
            self._E.reshape((self._D, self._D, self._D, self._D, self._D2, self._D2)),
        )
        colorOrder = np.lexsort(self._pcolD2.T)
        pcol = self._pcolD2[colorOrder].copy()
        bInds = [0, *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1), self._D2]
        s = np.empty(self._D2)
        U = np.zeros((self._D2, self._D2), dtype=self._E.dtype)
        for i, j in zip(bInds, bInds[1:]):  # diagonalize blocks
            s[i:j], U[colorOrder[i:j], i:j] = lg.eigh(
                matC[colorOrder[i:j, None], colorOrder[i:j]]
            )

        assert lg.norm(U.T.conj() @ matC @ U - np.diag(s)) / lg.norm(matC) < 1e-12
        sSort = np.argsort(np.abs(s))[::-1]
        U = U[:, sSort]  # keep only relevant cols in conveniant order
        T = np.einsum(
            "iijkl->ljk",
            self._E.reshape((self._D, self._D, self._D2, self._D2, self._D2)),
        )
        T = np.tensordot(U.conj(), np.tensordot(T, U, (1, 0)), (0, 0)).swapaxes(1, 2)
        self.set_sT(s[sSort], T, pcol[sSort])

        while self._chi * self._D2 < chi0:  # if chi0 is too large, take steps
            self.chi0 = self._chi * self._D2 // 2
            self.iterate()
        self.chi0 = chi0

    @property
    def pcol(self):
        return self._pcol

    def set_sT(self, s, T, pcol):  # cannot fix s, T and pcol separetly
        assert T.shape == (len(s), len(s), self._D2), "invalid T shape"
        assert pcol.shape == (len(s), self._ncol), "invalid pcol shape"
        self._s = s
        self._T = T
        self._pcol = pcol
        self._chi = len(s)

    def iterate(self):
        k = self._D2 * self._chi
        matC = self.construct_matC(reshape=False)

        # Z_2 diagonalization
        pcol = np.einsum("ik,jk->ijk", self._pcol, self._pcolD2).reshape(k, self._ncol)
        colorOrder = np.lexsort(pcol.T)
        pcol = pcol[colorOrder]
        bInds = [0, *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1), k]

        c1 = colorOrder // self._D2
        c2 = colorOrder % self._D2
        s = np.empty(k)
        U = np.zeros((k, k), dtype=self._E.dtype)
        for i, j in zip(bInds, bInds[1:]):  # diagonalize blocks
            s[i:j], U[colorOrder[i:j], i:j], info = self._evd(
                matC[
                    c1[i:j, None] * self._chi + c1[i:j],
                    c2[i:j] * self._D2 + c2[i:j, None],
                ],
                overwrite_a=1,
            )
            if info:  # fancy slicing creates a copy, so no overwrite on matC.
                print("error in dsyevd / zheevd routine, use lg.eigh")
                s[i:j], U[colorOrder[i:j], i:j] = lg.eigh(
                    matC[
                        c1[i:j, None] * self._chi + c1[i:j],
                        c2[i:j] * self._D2 + c2[i:j, None],
                    ],
                    overwrite_a=True,
                )

        assert abs(lg.norm(matC) / lg.norm(s) - 1.0) < 1e-12  # cheap but weak Z2 check
        del matC, bInds, colorOrder, c1, c2
        sSort = np.argsort(np.abs(s))[: -self._chimax : -1]
        s = s[sSort]
        newchi = (
            np.abs(s[self._chi0 :] / s[self._chi0 - 1 : -1]) < self.cuttol
        ).nonzero()[0][
            0
        ] + self._chi0  # chi0 <= chi <= chimax-2
        U = U[:, sSort[:newchi]].copy()  # keep only relevant cols in conveniant order
        newT = self.construct_newT(U)
        if newchi == self._chi:
            self._chichanges = [newchi]
        else:
            self._chichanges.append(newchi)
        self.set_sT(s[:newchi] / s[0], newT, pcol[sSort[:newchi]])

    def check_Z2(self, tol=1e-12):
        extD = self._D2 * self._chi
        matC = self.construct_matC(reshape=False)
        pcol = np.einsum("ik,jk->ijk", self._pcol, self._pcolD2).reshape(
            extD, self._ncol
        )
        colorOrder = np.lexsort(pcol.T)
        pcol = pcol[colorOrder]
        bInds = [0, *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1), extD]

        c1 = colorOrder // self._D2
        c2 = colorOrder % self._D2
        s = np.empty(extD)
        U = np.zeros((extD, extD), dtype=self._E.dtype)
        nblocks = len(bInds) - 1
        bsize, bdens = np.empty(nblocks, dtype=int), np.empty(nblocks)
        for k in range(nblocks):  # diagonalize blocks
            i, j = bInds[k : k + 2]
            m = matC[
                c1[i:j, None] * self._chi + c1[i:j], c2[i:j] * self._D2 + c2[i:j, None]
            ]
            s[i:j], U[colorOrder[i:j], i:j], info = self._evd(m)
            if info:
                print("error in dsyevd / zheevd routine, use lg.eigh")
                s[i:j], U[colorOrder[i:j], i:j] = lg.eigh(m)
            bsize[k] = j - i
            bdens[k] = (
                np.abs(m) > (tol * lg.norm(s[i:j]))
            ).sum() / m.size  # block dens

        del pcol, colorOrder, bInds, c1, c2
        matC = (
            matC.reshape(self._chi, extD, self._D2).swapaxes(1, 2).reshape(extD, extD)
        )
        if lg.norm(matC - matC.T.conj()) / lg.norm(matC) > tol:
            raise ValueError("matC is not hermitian")
        matC = matC @ U
        U = U.T.conj()
        matC = U @ matC
        del U
        matC[np.diag_indices_from(matC)] -= s
        return lg.norm(matC) / lg.norm(s), bsize, bdens

    def check(self, tol=1e-12, sqrt_inv=None):
        """
        Display degeneracies of s, boundSpec2, rho and compute check_Z2 to check SU(N)
        symmetry.
        """
        print("check SU(N) symmetry of CTMRG:")
        values = list(np.abs(np.rint(self.s / tol) * tol))
        deg = [values.count(v) for v in set(values)]
        print("s structure =", [(deg.count(n), n) for n in sorted(set(deg))])
        boundSpec2, degen = self.check_boundSpec2(tol=tol, sqrt_inv=sqrt_inv)
        print("boundSpec2 degeneracies =", degen)
        rho = self.compute_density_matrix(tol=tol)
        values = list(np.rint(lg.eigvalsh(rho) / tol) * tol)
        deg = [values.count(v) for v in set(values)]
        print("rho degeneracies =", [(deg.count(n), n) for n in sorted(set(deg))])
        p, bs, bd = self.check_Z2(tol=tol)
        print(f"check Z2: diagonalization precision = {p:.1e}")
        print(f"block sizes = {bs}\nblock densities = {bd}")

    def transmat_spec(self):
        """
        Compute spectrum of transfer matrix using Z2^n symmetry.
        """
        pcol = np.einsum("ik,jk->ijk", self._pcol, self._pcol).reshape(
            self._chi**2, self._ncol
        )
        colorOrder = np.lexsort(pcol.T)
        pcol = pcol[colorOrder]
        bInds = [
            0,
            *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1),
            self._chi**2,
        ]
        c1 = colorOrder // self._chi
        c2 = colorOrder % self._chi
        v = np.empty(self._chi**2)
        for i, j in zip(bInds, bInds[1:]):
            m = np.empty((j - i, j - i), dtype=self._T.dtype)
            for k in range(i, j):
                for n in range(k, j):  # fill only upper matrix
                    m[k - i, n - i] = self._T[c2[k], c2[n]] @ self._T[c1[n], c1[k]]
            v[i:j] = self._evd(m, compute_v=0, overwrite_a=1)[0]
        return v


def sparseE(A, B=None):
    D = A.shape[1]
    a = csr_matrix(A.reshape(A.shape[0], D**4))
    if B is not None:
        b = csr_matrix(B.reshape(a.shape))
    else:
        b = a
    c = (a.T @ b.conj()).tocoo()
    c.eliminate_zeros()
    cr = c.row[:, None] // np.array([D, 1])
    cc = c.col[:, None] // np.array([D, 1])
    nx = cr // D**2 % D @ np.array([D**3, D]) + cc // D**2 % D @ np.array(
        [D**2, 1]
    )
    ny = cr % D @ np.array([D**3, D]) + cc % D @ np.array([D**2, 1])
    e = csc_matrix((c.data, (nx, ny)), shape=c.shape)  # sparse reshape
    return e


class sparseCTM(CTM_Z2):
    def __init__(
        self,
        a_L,
        A_L,
        chi0,
        pcolD,
        force_real=False,
        s=None,
        T=None,
        pcol=None,
        window=50,
        break_rot=False,
        alternate=False,
    ):
        """
        Use sparse matrix for tensor E.
        """
        self._D = A_L[0].shape[1]
        self._D2 = self._D**2
        self.window = max(window, 4 * self._D2)  # look for chi in [chi0, chimax]
        self._alternate = alternate  # alternate rep. on the square lattice
        self._break_rot = break_rot  # may break rotation, keep Tv = Th.swap.conj()
        self.cuttol = 1 - 1e-6  # look for ratio of consecutive values < cuttol

        self.set_A(a_L, A_L, force_real=force_real)

        self._ncol = pcolD.shape[1]
        self._pcolD2 = np.einsum("ik,jk->ijk", pcolD, pcolD).reshape(
            (self._D2, self._ncol)
        )

        if s is not None:
            self.set_sT(s, T, pcol)
            self.chi0 = chi0

        else:
            self.reset(chi0)

    def reset(self, chi0):
        matC = self._ET._mul_vector(
            np.eye(self._D2).reshape((self._D,) * 4).swapaxes(1, 2).flatten()
        ).reshape(self._D2, self._D2)
        colorOrder = np.lexsort(self._pcolD2.T)
        pcol = self._pcolD2[colorOrder].copy()
        bInds = [0, *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1), self._D2]
        s = np.empty(self._D2)
        U = np.zeros((self._D2, self._D2), dtype=matC.dtype)
        for i, j in zip(bInds, bInds[1:]):  # diagonalize blocks
            s[i:j], U[colorOrder[i:j], i:j] = lg.eigh(
                matC[colorOrder[i:j, None], colorOrder[i:j]]
            )

        assert lg.norm(U.T.conj() @ matC @ U - np.diag(s)) / lg.norm(matC) < 1e-12
        sSort = np.argsort(np.abs(s))[::-1]
        U = U[:, sSort]  # keep only relevant cols in conveniant order
        T = (
            (
                self._ET.T.reshape(self._D2, self._D2**3).T._mul_vector(
                    np.eye(self._D).flatten()
                )
            )
            .reshape((self._D2,) * 3)
            .transpose(2, 0, 1)
        )
        T = np.tensordot(U.conj(), np.tensordot(T, U, (1, 0)), (0, 0)).swapaxes(1, 2)
        self.set_sT(s[sSort], T, pcol[sSort])

        while self._chi * self._D2 < chi0:  # if chi0 is too large, take steps
            self.chi0 = self._chi * self._D2 // 2
            self.iterate()
        self.chi0 = chi0

    def set_A(self, a_L, A_L, force_real=False):
        """
        set new value for ket-tensor A. Allow to force real double-layer tensor E in
        case of complex A. Compute E as a sparse tensor while A stays dense.
        """
        sh = (A_L[0].shape[0],) + (self._D,) * 4
        b = np.array(a_L, dtype=bool)
        if b.sum() == 1:  # in many cases, A = Ti
            self._A = np.ascontiguousarray(A_L[b.nonzero()[0][0]])
            assert self._A.shape == sh, "A shape is not (d,D,D,D,D)"
            E = sparseE(self._A)

        else:
            n = len(a_L)
            assert len(A_L) == n, "a_L and A_L have different length"
            a_Ln = np.array(a_L, dtype=float)  # complexes are in A_L
            for i in range(n):
                assert A_L[i].shape == sh, "A shape is not (d,D,D,D,D)"
                a_Ln[i] /= lg.norm(A_L[i])

            self._A = np.tensordot(a_Ln, A_L, (0, 0))
            E = csc_matrix((self._D2**2, self._D2**2))
            for i in range(n):
                E += a_Ln[i] ** 2 * sparseE(A_L[i])
                for j in range(i + 1, n):
                    e = sparseE(A_L[i], A_L[j]) + sparseE(A_L[j], A_L[i])  # exact
                    e.eliminate_zeros()
                    E += a_Ln[i] * a_Ln[j] * e  # avoid 1e-20 due to a*b != b*a

        if force_real or np.isrealobj(E.data):  # complex A may result in real E
            assert lg.norm(E.data.imag) / lg.norm(E.data.real) < 1e-15, "E is not real"
            E.data = np.array(E.data.real, dtype=float, order="C", copy=False)
            self._evd = lg.lapack.dsyevd
        else:
            self._evd = lg.lapack.zheevd

        self._E = E  # needed for super methods
        self._ET = E.T  # actually need a csr E.transpose() for mat @ E
        self._chichanges = []  # change in A may change s structure

    def iterate(self):
        # It is possible to use CTM_Z2 iterate, the function works, but it does
        # not optimize sparse-dense product with reshape and transpose.
        k = self._D2 * self._chi
        matC = np.einsum("j,ijk->ikj", self._s, self._T).reshape(k, self._chi)
        Tv = self._T.swapaxes(0, 1).conj() if self._break_rot else self._T
        matC = matC @ Tv.reshape(self._chi, k)
        del Tv
        matC = matC.reshape((self._chi, self._D2, self._chi, self._D2))
        matC = matC.transpose(1, 3, 0, 2).reshape(self._D2**2, self._chi**2)
        matC = self._ET._mul_multivector(matC).T

        # Z_2 diagonalization
        pcol = np.einsum("ik,jk->ijk", self._pcol, self._pcolD2).reshape(k, self._ncol)
        colorOrder = np.lexsort(pcol.T)
        pcol = pcol[colorOrder]
        bInds = [0, *((pcol[:-1] != pcol[1:]).any(axis=1).nonzero()[0] + 1), k]

        c1 = colorOrder // self._D2
        c2 = colorOrder % self._D2
        s = np.empty(k)
        U = np.zeros((k, k), dtype=matC.dtype)
        for i, j in zip(bInds, bInds[1:]):  # diagonalize blocks
            s[i:j], U[colorOrder[i:j], i:j], info = self._evd(
                matC[
                    c1[i:j, None] * self._chi + c1[i:j],
                    c2[i:j] * self._D2 + c2[i:j, None],
                ],
                overwrite_a=1,
            )
            if info:  # fancy slicing creates a copy, so no overwrite on matC.
                print("error in dsyevd / zheevd routine, use lg.eigh")
                s[i:j], U[colorOrder[i:j], i:j] = lg.eigh(
                    matC[
                        c1[i:j, None] * self._chi + c1[i:j],
                        c2[i:j] * self._D2 + c2[i:j, None],
                    ],
                    overwrite_a=True,
                )

        assert abs(lg.norm(matC) / lg.norm(s) - 1.0) < 1e-12  # cheap but weak Z2 check
        del matC, bInds, colorOrder, c1, c2
        sSort = np.argsort(np.abs(s))[: -self._chimax : -1]
        s = s[sSort]
        newchi = (
            np.abs(s[self._chi0 :] / s[self._chi0 - 1 : -1]) < self.cuttol
        ).nonzero()[0][
            0
        ] + self._chi0  # chi0 <= chi <= chimax-2
        U = U[:, sSort[:newchi]].reshape(self._chi, self._D2 * newchi)
        T = self._T.transpose(0, 2, 1).reshape(k, self._chi) @ U
        T = T.reshape(self._chi, self._D2**2, newchi).transpose(1, 0, 2)
        T = T.reshape(self._D2**2, self._chi * newchi)
        T = self._ET._mul_multivector(T)  # time expensive
        T = T.reshape(self._D2, self._D2, self._chi, newchi).transpose(2, 1, 3, 0)
        T = T.reshape(k, newchi * self._D2)
        U = U.reshape(self._chi, self._D2, newchi).transpose(2, 0, 1).reshape(newchi, k)
        T = (U.conj() @ T).reshape(newchi, newchi, self._D2)
        T /= lg.norm(T)
        if newchi == self._chi:
            self._chichanges = [newchi]
        else:
            self._chichanges.append(newchi)
        self.set_sT(s[:newchi] / s[0], T, pcol[sSort[:newchi]])
