#!/usr/bin/env python3

import numpy as np
import scipy.linalg as lg


class Lanczos:
    """
    This class implements Lanczos algorithm to compute eigenvalues of a Hermitian or
    real symmetric matrix. The matrix itself is not needed, the only requirement is a
    function that multiplies a given vector by the matrix.

    Eigenvectors computation is not implemented.
    """

    def __init__(self, multFunc, N, v0=None):
        self.multFunc = multFunc
        self.base = [None] * 3
        if v0 is None:
            v0 = np.random.random(N)
        elif v0.shape != (N,):
            raise Exception("v0 must be of shape (N,)")
        self.base[0] = v0 / lg.norm(v0)
        self.base[1] = self.multFunc(self.base[0])
        self.diag = [np.dot(self.base[0].conj(), self.base[1]).real]
        self.base[1] = self.base[1] - self.diag[0] * self.base[0]
        self.subdiag = [lg.norm(self.base[1])]
        self.base[1] /= self.subdiag[0]
        self.niter = 1
        self.raw_vals = None

    def iterate(self):
        self.raw_vals = None
        im1, i0, ip1 = (self.niter - 1) % 3, self.niter % 3, (self.niter + 1) % 3
        self.base[ip1] = self.multFunc(self.base[i0])
        self.diag.append(np.dot(self.base[i0].conj(), self.base[ip1]).real)
        self.base[ip1] = (
            self.base[ip1]
            - self.diag[-1] * self.base[i0]
            - self.subdiag[-1] * self.base[im1]
        )
        self.subdiag.append(lg.norm(self.base[ip1]))
        self.base[ip1] /= self.subdiag[-1]
        self.niter += 1

    def raw_eigvals(self):
        """
        Compute raw eigenvalues of tridiagonal matrix obtained from iterations.
        Do not remove any ghost.
        """
        if self.raw_vals is not None:
            return self.raw_vals  # do not recompute unless re-iterated
        d = np.array(self.diag)
        e = np.array(self.subdiag[:-1])
        vals, z, info = lg.lapack.dstev(
            d, e, compute_v=False, overwrite_d=True, overwrite_e=True
        )
        if info:
            raise lg.LinAlgError("error in LAPACK diagonalization routine.")
        self.raw_vals = vals
        return vals

    def eigvals(self, k, ncv=None, tol=1e-8):
        """
        Compute k DIFFERENT largest in magnitude eigenvalues after ncv iterations.
        This function DOES NOT give multiplicities of eigenvalues.
        Very crude ghot removal is implemented: remove lonely ghost and set
        multiplicites to 1.
        Iterate ncv time before computation, if None set it to max(10*k,50).
        """
        if ncv is None:
            ncv = max(10 * k, 50)
        while self.niter < ncv:
            self.iterate()
        raw = self.raw_eigvals()
        araw = np.abs(raw)
        so = np.argsort(araw)[::-1]
        raw = raw[so]
        araw = araw[so]
        clean_vals = []
        while len(clean_vals) < k:
            i = (araw[0] / araw > 1 + tol).nonzero()[0][0]
            if i > 1:  # avoid lonely ghost
                clean_vals.append(raw[0])
                if np.min(raw[:i]) < 0 and np.max(raw[:i]) > 0:
                    clean_vals.append(-raw[0])
            araw = araw[i:]
            raw = raw[i:]
        return np.array(clean_vals[:k])  # l size may be k+1
